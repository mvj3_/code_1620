/*  
application.js for Jing.fm
version: 2.7.7
Author: Jing.fm Dev Team
*/

function json2str(e) {
    var t = this;
    switch (typeof e) {
    case "string":
        return '"' + e.replace(/(["\\])/g, "\\$1") + '"';
    case "array":
        return "[" + e.map(t.json2str).join(",") + "]";
    case "object":
        if (e instanceof Array) {
            var n = [],
                r = e.length;
            for (var i = 0; i < r; i++) n.push(t.json2str(e[i]));
            return "[" + n.join(",") + "]"
        }
        if (e == null) return "null";
        var s = [];
        for (var o in e) s.push(t.json2str(o) + ":" + t.json2str(e[o]));
        return "{" + s.join(",") + "}";
    case "number":
        return e;
    case !1:
        return e
    }
}
function fingerGestureListener(e, t) {
    function u(i) {
        o = i.touches.length, e.addEventListener("touchmove", a, !1);
        if (e.getAttribute("id") == "bg" && o == 1) {
            e.addEventListener("touchend", f, !1);
            return
        }
        o == 2 && (i.preventDefault(), n = i.touches[0].pageX, r = i.touches[0].pageY, $("#topApp").isDisplay() && t({
            status: "start"
        }), e.addEventListener("touchend", f, !1))
    }
    function a(o) {
        if (o.touches.length == 1) o.preventDefault();
        else if (o.touches.length == 2) {
            i = o.touches[0].pageX - n;
            var u = o.touches[0].pageY - r;
            s == null ? s = i : s < 0 && i > 0 || s > 0 && i < 0 || Math.abs(u) > 100 ? l() : $("#topApp").isDisplay() && t({
                target: e,
                dx: i,
                status: "move"
            })
        } else l()
    }
    function f(n) {
        if (e.getAttribute("id") == "bg" && o == 1) {
            $(document).click();
            return
        }
        o == 2 && (Interface.Current == Interface.MAIN && !Core.isFullScren ? Math.abs(i) > 120 ? t({
            target: e,
            direction: i > 0 ? "right" : "left"
        }) : Math.abs(i) < 30 && t({
            target: e,
            direction: "middle"
        }) : ($("#topApp").isDisplay() || $("#abtCtn").isDisplay()) && Math.abs(i) > 10 && t({
            target: e,
            dx: i,
            status: "end"
        })), l()
    }
    function l() {
        e.removeEventListener("touchmove", a), e.removeEventListener("touchend", f), n = null, i = null, s = null
    }
    var n, r, i, s, o;
    e.addEventListener("touchstart", u, !1)
}
function socketOnDisconnect() {
    Core.nowIsReady = !1
}
function socketConnect() {
    now.reconnect()
}
function swfReady() {
    if (Player.swfObj != undefined) return;
    Player.swfObj = swfobject.getObjectById("rotateFlash"), Player.ready()
}
new function (e) {
    e.fn.setCursorPosition = function (t) {
        if (e(this).get(0).setSelectionRange) e(this).get(0).setSelectionRange(t, t);
        else if (e(this).get(0).createTextRange) {
            var n = e(this).get(0).createTextRange();
            n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", t), n.select()
        }
    }, e.fn.isDisplay = function () {
        return e(this).length == 0 ? !1 : e(this).css("display") == "none" ? !1 : !0
    }, e.fn.autoTextarea = function (t) {
        var n = {
            maxHeight: null,
            minHeight: e(this).height()
        }, r = e.extend({}, n, t);
        return e(this).each(function () {
            e(this).bind("paste cut keydown keyup focus blur", function () {
                var t, n = this.style;
                this.style.height = r.minHeight + "px", this.scrollHeight > r.minHeight && (r.maxHeight && this.scrollHeight > r.maxHeight ? (t = r.maxHeight, n.overflowY = "scroll") : (t = this.scrollHeight, n.overflowY = "hidden"), n.height = t + "px", e(this).parent().css("height", t + 40))
            })
        })
    }
}(jQuery);
var Core = {
    VERSION: "2.7.7",
    bodyWidth: 0,
    bodyHeight: 0,
    screenWidth: 0,
    screentHeight: 0,
    mouseX: 0,
    mouseY: 0,
    MIN_BODY_WIDTH: 994,
    MIN_BODY_HEIGHT: 620,
    MAX_BODY_WIDTH: 2560,
    MAX_BODY_HEIGHT: 1440,
    JingATokenHeader: "Jing-A-Token-Header",
    JingRTokenHeader: "Jing-R-Token-Header",
    API_VER: "/api/v1",
    isOverlay: !1,
    UPLOAD_ACTION: 0,
    SIGNUP: 1,
    SETTING: 2,
    isFullScren: !1,
    FS_HEIGHT: 0,
    MAX_OVERLAY_HEIGHT: 0,
    DOMAIN: "jing.fm",
    COOKIE_VERSION: "1.0",
    ie67: $("html").hasClass("ie6") || $("html").hasClass("ie7"),
    ie68: "",
    ie69: "",
    ie: $("html").hasClass("ie"),
    gecko: $("html").hasClass("gecko"),
    opera: $("html").hasClass("opera"),
    ipad: $("html").hasClass("ipad"),
    webkit: $("html").hasClass("webkit"),
    standalone: navigator.standalone,
    browser: "",
    nowIsReady: !1,
    nowSuccess: 1,
    A30: "30",
    A50: "50",
    A64: "64",
    init: function () {
        Core.standalone ? Core.standalone = "jing.fm" : Core.standalone = "", Core.ie68 = Core.ie67 || $("html").hasClass("ie8"), Core.ie69 = Core.ie68 || $("html").hasClass("ie9"), document.domain = Core.DOMAIN;
        var e = new Array("defaults/profile/cover" + Retina.suffix + ".jpg", "defaults/avatar/" + Core.A30 + ".jpg", "defaults/avatar/" + Core.A50 + ".jpg", "defaults/avatar/" + Core.A64 + ".jpg", "qkPlay" + Retina.suffix + ".png");
        for (var t = 0; t < e.length; ++t) {
            var n = new Image;
            n.src = IMG_URL + "/" + e[t]
        }
        $(document).on("click", ".overlay", function (e) {
            e.stopPropagation()
        }), Core.resize(), Core.screenWidth = window.screen.width, Core.screenHeight = window.screen.height, $("html").hasClass("win") && $("html").hasClass("chrome") ? $("#signUp, #signIn").children().css({
            width: "88px"
        }) : Core.ipad && $("#signUp, #signIn").children().css({
            width: "87px"
        })
    },
    getSelectionStart: function (e) {
        if (e.createTextRange) {
            var t = document.selection.createRange().duplicate();
            return t.moveEnd("character", e.value.length), t.text == "" ? e.value.length : e.value.lastIndexOf(t.text)
        }
        return e.selectionStart
    },
    getSelectionEnd: function (e) {
        if (e.createTextRange) {
            var t = document.selection.createRange().duplicate();
            return t.moveStart("character", -e.value.length), t.text.length
        }
        return e.selectionEnd
    },
    get: function (e, t) {
        return Number(e.css(t).replace("px", ""))
    },
    getCookie: function (name) {
        var tempName = "";
        name != "jing.auth" && name != "jing.misc" && (tempName = name, name = "jing.misc");
        var arg = name + "=",
            alen = arg.length,
            clen = document.cookie.length,
            i = 0;
        while (i < clen) {
            var j = i + alen;
            if (document.cookie.substring(i, j) == arg) {
                var endstr = document.cookie.indexOf(";", j);
                endstr == -1 && (endstr = document.cookie.length);
                var result = unescape(document.cookie.substring(j, endstr));
                if (tempName != "") {
                    try {
                        result = eval("(" + result + ")")
                    } catch (e) {
                        result = new Object
                    }
                    return result.v != Core.COOKIE_VERSION ? "" : result[tempName] == undefined ? "" : result[tempName]
                }
                return result
            }
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) break
        }
        return null
    },
    setCookie: function (name, value, expires, path, domain, secure) {
        if (name != "jing.auth") {
            var tempName = name + "";
            name = "jing.misc";
            var tempValue = value + "",
                misc = Core.getCookie("jing.misc");
            if (misc == null || misc == "") misc = new Object, misc.v = Core.COOKIE_VERSION;
            else {
                try {
                    misc = eval("(" + misc + ")")
                } catch (e) {
                    misc = new Object
                }
                misc.v != Core.COOKIE_VERSION && (misc = new Object, misc.v = Core.COOKIE_VERSION)
            }
            misc[tempName] = tempValue, value = Core.object2String(misc)
        }
        domain = "." + Core.DOMAIN, expires = Core.getExpDate(30, 0, 0), document.cookie = name + "=" + escape($.trim(value)) + (expires ? "; expires=" + expires : "") + (path ? "; path=" + path : "") + (domain ? "; domain=" + domain : "") + (secure ? "; secure" : "")
    },
    getExpDate: function (e, t, n) {
        var r = new Date;
        if (typeof e == "number" && typeof t == "number" && typeof t == "number") return r.setDate(r.getDate() + parseInt(e)), r.setHours(r.getHours() + parseInt(t)), r.setMinutes(r.getMinutes() + parseInt(n)), r.toGMTString()
    },
    resize: function (e) {
        $("#tps").hide(), Core.bodyWidth = document.documentElement.clientWidth, Core.bodyHeight = document.documentElement.clientHeight, Core.bodyWidth = Core.bodyWidth > Core.MIN_BODY_WIDTH ? Core.bodyWidth : Core.MIN_BODY_WIDTH, Core.bodyWidth = Core.bodyWidth < Core.MAX_BODY_WIDTH ? Core.bodyWidth : Core.MAX_BODY_WIDTH, $("#allBd").css({
            height: "100%"
        }), !About.isOpen && !NtrlLngTop.isOpen && $("#mainBd").css({
            width: "100%"
        }), $("#mainBd, #ntrlLngTopCtn").css({
            height: Core.bodyHeight + "px"
        }), Core.MAX_OVERLAY_HEIGHT = Core.bodyHeight - 90, Core.FS_HEIGHT = Core.bodyHeight - 75 - 3, About.resize(), Gns.resize(), NtrlLngTop.resize(), Core.isFullScren && Guide.resize();
        switch (Interface.Current) {
        case Interface.LOGIN:
            Signup.resize();
            break;
        case Interface.MAIN:
            Player.resize(), $("#topApp").isDisplay() && Top.resize();
            break;
        case Interface.SEARCH:
            Player.resize(), Search.resize();
            break;
        case Interface.JING:
        }
    },
    strLength: function (e) {
        var t = 0;
        for (var n = 0; n < e.length; n++) e.charCodeAt(n) > 256 ? t += 2 : t++;
        return t
    },
    objLength: function (e) {
        var t = 0;
        for (var n in e)++t;
        return t
    },
    objClone: function (e, t) {
        var n = new t;
        for (var r in e) n[r] = e[r];
        return n
    },
    cloneObject: function (e) {
        var t = e.constructor === Array ? [] : {};
        for (var n in e) e.hasOwnProperty(n) && (t[n] = typeof e[n] == "object" ? cloneObject(e[n]) : e[n]);
        return t
    },
    inputConver: function (e) {
        $("#mainBd").append('<p id="inputConver"></p>'), $("#inputConver").text(e);
        var e = $("#inputConver").html();
        return $("#inputConver").remove(), e
    },
    badgesUrl: function (e, t, n) {
        return n == undefined && (n = "jpg"), Retina.enabled ? IMG_URL + "/badges/" + t + "@2x/" + e + "." + n : IMG_URL + "/badges/" + t + "/" + e + "." + n
    },
    imgLoad: function (e, t, n, r, i) {
        e.prop({
            width: r + "px",
            height: r + "px"
        }).css({
            width: r + "px",
            height: r + "px"
        });
        var s = new Image;
        s.obj = e, s.wh = r, s.st = t, s.type = i, s.onload = function () {
            var e = 0;
            this.type == "frd" && (e = Frd.st);
            if (this.st != "" && this.st != e) return;
            var t = this.width,
                n = this.height,
                i = "",
                s = 0;
            t < n ? (n = parseInt(this.wh / t * n), i = "top", t = this.wh) : t > n ? (t = parseInt(this.wh / n * t), i = "left", n = this.wh) : (t = this.wh, n = this.wh), r == 30 ? s = 1 : r == 64 && (s = 2), s = 0, this.obj.prop({
                width: t + "px",
                height: n + "px"
            }), this.obj.css({
                width: t + "px",
                height: n + "px"
            }), i == "top" ? this.obj.css({
                top: -((n - this.wh) / 2) + "px",
                left: s + "px"
            }) : i == "left" ? this.obj.css({
                left: -((t - this.wh) / 2) + "px",
                top: s + "px"
            }) : this.obj.css({
                left: s + "px",
                top: s + "px"
            }), this.obj.css({
                position: "absolute"
            }), this.obj.prop("src", this.src)
        };
        if (n == null || n == "" || n == "null") return;
        n.indexOf("http://") != 0 && (n = $.id2url(n, $.wh2Thumbtype(r), "avatar")), s.src = n
    },
    isEmpty: function (e) {
        return e == null || e == undefined || e == "" ? !0 : !1
    },
    object2String: function (e) {
        var t, n = "";
        if (e) {
            n += "{";
            for (var r in e) {
                t = e[r];
                switch (typeof t) {
                case "object":
                    t[0] ? n += "'" + r + "':" + Core.array2String(t) + "," : n += "'" + r + "':" + Core.object2String(t) + ",";
                    break;
                case "string":
                    n += "'" + r + "':'" + t + "',";
                    break;
                default:
                    n += r + ":" + t + ","
                }
            }
            n = n.substring(0, n.length - 1) + "}"
        }
        return n
    },
    array2String: function (e) {
        var t, n = "";
        if (e) {
            n += "[";
            for (var r in e) {
                t = e[r];
                switch (typeof t) {
                case "object":
                    t[0] ? n += Core.array2String(t) + "," : n += Core.object2String(t) + ",";
                    break;
                case "string":
                    n += "'" + t + "',";
                    break;
                default:
                    n += t + ","
                }
            }
            n = n.substring(0, n.length - 1) + "]"
        }
        return n
    },
    playerCDShow: function () {
        $("html").hasClass("gecko") || $("html").hasClass("opera") ? $("#mscPlrCtn, #rotateFlash").css({
            visibility: "visible"
        }) : $("#mscPlrCtn").show()
    },
    playerCDHide: function () {
        $("html").hasClass("gecko") || $("html").hasClass("opera") ? $("#mscPlrCtn, #rotateFlash").css({
            visibility: "hidden"
        }) : $("#mscPlrCtn").hide()
    },
    fullScrenMenuShow: function (e, t, n) {
        t == "psnRd" ? (About.isAtInsertPlay = !1, About.isTopInsertPlay = !1, About.isInsertPlay = !1, Top.isInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, About.isSingleInsertPlay = !1) : t == "abtTop" ? (About.isAtInsertPlay = !1, Player.isPsnRdInsertPlay = !1, About.isInsertPlay = !1, Top.isInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, About.isSingleInsertPlay = !1) : t == "mood" ? (About.isAtInsertPlay = !1, About.isTopInsertPlay = !1, Player.isPsnRdInsertPlay = !1, Top.isInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, About.isSingleInsertPlay = !1) : t == "ntrl" ? (About.isAtInsertPlay = !1, About.isTopInsertPlay = !1, About.isInsertPlay = !1, Player.isPsnRdInsertPlay = !1, Top.isInsertPlay = !1, About.isSingleInsertPlay = !1) : t == "atPlay" ? (About.isTopInsertPlay = !1, About.isInsertPlay = !1, Player.isPsnRdInsertPlay = !1, Top.isInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, About.isSingleInsertPlay = !1) : t == "flwLstn" ? (About.isAtInsertPlay = !1, About.isTopInsertPlay = !1, About.isInsertPlay = !1, Player.isPsnRdInsertPlay = !1, Top.isInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, About.isSingleInsertPlay = !1) : t == "singlePlay" && (About.isAtInsertPlay = !1, About.isTopInsertPlay = !1, About.isInsertPlay = !1, Player.isPsnRdInsertPlay = !1, Top.isInsertPlay = !1, NtrlLngTop.isInsertPlay = !1), t != "flwLstn" && Flw.isFlw && Flw.toid != "" && (Flw.leave(), Flw.empty()), Player.cacheMusic == null && t != "signup" && (Player.cacheMusic = Player.music, Player.cachePos = Player.pos, Player.cacheSt = Search.st, Player.cacheSec = Player.currentTime, Player.cacheTotal = Search.total), animateAction = function () {
            t != "rptOne" && $("#playerRptOne").removeClass("selected");
            if ($("#fsappMenu").length != 0) {
                t == "rptOne" && (Player.isPsnRdInsertPlay || About.isInsertPlay || About.isTopInsertPlay || About.isAtInsertPlay || NtrlLngTop.isInsertPlay || About.isSingleInsertPlay) ? e == "" ? $("#prevHtml").show() : ($("#fsappMenu>.ctt").append(e), $("#prevHtml").hide()) : $("#fsappMenu>.ctt").html(e), $("#fsappMenu").css({
                    width: "auto"
                });
                var r = $("#fsappMenu>.ctt").width() + 41;
                $("#fsappMenu").css({
                    width: r + "px"
                });
                return
            }
            $(this).hide();
            var i = "";
            t == "signup" && (i = "hide"), $("#tbCtn").append('<div id="fsappMenu" class="fsappTit" style="bottom:-75px; visibility:hidden"><a id="' + t + 'FsappMenu" class="rndBtn ' + t + " " + i + '" href="#"></a>' + '<p class="ctt">' + e + "</p>" + "</div>");
            var r = $("#fsappMenu>.ctt").width();
            t != "signup" && (r += 41), $("#fsappMenu").css({
                width: r + "px",
                visibility: "visible"
            }), setTimeout(function () {
                $("#fsappMenu").animate({
                    bottom: "-14px"
                }, 300)
            }, 300), $("#abtTopFsappMenu").click(function () {
                About.currentMain = "top", About.switching({
                    ouid: n
                });
                return
            })
        }, $("#fsappMenu").length == 0 ? $("#mainMenuCtn").animate({
            bottom: "-75px"
        }, 300, animateAction) : animateAction()
    },
    fullScrenMenuHide: function () {
        $("#fsappMenu").animate({
            bottom: "-75px"
        }, 300, function () {
            $(this).remove(), $("#mainMenuCtn").show().animate({
                bottom: "-14px"
            }, 300)
        })
    },
    getDate: function () {
        var e = new Date,
            t = e.getYear() + 1900;
        return Core.ie68 && (t = e.getYear()), t + "-" + (e.getMonth() + 1) + "-" + e.getDate() + " " + e.getHours() + ":" + e.getMinutes() + ":" + e.getSeconds()
    },
    isNewIcon: function (e, t) {
        var n = Signup.userDetail.newview;
        for (var r in n) if (r == e) for (var i = 0; i < n[r].length; ++i) if (n[r][i] == t) return !0;
        return !1
    },
    isNewBdg: function (e, t) {
        e += "";
        var n = Signup.userDetail.newview.b;
        for (var r = 0; r < n.length; ++r) {
            var i = n[r].split("-");
            for (var s = 0; s < i.length; ++s) if (i[s] == e) return t == 1 && (Signup.userDetail.newview.b = Signup.userDetail.newview.b.slice(0, r).concat(Signup.userDetail.newview.b.slice(r + 1))), !0
        }
        return !1
    },
    ulNewIcon: function (e, t) {
        var n = Signup.userDetail.newview;
        t = t.split("-")[t.split("-").length - 1];
        for (var r = 0; r < n[e].length; ++r) {
            var i = n[e][r].split("-")[n[e][r].split("-").length - 1];
            if (i == t) {
                Signup.userDetail.newview[e] = n[e].slice(0, r).concat(n[e].slice(r + 1)), n[e] = n[e].slice(0, r).concat(n[e].slice(r + 1)), --r;
                break
            }
        }
        $.ajax({
            url: Core.API_VER + "/app/post_known_app",
            data: {
                uid: Signup.userDetail.id,
                t: e,
                mids: t
            }
        })
    },
    uploadify: function (action) {
        $("#uploadify").uploadify({
            formData: {
                uid: Signup.userDetail.id,
                atoken: Core.getCookie("jing.auth").split(",")[0],
                rtoken: Core.getCookie("jing.auth").split(",")[1]
            },
            uploader: "/api/v1/user/avatar/avatarupload",
            swf: "http://jing.fm/assets/vendor/uploadify.swf",
            auto: !0,
            buttonText: "上传头像",
            fileObjName: "file",
            width: 252,
            multi: !1,
            height: 52,
            onSWFReady: function () {
                $("#SWFUpload_0").css("left", "10px")
            },
            onDialogOpen: function () {
                $("#uploadify-button").addClass("selected"), $("#uploadify-button").children("span").text("正在打开")
            },
            onDialogClose: function () {
                $("#uploadify-button").removeClass("selected"), $("#uploadify-button").children("span").text("上传头像")
            },
            onUploadStart: function (e) {
                $("#uploadify-button").children("span").text("上传中，请稍侯...")
            },
            onUploadSuccess: function (file, response, data) {
                var $obj = $("#uploadify-button").children("span");
                response = eval("(" + response + ")");
                if (response.success) {
                    $obj.text("上传成功"), Signup.userDetail.fid = response.result;
                    if (Interface.Current == Interface.LOGIN) {
                        var url = $.id2url(response.result, "US", "avatar");
                        $("#localAvt").prop("src", url)
                    } else {
                        var img = new Image;
                        img.onload = function () {
                            $("#abtAvt").prop("src", this.src)
                        };
                        var avt = response.result;
                        avt.indexOf("http://") != 0 && (avt = $.id2url(avt, "U1", "avatar"), avt.indexOf("?") >= 0 ? avt = avt.substring(0, avt.indexOf("?")) + ".png" + avt.substring(avt.indexOf("?")) : avt += ".png"), img.src = avt;
                        var url = $.id2url(response.result, "UT", "avatar");
                        $("#avt-0").prop("src", url), $("#avt-0").parents(".fldCtn").find("a").click()
                    }
                } else response.code == "312" ? $obj.text("图片格式仅支持jpg,png,bmp，请重传") : $obj.text("上传失败");
                Signup.timeOutId = setTimeout(function () {
                    $obj.unbind("mouseover"), $obj.text("上传头像")
                }, 5e3), $("#uploadify-button").bind("mouseover", function () {
                    clearTimeout(Signup.timeOutId), $(this).text("上传头像"), $obj.unbind("mouseover")
                })
            }
        })
    }
};
new function (e) {
    var t = e.separator || "&",
        n = e.spaces === !1 ? !1 : !0,
        r = e.suffix === !1 ? "" : "[]",
        i = e.prefix === !1 ? !1 : !0,
        s = i ? e.hash === !0 ? "#" : "?" : "",
        o = e.numbers === !1 ? !1 : !0;
    jQuery.query = new function () {
        var e = function (e, t) {
            return e != undefined && e !== null && (t ? e.constructor == t : !0)
        }, r = function (e) {
                var t, n = /\[([^[]*)\]/g,
                    r = /^([^[]+)(\[.*\])?$/.exec(e),
                    i = r[1],
                    s = [];
                while (t = n.exec(r[2])) s.push(t[1]);
                return [i, s]
            }, i = function (t, n, r) {
                var s, o = n.shift();
                typeof t != "object" && (t = null);
                if (o === "") {
                    t || (t = []);
                    if (e(t, Array)) t.push(n.length == 0 ? r : i(null, n.slice(0), r));
                    else if (e(t, Object)) {
                        var u = 0;
                        while (t[u++] != null);
                        t[--u] = n.length == 0 ? r : i(t[u], n.slice(0), r)
                    } else t = [], t.push(n.length == 0 ? r : i(null, n.slice(0), r))
                } else if (o && o.match(/^\s*[0-9]+\s*$/)) {
                    var a = parseInt(o, 10);
                    t || (t = []), t[a] = n.length == 0 ? r : i(t[a], n.slice(0), r)
                } else {
                    if (!o) return r;
                    var a = o.replace(/^\s*|\s*$/g, "");
                    t || (t = {});
                    if (e(t, Array)) {
                        var f = {};
                        for (var u = 0; u < t.length; ++u) f[u] = t[u];
                        t = f
                    }
                    t[a] = n.length == 0 ? r : i(t[a], n.slice(0), r)
                }
                return t
            }, u = function (e) {
                var t = this;
                return t.keys = {}, e.queryObject ? jQuery.each(e.get(), function (e, n) {
                    t.SET(e, n)
                }) : jQuery.each(arguments, function () {
                    var e = "" + this;
                    e = e.replace(/^[?#]/, ""), e = e.replace(/[;&]$/, ""), n && (e = e.replace(/[+]/g, " ")), jQuery.each(e.split(/[&;]/), function () {
                        var e = decodeURIComponent(this.split("=")[0] || ""),
                            n = decodeURIComponent(this.split("=")[1] || "");
                        if (!e) return;
                        o && (/^[+-]?[0-9]+\.[0-9]*$/.test(n) ? n = parseFloat(n) : /^[+-]?[0-9]+$/.test(n) && (n = parseInt(n, 10))), n = !n && n !== 0 ? !0 : n, n !== !1 && n !== !0 && typeof n != "number" && (n = n), t.SET(e, n)
                    })
                }), t
            };
        return u.prototype = {
            queryObject: !0,
            has: function (t, n) {
                var r = this.get(t);
                return e(r, n)
            },
            GET: function (t) {
                if (!e(t)) return this.keys;
                var n = r(t),
                    i = n[0],
                    s = n[1],
                    o = this.keys[i];
                while (o != null && s.length != 0) o = o[s.shift()];
                return typeof o == "number" ? o : o || ""
            },
            get: function (t) {
                var n = this.GET(t);
                return e(n, Object) ? jQuery.extend(!0, {}, n) : e(n, Array) ? n.slice(0) : n
            },
            SET: function (t, n) {
                var s = e(n) ? n : null,
                    o = r(t),
                    u = o[0],
                    a = o[1],
                    f = this.keys[u];
                return this.keys[u] = i(f, a.slice(0), s), this
            },
            set: function (e, t) {
                return this.copy().SET(e, t)
            },
            REMOVE: function (e) {
                return this.SET(e, null).COMPACT()
            },
            remove: function (e) {
                return this.copy().REMOVE(e)
            },
            EMPTY: function () {
                var e = this;
                return jQuery.each(e.keys, function (t, n) {
                    delete e.keys[t]
                }), e
            },
            load: function (e) {
                var t = e.replace(/^.*?[#](.+?)(?:\?.+)?$/, "$1"),
                    n = e.replace(/^.*?[?](.+?)(?:#.+)?$/, "$1");
                return new u(e.length == n.length ? "" : n, e.length == t.length ? "" : t)
            },
            empty: function () {
                return this.copy().EMPTY()
            },
            copy: function () {
                return new u(this)
            },
            COMPACT: function () {
                function t(n) {
                    var r = typeof n == "object" ? e(n, Array) ? [] : {} : n;
                    if (typeof n == "object") {
                        function i(t, n, r) {
                            e(t, Array) ? t.push(r) : t[n] = r
                        }
                        jQuery.each(n, function (n, s) {
                            if (!e(s)) return !0;
                            i(r, n, t(s))
                        })
                    }
                    return r
                }
                return this.keys = t(this.keys), this
            },
            compact: function () {
                return this.copy().COMPACT()
            },
            toString: function () {
                var r = 0,
                    i = [],
                    o = [],
                    u = this,
                    a = function (e) {
                        return e += "", n && (e = e.replace(/ /g, "+")), encodeURIComponent(e)
                    }, f = function (t, n, r) {
                        if (!e(r) || r === !1) return;
                        var i = [a(n)];
                        r !== !0 && (i.push("="), i.push(a(r))), t.push(i.join(""))
                    }, l = function (e, t) {
                        var n = function (e) {
                            return !t || t == "" ? [e].join("") : [t, "[", e, "]"].join("")
                        };
                        jQuery.each(e, function (e, t) {
                            typeof t == "object" ? l(t, n(e)) : f(o, n(e), t)
                        })
                    };
                return l(this.keys), o.length > 0 && i.push(s), i.push(o.join(t)), i.join("")
            }
        }, new u(location.search, location.hash)
    }
}(jQuery.query || {});
var Interface = {
    Current: 2,
    MAIN: 0,
    SEARCH: 1,
    LOGIN: 2,
    JING: 3
}, NOW_URL = "http://zoro.jing.fm",
    MEDIA_URL = "http://pmedia.jing.fm",
    IMG_URL = "http://img.jing.fm/assets/jing",
    res = new Object,
    SnsArr = new Array("qq", "wb", "rr", "db"),
    resRoot = new Object;
resRoot.tkrs = !1, resRoot.apps = !1, resRoot.frdCt = !1;
var resData = {
    frdCt: [{
            mid: "inHs",
            name: "入驻好友",
            display: "true"
        }, {
            mid: "wb",
            name: "微博好友",
            display: "false"
        }, {
            mid: "rr",
            name: "人人好友",
            display: "false"
        }, {
            mid: "db",
            name: "豆瓣好友",
            display: "false"
        }, {
            mid: "qq",
            name: "腾讯好友",
            display: "false"
        }, {
            mid: "rbsFrd",
            name: "好友垃圾箱",
            display: "true"
        }
    ],
    ext: [{
            mid: "mtFrds",
            name: "你可能认识的人",
            display: "true"
        }, {
            mid: "invt",
            name: "邀请",
            display: "true"
        }, {
            mid: "extWb",
            name: "连接微博",
            display: "true"
        }, {
            mid: "extRr",
            name: "连接人人",
            display: "true"
        }, {
            mid: "extDb",
            name: "连接豆瓣",
            display: "true"
        }, {
            mid: "extQq",
            name: "连接腾讯",
            display: "true"
        }, {
            mid: "gm",
            name: "邀请Gmail邮箱好友",
            display: "true"
        }, {
            mid: "yh",
            name: "邀请Yahoo!邮箱好友",
            display: "true"
        }, {
            mid: "ht",
            name: "邀请Hotmail邮箱好友",
            display: "true"
        }, {
            mid: "rbsFrd",
            name: "好友垃圾箱",
            display: "true"
        }
    ]
}, resUrl = new Object;
resUrl.qq = Core.API_VER + "/oauth/fetch_friends?uid={id}&identify=qq_weibo", resUrl.rr = Core.API_VER + "/oauth/fetch_friends?uid={id}&identify=renren", resUrl.db = Core.API_VER + "/oauth/fetch_friends?uid={id}&identify=douban", resUrl.wb = Core.API_VER + "/oauth/fetch_friends?uid={id}&identify=sina_weibo", resUrl.inHs = Core.API_VER + "/account/fetch_friends_order?uid={id}&ouid={id}", resUrl.rbsFrd = Core.API_VER + "/account/fetch_block_friends?uid={id}", resUrl.mtFrds = Core.API_VER + "/account/fetch_mtknown_friends?uid={id}", resUrl.invt = Core.API_VER + "/app/fetch_invitations?uid={id}";
var ConverSns = new Object;
ConverSns.extQq = "qq", ConverSns.extRr = "rr", ConverSns.extWb = "wb", ConverSns.extDb = "db", ConverSns.qq = "extQq", ConverSns.rr = "extRr", ConverSns.wb = "extWb", ConverSns.db = "extDb", ConverSns.qq1 = "qq_weibo", ConverSns.rr1 = "renren", ConverSns.db1 = "douban", ConverSns.wb1 = "sina_weibo", ConverSns.renren = "rr", ConverSns.sina_weibo = "wb", ConverSns.qq_weibo = "qq", ConverSns.douban = "db";
var snsWindowWh = new Object;
snsWindowWh.extQq = new Array(650, 480), snsWindowWh.extRr = new Array(981, 523), snsWindowWh.extWb = new Array(750, 555), snsWindowWh.extDb = new Array(590, 435);
var resStyle = new Object;
resStyle.tkrs = new Array(250, 87, 3, 2, 50);
var Thm = new Array;
Thm[0] = new Array("jzwd", "爵士印象"), Thm[1] = new Array("rdpnk", "橙色年华"), Thm[2] = new Array("dflt", "淡淡时光"), Thm[3] = new Array("blkstn", "黑色迷墙"), Thm[4] = new Array("ctwd", "雕刻时光"), Thm[5] = new Array("rlwd", "一叶知秋");
var resEmail = new Object;
resEmail.gm = "Gmail", resEmail.yh = "Yahoo", resEmail.ht = "Hotmail";
var resSns = new Object;
resSns.extQq = "qq_weibo", resSns.extRr = "renren", resSns.extWb = "sina_weibo", resSns.extDb = "douban";
var btnDes = new Object;
btnDes.mscSch = "音乐中心", btnDes.gns = "Genius", btnDes.playCtlPlay = "播放", btnDes.playCtlPause = "暂停", btnDes.next = "换歌", btnDes.tkrs = "Ta们在听", btnDes.about = "个人主页", btnDes.frdCt = "好友中心", btnDes.menuMore = "下一页", btnDes.menuRfrsh = "刷新", btnDes.menuLove = "收听自己", btnDes.menuVo = "语音说明", btnDes.signUp = "注册", btnDes.signIn = "登录", btnDes.snsRenren = "人人网登录", btnDes.renren = "人人网", btnDes.snsSina = "微博登录", btnDes.sina_weibo = "新浪微博", btnDes.snsQQ = "腾讯登录", btnDes.qq_weibo = "腾讯微博", btnDes.snsDouban = "豆瓣登录", btnDes.douban = "豆瓣网";
var St = {};
St.sns = {
    wb: "自动同步到" + btnDes.sina_weibo,
    qq: "自动同步到" + btnDes.qq_weibo,
    rr: "自动同步到" + btnDes.renren,
    db: "自动同步到" + btnDes.douban
}, St.ntfct = {
    tckNtf: "自动提醒好友在听",
    rmdTone: "开启消息提醒声音",
    frdCntNtf: "自动提醒在线好友",
    tipNtf: "自动提示菜单Tip"
}, St.gns = {
    timedot: "自动提示歌曲的演奏信息",
    rltd: "自动提示歌曲的周边信息，电影相关",
    frdlvd: "自动提示你的朋友也喜欢了这首歌",
    rcmd: "自动推荐搜索组合",
    ss: "自动开启智能推荐"
}, St.dsply = {
    lgA: "开启大字体显示",
    rtCv: "开启封面旋转",
    autoSnc: "开启一键分享",
    hbr: "使用高品质音乐"
}, St.snsDes = "社交网络设置", St.ntfctDes = "提醒设置", St.gnsDes = "Genius设置", St.dsplyDes = "辅助设置";
var StPrfl = {};
StPrfl.info = ["个人信息", 257], StPrfl.avatar = ["修改头像"], StPrfl.etpwd = ["修改密码", 125], StPrfl.thm = ["设置主题", 367], StPrfl.fltr = ["过滤器"];
var FrdDes = new Object;
FrdDes.olFrd = "好友在线", FrdDes.inHs = "入驻好友", FrdDes.rbsFrd = "被删除的好友", FrdDes.qq = "腾讯微博好友", FrdDes.rr = "人人好友", FrdDes.wb = "微博好友", FrdDes.db = "豆瓣好友", FrdDes.mtFrds = "你可能认识的人", FrdDes.invt = "邀请码";
var Symbol = new Object;
Symbol["+"] = "+", Symbol["="] = "+", Symbol["＋"] = "+", Symbol["＝"] = "+", Symbol["-"] = "-", Symbol["－"] = "-", Symbol["＿"] = "-", Symbol["&"] = "&", Symbol[" + "] = "", Symbol[" - "] = "", Symbol[" & "] = "";
var Emj = new Object;
Emj.tst = "1", Emj.wzb = "2", Emj.dx = "3", Emj.k = "4", Emj.bj = "5", Emj.by = "6", Emj.lh = "7", Emj.ks = "8", Emj.wq = "9", Emj.jy = "10", Emj.n = "11", Emj.bb = "12", Emj.pz = "13", Emj.l = "14", Emj.dk = "15", Emj.yw = "16";
var Device = new Object;
Device.P = new Array("webOl", "Web在线"), Device.O = new Array("iphnOl", "手机在线"), Device.D = new Array("iphnOl", "手机在线"), Device.T = new Array("iphnOl", "手机在线"), Device.R = new Array("iphnOl", "手机在线"), $(function () {
    var e = ["22a8db", "280df9", "490917"];
    /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) && (window.location.href = "http://jing.fm/tracks/" + e[parseInt(Math.random() * 3)] + ".html");
    try {
        _gaq.push()
    } catch (t) {
        _gaq = new Object, _gaq.push = function () {}
    }($("html").data("now_time") == undefined || $("html").data("now_time") == "") && $("html").data("now_time", (new Date).getTime()), $.query.get(Core.JingATokenHeader) != "" && Core.setCookie("jing.auth", $.query.get(Core.JingATokenHeader) + "," + $.query.get(Core.JingRTokenHeader));
    var n = Core.getCookie("jing.auth"),
        r = "",
        i = "";
    n != null && n != "" && (r = n.split(",")[0], i = n.split(",")[1]), $.ajaxSetup({
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        type: "POST",
        dataType: "json",
        headers: {
            "Jing-A-Token-Header": r,
            "Jing-R-Token-Header": i
        },
        async: !0
    }), Core.ipad && ($("#vlmCtrl").hide(), $("#mainBd").append('<div id="bg" style="width:100%; height:100%; plosition:absolute; z-index:1"></div>'), fingerGestureListener($("body")[0], function (e) {}), fingerGestureListener($("#bg")[0], function (e) {})), !swfobject.ua.pv[0] && (Core.ie68 || Core.gecko || Core.opera) && (window.location.href = "/noflash.html"), Retina.init(), Retina.enabled && $("img[src='" + IMG_URL + "/defaults/avatar/64.jpg']").prop("src", IMG_URL + "/defaults/avatar/" + Core.A64 + ".jpg");
    if (window.console == undefined) {
        var s = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
        window.console = {};
        for (var o = 0; o < s.length; ++o) window.console[s[o]] = function () {}
    }
    setInterval(function () {
        if (Cht.offlineCount == 0) {
            document.title = "Jing+ Music";
            return
        }
        document.title == "[新消息] Jing+ Music" ? document.title = "Jing+ Music" : document.title = "[新消息] Jing+ Music"
    }, 2e3), Core.init(), Signup.init(), $("#maskUserMenu").click(function () {
        $("#guide").isDisplay() || Gns.nowGns('请点击右上角的 X 关闭应用，或 <a href="#" class="trg closeFullScreenEvt">点此处直接关闭</a>')
    }), $(document).click(function () {
        $("#atSgstCtn").hide();
        if (Core.isFullScren) return;
        $("#tps").hide();
        switch (Interface.Current) {
        case Interface.MAIN:
            Gns.openGnsArr = new Array, About.closeAbout(), NtrlLngTop.closeSelf(), Gns.closeGns(), Main.hide();
            break;
        case Interface.SEARCH:
            Interface.Current = Interface.MAIN, Main.hide(), Search.hide(), $("#schFld").blur();
            break;
        case Interface.LOGIN:
            $("#signUpCtn").isDisplay() && ($("#tps").hide(), $("#signUp").hasClass("selected") ? $("#signUp").click() : $("#signIn").click());
            break;
        case Interface.JING:
            Interface.Current = Interface.MAIN, $("#logo").removeClass("selected"), $("#rlsCtn").hide(), Main.hide(), Search.hide()
        }
    }), key("delete", function () {
        Interface.Current == Interface.MAIN && !Core.isFullScren && $("#playerHate").click()
    }), key("c", function () {
        Interface.Current == Interface.MAIN && !Core.isFullScren && ($("#about").hasClass("selected") || $("#about").click())
    }), key("s", function () {
        if (Interface.Current == Interface.MAIN && !Player.isPsnRdInsertPlay && !Core.isFullScren && !About.isInsertPlay && !About.isTopInsertPlay && !About.isAtInsertPlay && !NtrlLngTop.isInsertPlay && !About.isSingleInsertPlay && (!Flw.isFlw || Flw.toid == "")) return $("#schBxCtn").click(), !1
    }), key("t", function () {
        Interface.Current == Interface.MAIN && !Core.isFullScren && $("#playerHate").click()
    }), key("x", function () {
        Interface.Current == Interface.MAIN && !Core.isFullScren && $("#playerLove").click()
    }), key("r", function () {
        Interface.Current == Interface.MAIN && !Core.isFullScren && $("#playerRptOne").click()
    }), key("space", function () {
        Interface.Current == Interface.MAIN && !$("#topApp").isDisplay() && (!Flw.isFlw || Flw.toid == "") && Player.playCtl()
    }), key("up", function () {
        if (Flw.isFlw && Flw.toid != "") {
            Gns.nowGns("你正在跟听状态中，将跟对方音量同步，不能自己调节");
            return
        }
        Interface.Current == Interface.MAIN && (Player.showVolume(), Player.volumeUp(), Player.hideVolume())
    }), key("down", function () {
        if (Flw.isFlw && Flw.toid != "") {
            Gns.nowGns("你正在跟听状态中，将跟对方音量同步，不能自己调节");
            return
        }
        Interface.Current == Interface.MAIN && (Player.showVolume(), Player.volumeDown(), Player.hideVolume())
    }), key("right", function () {
        if (Interface.Current == Interface.MAIN && !$("#topApp").isDisplay() && (!Flw.isFlw || Flw.toid == "") || Player.isPsnRdInsertPlay) Player.postNext(), $("#playerNext").click()
    }), key("esc", function () {
        return $("#topApp").isDisplay() ? $("#clsTop").click() : $("#shrGuide").isDisplay() ? $("#shrGuide>.cls").click() : $("#rlsNote").isDisplay() && $("#rlsNote>.cls").click(), !1
    })
});
var Main = {
    st: 0,
    init: function () {
        Now.init(), Message.init(), Player.init(), Search.init(), Player.load(), Apps.init(), Top.init(), Gns.init(), Cht.init(), Guide.init(), Flw.init(), Frd.init(), About.init(), NtrlLngTop.init(), Core.ipad && (fingerGestureListener($(".mscPlrMask")[0], function (e) {
            e.direction == "middle" && Player.playCtl(), (Interface.Current == Interface.MAIN && Signup.userDetail.avbF.stct != undefined && !$("#topApp").isDisplay() || Player.isPsnRdInsertPlay) && e.direction == "left" && Player.next()
        }), fingerGestureListener($(".slide")[0], function (e) {
            if (e.status == "start") {
                Top.ipadleft = $("#topApp .slide").offset().left;
                return
            }
            var t = Top.ipadleft + e.dx,
                n = 0,
                r = $("#topApp .slide").width() - Core.bodyWidth;
            e.status == "move" ? ($(".slide").removeClass("slideAnmt"), n = Core.bodyWidth / 4, r += Core.bodyWidth / 4) : e.status == "end" && ($(".slide").addClass("slideAnmt"), t += e.dx * .5), t > n && (t = n), -t > r && (t = -r), $("#topApp .slide").css("left", t + "px")
        })), $("#logo").click(function () {
            if (Signup.userDetail.newbie != 0) return;
            _gaq.push(["_trackEvent", "Toolbar", "Logo", "ClickToJingLogo"]), $.ajax({
                url: Core.API_VER + "/app/fetch_release",
                success: function (e) {
                    if (!e.sucess && e.result == null) return;
                    $("#rlsNote .rlsCtn .cnfmBtn").hide(), Guide.rlsShow(e.result)
                }
            })
        }), $("#rlsLeftBtn").click(function () {
            Main.st > 0 && (--Main.st, Main.jingClick())
        }), $("#rlsRightBtn").click(function () {
            ++Main.st, Main.jingClick()
        })
    },
    jingClick: function () {
        $.ajax({
            url: Core.API_VER + "/app/fetch_release",
            data: {
                st: Main.st
            },
            success: function (e) {
                if (!e.success) return;
                if (e.result == null) {
                    --Main.st;
                    return
                }
                var t = "",
                    n = e.result.content.split("\n");
                for (var r = 0; r < n.length; ++r) t == "" && (t += '<p class="rlsElm"></p>'), t += '<p class="rlsElm"><span class="rlsElmNum serif">' + (r + 1) + ".</span>" + n[r] + "</p>";
                $("#rlsTmst").html(e.result.released_at), $("#rlsTit").html("Jing.fm " + e.result.title), $("#rlsBd").html(t), $("#rlsCtn").css({
                    visibility: "hidden"
                }).show(), $("#rlsCtn").css({
                    "margin-top": "-" + ($("#rlsCtn").height() / 2 + 70) + "px",
                    visibility: "visible"
                })
            }
        })
    },
    hide: function (e) {
        $("#ppLsted").isDisplay() && $("#ppLsted").animate({
            opacity: "0"
        }, 300, function () {
            $(this).children(".ovlCttCtn").css({
                "margin-top": "-125px",
                height: "180px"
            }).children(".ovlCtt").remove(), $(this).hide()
        }), $("#mscInfo").isDisplay() && $("#mscInfo").animate({
            opacity: "0"
        }, 300, function () {
            $(this).hide(), $("#mscInfoCtn").children("div").empty(), $(".tabCtn").each(function () {
                $(this).removeClass("selected")
            })
        }), $("#mscMv").isDisplay() && $("#mscMv").animate({
            opacity: "0"
        }, 300, function () {
            $(this).hide(), $("#mvPlayer").html(), Player.isPlay || $("#playCtl").click(), Player.setVolumeUp()
        })
    }
}, Signup = {
        FAV_LIST: 15,
        MORE_POS: 10,
        more_POS: 14,
        MIN_HEIGHT: 750,
        ACTION: 0,
        SIGN_UP: 1,
        SIGN_IN: 2,
        CHECK_EMAIL: 3,
        isCheckEmail: 0,
        timeOutId: 0,
        tpsTmo: 0,
        onlineCount: 0,
        resetPwdToken: "",
        tkrsCount: 0,
        userDetail: {
            id: "",
            nick: "",
            fid: "",
            snsId: "",
            snsType: "",
            newbie: 0,
            c: 0,
            invitation: "",
            k: "",
            pld: null,
            sts: null,
            avbF: new Object,
            newview: new Object
        },
        init: function () {
            var e = function () {
                $.query.get("forgot") != "" ? (Signup.resetPwdToken = $.query.get("forgot"), $.ajax({
                    url: Core.API_VER + "/account/verify_forgot_token",
                    data: {
                        token: Signup.resetPwdToken
                    },
                    success: function (e) {
                        e.success || (Signup.resetPwdToken = ""), Signup.switchAnimate("resetPwdCtn")
                    }
                })) : $.query.get("invt") != "" && (Signup.userDetail.invitation = $.query.get("invt"))
            };
            Core.getCookie("jing.auth") != "" && Core.getCookie("jing.auth") != null ? $.ajax({
                url: Core.API_VER + "/sessions/validates",
                data: {
                    i: ""
                },
                success: function (t, n, r) {
                    t.success ? (clearTimeout(Signup.tpsTmo), Signup.setUserDetail(t.result, n, r), Signup.finishClick()) : e()
                }
            }) : e(), $(".bdgCtn").css({
                left: 800 + (Core.bodyWidth - 800) / 2 + "px"
            }), $("#bdgStr").css({
                left: -((Core.bodyWidth - 800) / 2 + 550) + "px"
            }), $("#fvtAtsCtn").css({
                bottom: -((Core.bodyHeight - 512) / 2 + 75 + 420) + "px"
            }), $("#bindCtn").css({
                bottom: -((Core.bodyHeight - 404) / 2 + 75 + 375) + "px"
            }), $("#cmpltSignupCtn").css({
                bottom: -((Core.bodyHeight - 335) / 2 + 75 + 305) + "px"
            }), $("#invt, #fgtCtn, #resetPwdCtn").css({
                bottom: -((Core.bodyHeight - 219) / 2 + 75 + 219) + "px"
            }), $(".signUpMenuBtn").bind("click", function () {
                $("#tps").hide();
                if ($("#signUpCtn").length == 0) return;
                var e, t;
                $(this).prop("id").indexOf("signUp") >= 0 ? (e = "invt", t = "signIn") : $(this).prop("id").indexOf("signIn") >= 0 ? (_gaq.push(["_trackEvent", "Login", "SignIn", "ClickToSignInButton"]), e = "signIn", t = "signUp") : (_gaq.push(["_trackEvent", "Login", "SignUp", "ClickToSignUpButton"]), e = "signUp", t = "signIn");
                if ($(this).hasClass("selected")) {
                    if (e == "invt" && $("#invt").isDisplay()) {
                        $("#bdgCtn").css({
                            top: $("#bdgCtn").offset().top,
                            "margin-top": "0px"
                        }), $("#bdgCtn").animate({
                            height: "70px",
                            top: (Core.bodyHeight - 70 - 75) / 2 + "px",
                            "margin-top": "0px"
                        }), $("#invt").animate({
                            bottom: -((Core.bodyHeight - 219) / 2 + 75 + 219) + "px"
                        }, 300, function () {
                            $(this).hide()
                        }), $(this).removeClass("selected");
                        return
                    }
                    $(this).removeClass("selected");
                    var n = !1,
                        r = $("#signUpCtn").offset().top,
                        i = $("#bdgCtn").offset().top;
                    $("#signUpCtn").animate({
                        bottom: "-300px"
                    }, 300, function () {
                        $("#signUpCtn").hide()
                    }), Core.bodyHeight < Signup.MIN_HEIGHT && $("#bdgCtn").animate({
                        top: (Core.bodyHeight - 70 - 75) / 2 + "px"
                    }, 300)
                } else $(this).addClass("selected"), $("#" + t).removeClass("selected"), Signup.signupAnimate(e, t);
                return !1
            }), $("#signUpEmail, #signInEmail, #invtEmail, #fgtEmail").blur(function () {
                $(".wb").removeClass("selected");
                var e = $(this),
                    t = $(this).val();
                if (t == "") return;
                if (t == "你的Email") $("#snsEmail").data("error", "邮箱不能为空");
                else if (!/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(t)) Signup.tpsShow($(this), "邮箱格式错误"), $(this).data("error", "邮箱格式错误");
                else {
                    $(this).data("error", "");
                    if ($(this).prop("id") == "signInEmail" || $(this).prop("id") == "fgtEmail") {
                        $("#tps").hide();
                        return
                    }
                    $.ajax({
                        url: Core.API_VER + "/account/check_email",
                        data: {
                            email: t
                        },
                        success: function (t) {
                            t.success ? ($("#tps").hide(), e.data("error", "")) : (e.data("error", "邮箱已经被人注册"), Signup.tpsShow(e, "邮箱已经被人注册"))
                        }
                    })
                }
            }), $("#resetPwd").prev().click(function () {
                $(this).next().focus()
            }), $("#resetPwd").focus(function () {
                $(this).prev().hide()
            }).blur(function () {
                $(this).val().length == 0 && $(this).prev().show()
            }).keyup(function (e) {
                e.keyCode == 13 && ($(this).blur(), $("#resetPwdBtn").click())
            }), $("#signUpPwd, #signInPwd").blur(function () {
                $(".wb").removeClass("selected");
                var e = $(this).val();
                e == "输入你的密码" || e.length == 0 ? $(this).data("error", "密码不能为空") : e.length < 6 ? ($(this).data("error", "密码必须6位以上"), Signup.tpsShow($(this), "密码必须6位以上")) : ($("#tps").hide(), $(this).data("error", ""))
            }).keyup(function (e) {
                e.keyCode == 13 && ($(this).blur(), $("#" + $(this).prop("id").replace("Pwd", "") + "Cmt").click())
            }), $("#signUpCmt").click(Signup.signUpCmtClick), $("#signInCmt").click(Signup.signInCmtClick), $(".rr, .wb, .qq, .db").click(function () {
                var e = $(this).prop("id"),
                    t = "",
                    n = "";
                e.indexOf("Renren") >= 0 ? (t = "renren", n = "extRr") : e.indexOf("Sina") >= 0 ? (t = "sina_weibo", n = "extWb") : e.indexOf("Douban") >= 0 ? (t = "douban", n = "extDb") : (t = "qq_weibo", n = "extQq");
                var r = snsWindowWh[n][0],
                    i = snsWindowWh[n][1],
                    s = (Core.screenWidth - r) / 2,
                    o = (Core.screenHeight - i - 68) / 2,
                    u = !1;
                Core.ipad && (u = !0);
                var a = Core.API_VER + "/oauth/proxyauthorize?identify=" + t + "&autoclose=" + u + "&standalone=" + Core.standalone;
                u ? window.open(a, "_blank", "width=" + r + "px, height=" + i + "px, left=" + s + "px, top=" + o + "px, directories=0, location=0, menubar=0, resizable=0, status=0, toolbar=0") : Signup.OpenWindow = window.open(a, "_blank", "width=" + r + "px, height=" + i + "px, left=" + s + "px, top=" + o + "px, directories=0, location=0, menubar=0, resizable=0, status=0, toolbar=0")
            }), Core.ipad || ($("#snsRenren, #snsSina, #snsQQ, #snsDouban").mouseenter(function () {
                if ($(this).hasClass("rgstBtn")) return;
                Signup.btnTpsShow($(this))
            }).mouseleave(function () {
                if ($(this).hasClass("rgstBtn")) return;
                var e = $(this).prop("id");
                btnDes[e] == $("#tps").children(".tpsCtn").children("p").text() && $("#tps").hide()
            }), $("#signIn, #signUp").mouseenter(function () {
                $("#tps").hide(), clearTimeout(Signup.tpsTmo);
                if ($(this).hasClass("selected") || $("#signUpCtn").isDisplay()) return;
                $(this).children(".tps").show();
                var e = "";
                $(this).prop("id") == "signIn" ? e = "登录" : e = "注册", Signup.btnTpsShow($(this), e)
            }).mouseleave(function () {
                if ($(this).hasClass("selected")) return;
                $("#tps").hide()
            })), $("#invtBtn").click(function () {
                if ($("#invtBtn").hasClass("selected")) return;
                $("#invtEmail").data("error") != "" && Signup.tpsShow($("#invtEmail"), $("#invtEmail").data("error")), $("#tps").isDisplay() || ($("#invtBtn").addClass("selected"), $.ajax({
                    url: Core.API_VER + "/account/post_invitation",
                    data: {
                        email: $("#invtEmail").val()
                    },
                    success: function (e) {
                        $("#invtEmail").val(""), $("#invtBtn").removeClass("selected"), e.success ? Signup.tpsShow($("#invtEmail"), "邮箱已记录，请耐心等待。") : Signup.tpsShow($("#invtEmail"), e.codemsg)
                    }
                }))
            }), $("#fgtBtn").click(function () {
                if ($("#fgtBtn").hasClass("selected")) return;
                $("#fgtEmail").data("error") != "" && Signup.tpsShow($("#fgtEmail"), $("#fgtEmail").data("error")), $("#tps").isDisplay() || ($("#fgtBtn").addClass("selected"), $.ajax({
                    url: Core.API_VER + "/account/forgot_password",
                    data: {
                        email: $("#fgtEmail").val()
                    },
                    success: function (e) {
                        $("#fgtEmail").val(""), $("#fgtBtn").removeClass("selected"), e.success ? Signup.tpsShow($("#fgtEmail"), "重置密码链接已发送，请查收。") : Signup.tpsShow($("#fgtEmail"), "发送失败，请重试。")
                    }
                }))
            }), $("#resetPwdBtn").click(function () {
                if ($("#resetPwdBtn").hasClass("selected")) return;
                if (Signup.resetPwdToken == "") return;
                $("#resetPwdBtn").addClass("selected"), $.ajax({
                    url: Core.API_VER + "/account/reset_password",
                    data: {
                        pwd: $("#resetPwd").val(),
                        token: Signup.resetPwdToken
                    },
                    success: function (e) {
                        $("#resetPwd").val(""), $("#resetPwdBtn").removeClass("selected"), e.success ? (Signup.tpsShow($("#resetPwd"), "密码修改成功，请登录。"), setTimeout(function () {
                            $("#signIn").click()
                        }, 3e3)) : Signup.tpsShow($("#resetPwd"), "密码修改失败，请重试。")
                    }
                })
            }), $("#fgtPwd").click(function () {
                _gaq.push(["_trackEvent", "Login", "FgtPwd", "ForgotPassword"]), $("#tps").hide(), $(".signUpMenuBtn").removeClass("selected"), Signup.switchAnimate("fgtCtn")
            });
            var t = 1,
                n = function () {
                    if (t == -1) {
                        clearTimeout(Signup.tpsTmo), $("#tps").hide("slow");
                        return
                    }
                    t ? (Signup.btnTpsShow($("#signUp"), "注册", "slow"), t = 0) : ($("#tps").hide("slow"), setTimeout(function () {
                        Signup.btnTpsShow($("#signIn"), "登录", "slow")
                    }, 800), t = -1)
                };
            Signup.tpsTmo = setInterval(n, 5e3), n()
        },
        btnTpsShow: function (e, t, n) {
            if (!e.hasClass("selected")) {
                $(".wb").removeClass("selected"), $("#signIn, #signUp").children(".tps").hide();
                var r = e.attr("id");
                n == undefined && clearTimeout(Signup.tpsTmo), t != undefined ? $("#tps").children(".tpsCtn").children("p").text(t) : $("#tps").children(".tpsCtn").children("p").text(btnDes[r]);
                var i = e.offset().left - $("#tps").width() / 2 + 15,
                    s = e.offset().top - 55;
                n != undefined ? $("#tps").css({
                    left: i,
                    top: s + "px"
                }).show("slow") : $("#tps").css({
                    left: i,
                    top: s + "px"
                }).show()
            }
        },
        signUpCmtClick: function () {
            if ($("#signUpCmt").hasClass("selected")) return;
            $("#signUpEmail").data("error") != "" ? Signup.tpsShow($("#signUpEmail"), $("#signUpEmail").data("error")) : $("#signUpPwd").data("error") != "" && Signup.tpsShow($("#signUpPwd"), $("#signUpPwd").data("error"));
            var e = $("#signUpEmail").val(),
                t = $("#signUpPwd").val();
            $("#tps").isDisplay() || (_gaq.push(["_trackEvent", "Login", "SignUpCmt", "FinishRegister"]), $("#signUpCmt").addClass("selected"), Signup.ACTION = Signup.SIGN_UP, $.ajax({
                url: Core.API_VER + "/account/create",
                data: {
                    email: e,
                    pwd: t,
                    i: Signup.userDetail.invitation
                },
                success: Signup.signupCallback
            }))
        },
        signInCmtClick: function () {
            if ($("#signInCmt").hasClass("selected")) return;
            $("#signInEmail").data("error") != "" && Signup.tpsShow($("#signInEmail"), $("#signInEmail").data("error")), $("#signInPwd").data("error") != "" && Signup.tpsShow($("#signInPwd"), $("#signInPwd").data("error"));
            var e = $("#signInEmail").val(),
                t = $("#signInPwd").val();
            $("#tps").isDisplay() || (_gaq.push(["_trackEvent", "Login", "SignInCmt", "LoginSuccessfully"]), $("#signInCmt").addClass("selected"), Signup.ACTION = Signup.SIGN_IN, $.ajax({
                url: Core.API_VER + "/sessions/create",
                data: {
                    email: e,
                    pwd: t
                },
                success: Signup.signupCallback
            }))
        },
        signupCallback: function (e, t, n) {
            if (!e.success) {
                $("#signUpFormCtn").isDisplay() ? Signup.tpsShow($("#signUpPwd"), e.codemsg) : $("#signInFormCtn").isDisplay() ? Signup.tpsShow($("#signInPwd"), e.codemsg) : Signup.tpsShow($("#snsPwd"), e.codemsg), $("#signUpCmt, #signInCmt, #bindBtn").removeClass("selected");
                return
            }
            Signup.setUserDetail(e.result, t, n), Signup.ACTION == Signup.SIGN_UP && (Signup.userDetail.c = 1), Signup.userDetail.snsType != "" && $.ajax({
                url: Core.API_VER + "/oauth/association",
                data: {
                    uid: Signup.userDetail.id,
                    identify: Signup.userDetail.snsType,
                    auid: Signup.userDetail.snsId
                },
                success: function (e) {
                    if ( !! e.success) {
                        Signup.userDetail.snsType == "qq_weibo" ? Signup.userDetail.snsType = "qq" : Signup.userDetail.snsType == "renren" ? Signup.userDetail.snsType = "rr" : Signup.userDetail.snsType == "sina_weibo" && (Signup.userDetail.snsType = "wb");
                        for (var t in e.result) Signup.userDetail.snstokens[t] = e.result[t];
                        for (var n = 0; n < resData.frdCt.length; ++n) for (var t in Signup.userDetail.snstokens) resData.frdCt[n].mid == ConverSns[t] && (resData.frdCt[n].display = "true")
                    }
                }
            }), Signup.userDetail.newbie == 1 ? Signup.cmpltAnimate() : Signup.userDetail.newbie == 2 ? Signup.bdgAnimate() : Signup.finishClick()
        },
        cmpltAnimate: function () {
            Core.ipad ? $("#uploadify").parent().html('<div class="uploadify-button"><span class="uploadify-button-text">iPad 不支持上传头像</span></div>') : swfobject.ua.pv[0] ? Core.uploadify() : $("#uploadify").parent().html('<div class="uploadify-button"><span class="uploadify-button-text">没装flash，不能上传头像</span></div>'), $(".signUpMenuBtn").removeClass("selected"), $("#tps").hide(), $("#signUpCtn").animate({
                bottom: "-300px"
            }, 300, function () {
                $("#signUpCtn").remove(), Core.bodyHeight < Signup.MIN_HEIGHT && $("#bdgCtn").css({
                    top: "50%",
                    marginTop: $("#bdgCtn").offset().top - Core.bodyHeight / 2 + "px"
                }), $("#bdgCtn").animate({
                    height: "335px",
                    marginTop: "-207px"
                }, 200, function () {
                    $("#cmpltSignupCtn").removeClass("hide").animate({
                        bottom: "0px"
                    }, 200)
                })
            }), $("#browseAvatar").click(function () {
                $("#uploadPhoto").click()
            }), $("#localNickName").blur(function () {
                var e = $(this).val(),
                    t = $(this).offset().left,
                    n = $(this).offset().top - 32;
                e.length == 0 || e == "你的昵称" ? ($("#tps").hide(), $(this).data("error", "昵称不能为空")) : /^[\u4E00-\u9FA5A-Za-z0-9_]+$/.test(e) ? e.length < 2 || e.length > 14 ? (Signup.tpsShow($(this), "昵称只能2-14个字符"), $(this).data("error", "昵称只能2-14个字符")) : ($("#tps").hide(), $(this).data("error", ""), $.ajax({
                    url: Core.API_VER + "/account/check_nick",
                    data: {
                        nick: e
                    },
                    success: function (e) {
                        e.success ? $("#localNickName").data("error", "") : e.code == "253" ? (Signup.tpsShow($("#localNickName"), "昵称已被使用"), $("#localNickName").data("error", "昵称已被使用")) : Signup.tpsShow($("#localNickName"), "Jing开小差了")
                    }
                })) : (Signup.tpsShow($(this), '支持中英文、数字、"_"'), $(this).data("error", '支持中英文、数字、"_"'))
            }).keyup(function (e) {
                e.keyCode == 13 && ($(this).blur(), $("#cmpltBtn").click())
            }), $("#cmpltBtn").click(function () {
                if ($("#cmpltBtn").hasClass("selected")) return;
                if ($("#uploadify-button").children("span").text() == "上传中，请稍侯...") {
                    Signup.tpsShow($("#localNickName"), "图片正在上传，请稍候再提交");
                    return
                }
                var e = $("#localNickName").val();
                $("#localNickName").data("error") != "" && Signup.tpsShow($("#localNickName"), $("#localNickName").data("error"));
                if ($("#tps").isDisplay() || e.length == 0) return !1;
                $("#cmpltBtn").addClass("selected"), $.ajax({
                    url: Core.API_VER + "/account/update_profile",
                    data: {
                        uid: Signup.userDetail.id,
                        nick: e,
                        isreg: "true"
                    },
                    success: function (t) {
                        t.success ? (Signup.userDetail.nick = e, Signup.userDetail.newbie = 2, Signup.finishClick()) : (Signup.tpsShow($("#localNickName"), t.codemsg), $("#cmpltBtn").removeClass("selected"))
                    }
                })
            })
        },
        authCallback: function (e) {
            if (!e.success) {
                alert(e.msg);
                return
            }
            if (e.result.association == undefined) {
                var t = e.result.identify;
                $.ajax({
                    url: Core.API_VER + "/oauth/bind",
                    data: {
                        uid: Signup.userDetail.id,
                        identify: t
                    },
                    success: function (e) {
                        if (!e.success) {
                            e.code == "806" ? Gns.nowGns("你的" + btnDes[t] + "已经绑定了别的账户，请换别的SNS。") : alert(e.msg);
                            return
                        }
                        if (ConverSns[t] != undefined) {
                            var n = ConverSns[ConverSns[t]],
                                r = $("a[data-mid='" + n + "']").parent();
                            r.children(".tpsMenuCtn").html('<a data-tps="更新" class="rndBtn updt updtEvent" href="#" data-mid="' + ConverSns[t] + '"></a><a class="rndBtn ext off" href="#" data-mid="' + ConverSns[t] + '"></a></div>'), r.prev("desc").html(FrdDes[ConverSns[t]])
                        }
                    }
                })
            } else e.result.association ? (Core.setCookie("jing.auth", e.result[Core.JingATokenHeader] + "," + e.result[Core.JingRTokenHeader]), $.ajaxSetup({
                    headers: {
                        "Jing-A-Token-Header": e.result[Core.JingATokenHeader],
                        "Jing-R-Token-Header": e.result[Core.JingRTokenHeader]
                    }
                }), Signup.setUserDetail(e.result), Signup.userDetail.newbie == 2 ? Signup.bdgAnimate() : Signup.finishClick()) : Signup.snsAnimate(e);
            Signup.OpenWindow && setTimeout(function () {
                Signup.OpenWindow.window.close(), Signup.OpenWindow = !1
            }, 2e3)
        },
        snsAnimate: function (e) {
            var t = e.result.nick,
                n = e.result.avatar,
                r = e.result.identify;
            Signup.userDetail.fid = n, Signup.userDetail.snsId = e.result.id, Signup.userDetail.snsType = r, $(".signUpMenuBtn").removeClass("selected"), $("#tps").hide();
            var i = "";
            $("#invt").isDisplay() ? i = "#invt" : i = "#signUpCtn", $(i).animate({
                bottom: "-500px"
            }, 300, function () {
                $("#signUpCtn").remove(), Core.bodyHeight < Signup.MIN_HEIGHT && $("#bdgCtn").css({
                    top: "50%",
                    marginTop: $("#bdgCtn").offset().top - Core.bodyHeight / 2 + "px"
                }), $("#bdgCtn").animate({
                    height: "404px",
                    marginTop: "-242px"
                }, 200, function () {
                    $("#bindCtn").removeClass("hide").animate({
                        bottom: "0px"
                    }, 500)
                }), $("#snsNickName").text("Hi, " + t), $("#snsInputNickName").val(t), $("#snsDesc").text("你已经成功绑定了" + btnDes[r] + "，现在你需要设置填写登录邮箱和密码就能开始享用Jing了。"), Core.imgLoad($("#snsAvt"), "", n, 64), $("#bindBtn").click(Signup.bindBtnClick), $("#snsEmail").blur(Signup.snsEmailBlur), $("#snsPwd").blur(function () {
                    var e = $(this).val();
                    e == "输入你的密码" ? (Signup.tpsShow($(this), "密码不能为空"), $(this).data("error", "密码不能为空")) : e.length < 6 ? (Signup.tpsShow($(this), "密码必须6位以上"), $(this).data("error", "密码必须6位以上")) : ($("#tps").hide(), $(this).data("error", ""))
                }).keyup(function (e) {
                    e.keyCode == 13 && ($(this).blur(), Signup.bindBtnClick())
                }), $("#snsInputNickName").blur(function () {
                    var e = $(this).val(),
                        t = $(this).offset().left,
                        n = $(this).offset().top - 32;
                    e.length == 0 || e == "你的昵称" ? ($("#tps").hide(), $(this).data("error", "昵称不能为空")) : /^[\u4E00-\u9FA5A-Za-z0-9_]+$/.test(e) ? Core.strLength(e) < 2 || Core.strLength(e) > 14 ? (Signup.tpsShow($(this), "昵称只能2-14个字符"), $(this).data("error", "昵称只能2-14个字符")) : ($("#tps").hide(), $(this).data("error", ""), $.ajax({
                        url: Core.API_VER + "/account/check_nick",
                        data: {
                            nick: e
                        },
                        success: function (e) {
                            e.success ? $("#snsInputNickName").data("error", "") : e.code == "253" ? (Signup.tpsShow($("#snsInputNickName"), "昵称已被使用"), $("#snsInputNickName").data("error", "昵称已被使用")) : Signup.tpsShow($("#snsInputNickName"), "Jing开小差了")
                        }
                    })) : (Signup.tpsShow($(this), '支持中英文、数字、"_"'), $(this).data("error", '支持中英文、数字、"_"'))
                })
            })
        },
        snsEmailBlur: function () {
            var e = $("#snsEmail").val(),
                t = $("#snsEmail").offset().left,
                n = $("#snsEmail").offset().top - 35;
            e == "你的Email" ? $("#snsEmail").data("error", "邮箱不能为空") : /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(e) ? ($("#tps").hide(), $("#snsEmail").data("error", ""), Signup.ACTION = Signup.CHECK_EMAIL, $.ajax({
                url: Core.API_VER + "/account/check_email",
                data: {
                    email: e
                },
                success: function (e) {
                    e.success || e.msg == "1" ? (e.success ? Signup.ACTION = Signup.SIGN_UP : Signup.ACTION = Signup.SIGN_IN, $.ajax({
                        url: Core.API_VER + "/account/check_nick",
                        data: {
                            nick: $("#snsInputNickName").val()
                        },
                        success: function (e) {
                            e.success || (e.code == "253" || e.code == "252" ? $("#bdgCtn").data("tag") != "nickname" ? ($("#bdgCtn").data("tag", "nickname"), $("#bdgCtn").animate({
                                height: "+=70",
                                marginTop: "-=35"
                            }, 300), $("#bindCtn").animate({
                                height: "+=70"
                            }, 300, function () {
                                $("#snsInputNickName").parent().show();
                                var t = $("#snsInputNickName").offset().left,
                                    n = $("#snsInputNickName").offset().top - 35;
                                e.code == "252" ? (Signup.tpsShow($("#snsInputNickName"), '支持中英文、数字、"_"或"-"'), $("#snsInputNickName").data("error", '支持中英文、数字、"_"或"-"')) : (Signup.tpsShow($("#snsInputNickName"), "昵称已被使用"), $("#snsInputNickName").data("error", "昵称已被使用"))
                            })) : $("#snsInputNickName").data("error", "") : Signup.tpsShow($("#snsPwd"), "Jing开小差了"))
                        }
                    })) : Signup.ACTION = Signup.SIGN_IN
                }
            })) : (Signup.tpsShow($("#snsEmail"), "邮箱格式错误"), $("#snsEmail").data("error", "邮箱格式错误"))
        },
        bindBtnClick: function () {
            if ($("#bindBtn").hasClass("selected")) return;
            $("#snsEmail").data("error") != "" ? Signup.tpsShow($("#snsEmail"), $("#snsEmail").data("error")) : $("#snsPwd").data("error") != "" && Signup.tpsShow($("#snsPwd"), $("#snsPwd").data("error"));
            if ($("#tps").isDisplay()) return;
            var e = $("#snsInputNickName").val();
            Signup.userDetail.nick = e;
            if (Signup.ACTION == Signup.CHECK_EMAIL) Signup.tpsShow($("#snsPwd"), "正在检验您的email，请稍候");
            else if (Signup.ACTION == Signup.SIGN_IN) $("#bindBtn").addClass("selected"), $.ajax({
                    url: Core.API_VER + "/sessions/create",
                    data: {
                        email: $("#snsEmail").val(),
                        pwd: $("#snsPwd").val()
                    },
                    success: function (t, n, r) {
                        t.success ? (Signup.setUserDetail(t.result, n, r), Signup.userDetail.newbie == 1 ? $.ajax({
                            url: Core.API_VER + "/account/update_profile",
                            data: {
                                uid: Signup.userDetail.id,
                                nick: e,
                                isreg: "true"
                            },
                            success: function (e, t, n) {
                                e.success ? Signup.signupCallback(e, t, n) : ($("#bindBtn").removeClass("selected"), alert(e.codemsg))
                            }
                        }) : Signup.signupCallback(t, n, r)) : ($("#bindBtn").removeClass("selected"), Signup.tpsShow($("#snsPwd"), t.codemsg))
                    }
                });
            else if (Signup.ACTION == Signup.SIGN_UP) {
                $("#snsInputNickName").data("error") != "" && $("#snsInputNickName").data("error") != undefined && Signup.tpsShow($("#snsInputNickName"), $("#snsInputNickName").data("error"));
                if ($("#tps").isDisplay()) return;
                $.ajax({
                    url: Core.API_VER + "/account/create",
                    data: {
                        email: $("#snsEmail").val(),
                        pwd: $("#snsPwd").val(),
                        nick: e,
                        i: Signup.userDetail.invitation
                    },
                    success: Signup.signupCallback
                })
            }
        },
        bdgAnimate: function () {
            $("#tps").hide();
            var e = function () {
                Signup.finishClick()
            };
            if ($("#bindCtn").isDisplay()) {
                var t = Core.bodyHeight - $("#bdgCtn").offset().top + 130;
                $("#bindCtn").animate({
                    bottom: "-" + t + "px"
                }, 300, function () {
                    e()
                })
            } else if ($("#signUpCtn").isDisplay()) $("#signUp").removeClass("selected"), $("#signUpCtn").animate({
                    bottom: "-300px"
                }, 300, function () {
                    $("#signUpCtn").remove(), Core.bodyHeight < Signup.MIN_HEIGHT && $("#bdgCtn").css({
                        top: "50%",
                        marginTop: $("#bdgCtn").offset().top - Core.bodyHeight / 2 + "px"
                    }), e()
                });
            else if ($("#cmpltSignupCtn").isDisplay()) {
                var t = Core.bodyHeight - $("#cmpltSignupCtn").offset().top + 130;
                $("#cmpltSignupCtn").animate({
                    bottom: "-" + t + "px"
                }, 300, function () {
                    e()
                })
            }
            $("#bdg1, #bdg2, #bdg3").click(Signup.bdg1Click)
        },
        bdg1Click: function () {
            var e = 0,
                t = 0,
                n = "",
                r = function (e) {
                    $.ajax({
                        url: Core.API_VER + "/account/filter/fetch_ats",
                        data: {
                            st: e,
                            ps: "13"
                        },
                        success: function (e) {
                            var t = 0;
                            $("#signupFvtAts").children().each(function () {
                                if ($(this).children(".refresh").length == 0 && $(this).children(".finish").length == 0) {
                                    var n = e.result.items[t].n,
                                        r = e.result.items[t].fid,
                                        s = e.result.items[t].aid;
                                    r.indexOf(".") >= 0 ? r = $.id2url(r, "SS", "artist") : r = Core.badgesUrl(r, 64), $(this).children("span").text(n), $(this).children(".avtMask").prop("id", s), $(this).children(".avt").css({
                                        "background-image": "url('" + r + "')"
                                    }), $(this).children(".selected").remove(), ++t
                                }
                            })
                        }
                    })
                };
            $("#mscGnCtn").animate({
                left: -(800 + (Core.bodyWidth - 800) / 2)
            }, 450, function () {
                $("#mscGnCtn").addClass("hide"), $("#bdgCtn").animate({
                    height: "512px",
                    marginTop: "-299px"
                }, 300), $("#fvtAtsCtn").removeClass("hide"), $("#fvtAtsCtn").animate({
                    bottom: "290px"
                }, 500, function () {
                    $("#fvtAtsCtn").animate({
                        bottom: "0px",
                        height: "390px"
                    }, 300, function () {
                        $("#tps .tpsCtn .ctt").text("选择你喜欢的艺人"), $("#tps").css({
                            left: Core.bodyWidth / 2 - 90 + "px",
                            top: $(this).offset().top - 34
                        }).show()
                    })
                }), $.ajax({
                    url: Core.API_VER + "/account/filter/fetch_ats",
                    data: {
                        st: "0",
                        ps: "13"
                    },
                    success: function (i) {
                        t = i.result.total;
                        var s = "";
                        Core.ie68 || (s = "dspr");
                        var o = '<div id="signupFvtAts" class="fvtAts hide ' + s + '">';
                        for (var u = 0, a = 0; u < Signup.FAV_LIST; ++u) {
                            var f = "",
                                l = "",
                                c = "",
                                h = "";
                            if (u == 10) l = "更多艺人", c = "refresh";
                            else if (u == 14) l = "完成", c = "finish";
                            else {
                                l = i.result.items[a].n, f = i.result.items[a].fid;
                                var p = i.result.items[a].aid;
                                f.indexOf(".") >= 0 ? f = $.id2url(f, "SS", "artist") : f = Core.badgesUrl(f, 64), h = "favIconClick", ++a
                            }
                            f != "" && (f = '<div class="avt" style="background:#e1e1e1 url(' + f + ') no-repeat scroll; background-size:64px 64px;"></div>'), o += '<div class="iconCtn"><a id="' + p + '" href="#" class="avtMask overlay ' + c + " " + h + '"></a>' + f + "<span>" + l + "</span>" + "</div>"
                        }
                        o += "</div>", $("#fvtAtsCtn").html(o);
                        var d = setInterval(function () {
                            $("#fvtAtsCtn").height() > 380 && (clearTimeout(d), $("#fvtAtsCtn").children(".fvtAts").removeClass("hide"), Core.ie68 || $("#fvtAtsCtn").children(".fvtAts").animate({
                                opacity: "1"
                            }, 600))
                        }, 100);
                        $(".favIconClick").click(function () {
                            $("#tps").hide();
                            var e = $(this).prop("id");
                            $(this).parent().children(".selected").length == 0 ? (n += e + ",", $(this).parent().append('<div class="selected"></div>')) : (n = n.replace(e + ",", ""), $(this).parent().children(".selected").remove())
                        }), $(".fvtAts").children(".iconCtn").children(".finish").click(function () {
                            if (n == "") {
                                $("#tps .tpsCtn .ctt").text("至少选择1个艺人"), $("#tps").css({
                                    left: Core.bodyWidth / 2 - 88 + "px",
                                    top: $("#fvtAtsCtn").offset().top - 34
                                }).show();
                                return
                            }
                            n = n.substring(0, n.length - 1), Guide.aids = n, Signup.userDetail.newbie = 3, $.ajax({
                                url: Core.API_VER + "/account/filter/post_ats",
                                data: {
                                    uid: Signup.userDetail.id,
                                    aids: n
                                }
                            }), Signup.finishClick()
                        }), $(".fvtAts").children(".iconCtn").children(".refresh").click(function () {
                            e += 13, e + 13 > t && (e = 0), r(e)
                        })
                    }
                })
            })
        },
        finishClick: function () {
            $("body").removeClass("login").addClass("main"), $("#tps").hide(), Interface.Current = Interface.MAIN, $("#signUpCtn, #signUpMenuCtn, #fgtPwd").remove(), $("#tbCtn").removeClass("gnBd"), Signup.userDetail.newbie != 0 && Core.fullScrenMenuShow("你正在入门引导环节", "signup"), $("#mdCtn").animate({
                opacity: "0"
            }, 600, function () {
                $("body").addClass("ptrn"), $(this).remove(), Main.init(), $("html").addClass(Signup.userDetail.sts.thm + "Thm"), $("#lgtBtn").click(function () {
                    _gaq.push(["_trackEvent", "Main", "Lgt", "LogoutSuccessfully"]), Core.setCookie("jing.auth", ""), window.location.href = "/"
                }), Signup.userDetail.newbie == 0 && (setTimeout(function () {
                    var e = Core.getCookie("snsUpdateTime");
                    if (e == "" || e == null) Core.setCookie("snsUpdateTime", (new Date).getTime());
                    else {
                        e = parseInt(e);
                        if (Apps.offsetDate(e, "snsUpdate") == 1) {
                            var t = resData.frdCt,
                                n = "";
                            for (var r = 0; r < t.length; ++r) {
                                if (t[r].display != "true") continue;
                                t[r].mid == "rr" ? n += "人人网、" : t[r].mid == "qq" ? n += "腾讯微博、" : t[r].mid == "wb" && (n += "新浪微博、")
                            }
                            n != "" && (n = "你已经7天没有更新" + n.substring(0, n.length - 1) + '，<a href="#" id="gnsSnsUpdate" class="trg">立即更新</a>', Gns.nowGns(n))
                        }
                    }
                }, 15e3), setTimeout(function () {
                    $.ajax({
                        url: Core.API_VER + "/message/fetch_offline_messages",
                        data: {
                            uid: Signup.userDetail.id
                        },
                        success: function (e) {
                            var t = e.result.items,
                                n = e.result.chats,
                                r = new Array,
                                i = "",
                                s = "",
                                o = 0;
                            for (var u = 0; u < t.length; ++u) t[u].t == "flwd" ? t[u].flw_id == Signup.userDetail.id ? i == "" ? (i = t[u], i.flwer += "、") : i.flwer += t[u].flwer + "、" : ++o : t[u].t == "inhs" ? s == "" ? (s = t[u], s.frd += "、") : s.frd += t[u].frd + "、" : r[r.length] = t[u];
                            i != "" && (i.flwer = i.flwer.substring(0, i.flwer.length - 1), r[r.length] = i), s != "" && (s.frd = s.frd.substring(0, s.frd.length - 1), r[r.length] = s), t = r;
                            for (var u = 0; u < t.length; ++u) Message.send(t[u], !1);
                            var a = "";
                            if (o != 0) {
                                var f = n.length;
                                n[f] = new Object, n[f].uid = "0", n[f].nick = "系统消息", n[f].count = o, a = '未读的 <a href="#" class="trg gnsSysEvent">' + n[f].nick + " (" + n[f].count + ")</a>"
                            }
                            var l = "";
                            for (var u = 0; u < n.length; ++u) {
                                Cht.offlineCount += n[u].count, Cht.offlineMes[n[u].uid] = new Object, Cht.offlineMes[n[u].uid].nick = n[u].nick, Cht.offlineMes[n[u].uid].count = n[u].count;
                                if (n[u].uid == "0") {
                                    Cht.sysOfflineCount = n[u].count;
                                    continue
                                }
                                l += '<a href="#" class="trg offChtNickEvent" data-uid="' + n[u].uid + '" data-nick="' + n[u].nick + '" data-avatar="' + n[u].avatar + '" data-ol="' + n[u].ol + '">' + n[u].nick + " (" + n[u].count + ")</a>、"
                            }
                            n.length == 1 && a != "" ? (a = "你有" + a, a += '，<a href="#" class="trg gnsSysEvent">去看看？</a>', Gns.nowGns(a)) : Cht.offlineCount != 0 && (a != "" && (a = "，还有" + a), l = l.substring(0, l.length - 1), l += " 给你发送了消息" + a + '，<a href="#" class="trg offChtListEvent">去看看？</a>', Gns.nowGns(l))
                        }
                    })
                }, 1e4), Signup.userDetail.sts.hbr == "false" && Core.getCookie("highKnow") != "true" && setTimeout(function () {
                    Gns.nowGns('你现在在使用低品质音乐，如果想恢复高品质音乐，打开个人主页进行设置。<a href="#" class="trg knowEvent" data-cookieid="highKnow">不再提醒设置</a>。')
                }, 7e3)), setInterval(Signup.aboutCountShow, 1e4)
            })
        },
        aboutCountShow: function () {
            var e = Signup.tkrsCount + Cht.offlineCount + Signup.onlineCount;
            Signup.userDetail.sts.frdCntNtf == "false" && (e -= Signup.onlineCount), e > 99 && (e = "...");
            if (e == 0) {
                $("#aboutCount").hide();
                return
            }
            if ($("#aboutCount").isDisplay()) $("#aboutCount").text(e);
            else if (parseInt($("#aboutCount").text()) != e && e != 0) {
                if (About.ouid == Signup.userDetail.id) return;
                $("#aboutCount").text(e), $("#aboutNewIcon").isDisplay() || Signup.iconShake("aboutCount")
            }
        },
        iconShake: function (e) {
            if (e.indexOf("New") >= 0) {
                if ($("#" + e).isDisplay()) return;
                var t = function () {
                    $("#" + e).animate({
                        top: "-=8"
                    }, 300, function () {
                        $(this).animate({
                            top: "+=8"
                        }, 300, function () {
                            $(this).isDisplay() && t()
                        })
                    })
                };
                $("#" + e).show(), t()
            } else $("#" + e).show().animate({
                    top: "-=15"
                }, 400, function () {
                    $(this).animate({
                        top: "+=15"
                    }, 300, function () {
                        $(this).animate({
                            top: "-=7"
                        }, 200, function () {
                            $(this).animate({
                                top: "+=7"
                            }, 150)
                        })
                    })
                })
        },
        tpsShow: function (e, t) {
            $(".wb").removeClass("selected"), $("#signIn, #signUp").children(".tps").hide();
            var n = e.offset().left,
                r = e.offset().top - 40;
            if (e.prop("id") == "invtEmail" || e.prop("id") == "fgtEmail" || e.prop("id") == "resetPwd") r -= 5;
            $("#tps").css({
                left: n,
                top: r + "px"
            }).children(".tpsCtn").children("p").text(t), $("#tps").show()
        },
        signupAnimate: function (e, t) {
            if (e == "invt") {
                $("#invtBtn").removeClass("selected"), Signup.switchAnimate("invt");
                return
            }
            var n = function () {
                $("#signUpCtn").isDisplay() ? ($("#" + e + "FormCtn").show(), $("#" + t + "FormCtn").hide()) : ($("#signUpCtn").show(), $("#" + t + "FormCtn").hide(), $("#" + e + "FormCtn").show(), $("#signUpCtn").animate({
                    bottom: "70px"
                }, 300, function () {
                    if (!$("#invt").isDisplay() && !$("#fgtCtn").isDisplay() && !$("#resetPwdCtn").isDisplay()) {
                        var t = $("#signUpCtn").offset().top;
                        Core.bodyHeight < Signup.MIN_HEIGHT && $("#bdgCtn").animate({
                            top: t - 77 + "px"
                        }, 300)
                    }
                    var n = $("#" + e + "FormCtn .head .right .wb");
                    setTimeout(function () {
                        Signup.btnTpsShow(n, "你还可以通过SNS登录"), n.addClass("selected")
                    }, 0)
                })), e == "signUp" ? $("#signUpCtn").children(".ar").animate({
                    right: "242px"
                }) : $("#signUpCtn").children(".ar").animate({
                    right: "191px"
                })
            };
            $("#invt").isDisplay() || $("#fgtCtn").isDisplay() || $("#resetPwdCtn").isDisplay() ? ($(this).removeClass("selected"), $("#invt, #fgtCtn").animate({
                bottom: -((Core.bodyHeight - 219) / 2 + 75 + 219) + "px"
            }, 300, function () {
                $(this).hide()
            }), $("#resetPwdCtn").animate({
                bottom: -((Core.bodyHeight - 219) / 2 + 75 + 219) + "px"
            }, 300, function () {
                $(this).hide();
                if (!$("#signUpCtn").isDisplay() && Core.bodyHeight < Signup.MIN_HEIGHT) {
                    var e = $("#bdgCtn").offset().top;
                    $("#bdgCtn").css({
                        top: e + "px",
                        "margin-top": "0px",
                        height: "70px"
                    }), $("#bdgCtn").animate({
                        top: Core.bodyHeight - 341 - 77 + "px"
                    }, 300)
                } else $("#bdgCtn").animate({
                        height: "70px",
                        marginTop: "-73px",
                        top: "50%"
                    }, 300);
                n()
            })) : n()
        },
        switchAnimate: function (e) {
            $("#signUpCtn").isDisplay() && $("#signUpCtn").animate({
                bottom: "-300px"
            }, 300, function () {
                $("#signUpCtn").hide()
            });
            var t = 349;
            $("#invt, #fgtCtn").animate({
                bottom: -((Core.bodyHeight - 219) / 2 + 75 + 219) + "px"
            }, 300, function () {
                $(this).hide()
            }), e == "invt" && (t = 375), $("#bdgCtn").height() == t ? (e != "invt" && $("#signUp").removeClass("selected"), $("#resetPwdCtn").animate({
                bottom: -((Core.bodyHeight - 219) / 2 + 75 + 219) + "px"
            }, 300, function () {
                $(this).hide(), $("#" + e).show().animate({
                    bottom: "0px"
                }, 300, function () {
                    e == "resetPwdCtn" && Signup.resetPwdToken == "" && Signup.tpsShow($("#resetPwd"), "修改密码已经过期，请重试")
                })
            })) : (Core.bodyHeight < Signup.MIN_HEIGHT && $("#bdgCtn").css({
                top: "50%",
                "margin-top": $("#bdgCtn").offset().top - Core.bodyHeight / 2 + "px"
            }), $("#bdgCtn").animate({
                height: t + "px",
                "margin-top": "-215px"
            }, 300, function () {
                $("#" + e).show().animate({
                    bottom: "0px"
                }, 300, function () {
                    e == "resetPwdCtn" && Signup.resetPwdToken == "" && Signup.tpsShow($("#resetPwd"), "修改密码已经过期，请重试")
                })
            }))
        },
        resize: function () {
            if ($("#mscGnCtn").isDisplay() || $("#fvtAtsCtn").isDisplay() || $("#bindCtn").isDisplay() || $("#cmpltSignupCtn").isDisplay() || $("#invt").isDisplay() || $("#fgtCtn").isDisplay()) return;
            if ($("#signUpCtn").isDisplay()) {
                var e = $("#signUpCtn").offset().top;
                Core.bodyHeight < Signup.MIN_HEIGHT ? $("#bdgCtn").css({
                    "margin-top": "0px",
                    top: e - 77 + "px"
                }) : $("#bdgCtn").css({
                    "margin-top": "-73px",
                    top: "50%"
                })
            } else Core.bodyHeight < Signup.MIN_HEIGHT ? $("#bdgCtn").css({
                    "margin-top": "0px",
                    top: (Core.bodyHeight - 70 - 75) / 2 + "px"
                }) : $("#bdgCtn").css({
                    "margin-top": "-73px",
                    top: "50%"
                }), $("#bdgCtn").removeClass("hide");
            $("#ling").removeClass("hide")
        },
        setUserDetail: function (e, t, n) {
            if (n != undefined) {
                var r = n.getResponseHeader(Core.JingATokenHeader),
                    i = n.getResponseHeader(Core.JingRTokenHeader);
                Core.setCookie("jing.auth", r + "," + i), $.ajaxSetup({
                    headers: {
                        "Jing-A-Token-Header": r,
                        "Jing-R-Token-Header": i
                    }
                })
            }
            Signup.userDetail.pld = e.pld;
            if (e.avbF != null) {
                var s = e.avbF.split(",");
                for (var o = 0; o < s.length; ++o) Signup.userDetail.avbF[s[o]] = !0
            }
            Signup.userDetail.id = e.usr.id, Signup.userDetail.nick = e.usr.nick, Signup.userDetail.newbie = e.usr.newbie, Signup.userDetail.c = e.usr.c, Signup.userDetail.sid = e.usr.sid, e.usr.avatar == null && (e.usr.avatar = ""), Signup.userDetail.fid = e.usr.avatar, Signup.userDetail.fidtiny = e.usr.avatartiny, Signup.userDetail.sts = e.sts, Signup.userDetail.snstokens = e.snstokens;
            for (var o = 0; o < resData.frdCt.length; ++o) for (var u in e.snstokens) resData.frdCt[o].mid == ConverSns[u] && (resData.frdCt[o].display = "true");
            Core.ipad && (Signup.userDetail.sts.tipNtf = undefined), Signup.userDetail.newview = e.newview, Signup.userDetail.newview.b = new Array, e.release != null && Signup.userDetail.newbie == 0 && setTimeout(function () {
                Guide.rlsShow(e.release)
            }, 2e4), Signup.userDetail.sts.lgA == "true" && $("html").addClass("ftlg")
        }
    }, Guide = {
        step: 1,
        result: null,
        mid: "",
        obj: null,
        isDisplayAppsCtn: !1,
        isDisplayMscSchCtn: !1,
        aids: "",
        isSmlAnimate: !1,
        start: "",
        shrData: null,
        oldStart: 0,
        atCount: 0,
        init: function () {
            $("#step1Tip, #step2Tip, #step3Tip, #step1Btn, #step2Btn").css("left", "-120px"), $("#step1Ds, #step2Ds, #step3Ds").css("left", "-570px"), Search.setSchHint(), Core.ie68 && $("#guide").removeClass("dspr"), setInterval(function () {
                $.ajax({
                    url: Core.API_VER + "/app/polling_fetch_release",
                    data: {
                        uid: Signup.userDetail.id
                    },
                    success: function (e) {
                        if (!e.sucess && e.result == null) return;
                        $("#rlsNote .rlsCtn .cnfmBtn").show(), Guide.rlsShow(e.result)
                    }
                })
            }, 18e5), $("#rlsNote .cnfm").click(function () {
                window.location.reload(!0)
            }), $("#rlsNote .cncl").click(function () {
                $("#rlsNote .cls").click()
            }), $("#rlsNote").children(".cls").click(function () {
                return $("#maskUserMenu").hide(), $("#tbCtn").removeClass("gnBd"), $("#rlsNote").animate({
                    opacity: "0"
                }, 300, function () {
                    $(".drgbEvt").show(), $("#mscPlrCtn").css({
                        "z-index": "1002"
                    }), Guide.isShow(), $(this).hide(), Core.isFullScren = !1
                }), !1
            }), $("#shrCncl, #shrGuide .cls").click(function () {
                return Guide.shrClose(!0), !1
            }), $("#shrCnfm").click(function () {
                _gaq.push(["_trackEvent", "Player", "Shr" + ConverSns[Guide.shrData.identify].toUpperCase(), Player.music[Player.pos].tid]), Guide.shrClose(!1);
                var e = $.trim($("#shrTextarea").val());
                e.length != 0 && (Guide.shrData.c = e), Guide.shrSend()
            }), $("#shrTextarea").autoTextarea({
                maxHeight: 160
            }), $("#shrTextarea").keydown(function (e) {
                if (e.keyCode == 27) return $("#shrGuide>.cls").click(), !1;
                if ($(this).val().length > 70 && e.keyCode != 8 && (e.keyCode < 37 || e.keyCode > 40)) return !1;
                if ((e.keyCode == 32 || e.keyCode == 13) && $("#atSgstCtn").isDisplay()) {
                    var t = $(this).val(),
                        n = $(this).val();
                    n = n.substring(0, Guide.oldStart);
                    for (var r = n.length - 1; r >= 0; --r) if (n[r] == "@") {
                            var n = t.substring(0, r + 1) + $("#atSgstCtn").find("a").first().text() + " ",
                                i = n.length;
                            n += t.substring(Guide.oldStart), $("#shrTextarea").val(n).focus().setCursorPosition(i), $("#atSgstCtn").hide();
                            break
                        }
                    return !1
                }
            }).keyup(function (e) {
                var t = $(this).val();
                t.length > 70 && e.keyCode != 8 && (e.keyCode < 37 || e.keyCode > 40) ? $(this).val(t.substring(0, 70)) : Guide.shrData.identify == "sina_weibo" && Guide.atAuto()
            }), $("#appGuide").children(".cls").click(function () {
                Player.pauseMsgTone(), Guide.appGideHide($("#appGuide"))
            }), $("#smlGuideKnow").click(function () {
                Guide.start == "guide" ? (Guide.psnRdGuideShow(), $("#smlGuide").hide()) : Guide.appGideHide($("#smlGuide")), Player.pauseMsgTone()
            }), $("#psnRdGuideKnow").click(function () {
                Guide.appGideHide($("#psnRdGuide")), Player.pauseMsgTone()
            }), $("#acptBtn").click(function () {
                Player.pauseMsgTone();
                if ($(this).hasClass("selected")) {
                    $("#appGuide").children(".cls").click();
                    return
                }
                Guide.obj.prev(".acceptTaskEvt").length == 1 && (Guide.obj.prev(".acceptTaskEvt").click(), $("#acptBtn").html("已接受").addClass("selected"))
            })
        },
        shrClose: function (e) {
            $("#atSgstCtn").hide(), $("#maskUserMenu").hide(), $("#tbCtn").removeClass("gnBd");
            var t = Guide.shrData.identify;
            $("#shrGuide").animate({
                opacity: "0"
            }, 300, function () {
                $("#shrTextarea").prop("style", ""), $("#shrTextarea").parent().prop("style", ""), $("#shrBoxFtr").removeClass(ConverSns[t]), $(".drgbEvt").show(), $("#mscPlrCtn").css({
                    "z-index": "1002"
                }), Guide.isShow(), $(this).hide(), Core.isFullScren = !1, e && (Guide.shrData = null)
            })
        },
        atAuto: function () {
            ++Guide.atCount, Guide.atCount == 100 && (Guide.atCount = 0);
            var e = Guide.atCount,
                t = Guide.oldStart,
                n = $("#shrTextarea").val(),
                r = n.substring(0, Guide.oldStart);
            for (var i = r.length - 1; i >= 0; --i) {
                if (r[i] == " ") break;
                if (r[i] == "@") {
                    var s = i,
                        o = TP.getInputPositon(document.getElementById("shrTextarea"));
                    r = r.substring(i + 1);
                    if (r == "") break;
                    $.getJSON("https://api.weibo.com/2/search/suggestions/at_users.json?access_token=" + Signup.userDetail.snstokens.sina_weibo + "&q=" + r + "&type=0&count=3&callback=?", function (r) {
                        if (Gns.isAnimate || r.data.length == 0 || e != Guide.atCount) {
                            $("#atSgstCtn").hide();
                            return
                        }
                        var i = r.data,
                            u = '<div class="atSgstBg"><div class="atSgstLogo wb"></div>',
                            a = 0;
                        for (; a < i.length; ++a) u += '<a href="#" class="atSgst">' + i[a].nickname + "</a>", a != i.length - 1 && (u += '<span class="splt"></span>');
                        u += '</div><div class="atSgstArr"></div>', $("#atSgstCtn").html(u);
                        var f = 37 * (a + 1) + a * 2;
                        $("#atSgstCtn").css({
                            left: o.left - 18 - 75 + "px",
                            top: o.bottom - 17 - f + "px"
                        }).show(), $(".atSgst").click(function () {
                            var e = n.substring(0, s + 1) + $(this).text() + " ",
                                r = e.length;
                            e += n.substring(t), $("#shrTextarea").val(e).focus().setCursorPosition(r), $("#atSgstCtn").hide()
                        })
                    });
                    return
                }
            }
            $("#atSgstCtn").hide()
        },
        update: function (e) {
            return Guide.oldStart = Core.getSelectionStart(e), !0
        },
        isShow: function () {
            Guide.isDisplayAppsCtn && ($("#appsCtn, #rightAr").show(), Guide.isDisplayAppsCtn = !1, $("#about").addClass("selected"))
        },
        isHide: function () {
            $("#appsCtn").isDisplay() && (Guide.isDisplayAppsCtn = !0, $("#appsCtn, #rightAr").hide())
        },
        rlsShow: function (e) {
            if (!Core.isFullScren) {
                var t = e.content.split("；"),
                    n = "";
                for (var r = 0; r < t.length; ++r) n += "<li>" + t[r] + "</li>";
                $("#rlsNote .rlsCtn .rlsCttCtn").html(n), $("#rlsNote .rlsCtn .rlsTit .rlsVer").html(e.title), Interface.Current == Interface.SEARCH && Search.hide(), $(".drgbEvt").hide(), $("#maskUserMenu").show(), $("#tbCtn").addClass("gnBd"), Core.isFullScren = !0, $("#mscPlrCtn").css({
                    "z-index": "999"
                }), Guide.isHide(), $("#rlsNote").show(), Guide.resize(), $("#rlsNote").animate({
                    opacity: "0.95"
                }, 300)
            }
        },
        shrSend: function (e) {
            e != "false" && Gns.nowGns("正在分享...");
            var t = function () {
                Gns.nowGns("你已成功分享到" + btnDes[Guide.shrData.identify]), Guide.shrData = null
            }, n = !1,
                r = !1;
            setTimeout(function () {
                n = !0, r && t()
            }, 3e3), $.ajax({
                url: Core.API_VER + "/oauth/music/share",
                data: Guide.shrData,
                success: function (e) {
                    if (e.success) {
                        n ? t() : r = !0;
                        return
                    }
                    e.code == "802" ? Gns.nowGns("你的" + btnDes[Guide.shrData.identify] + '绑定已过期，<a data-identify="' + ConverSns[Guide.shrData.identify] + '" href="#" class="trg gnsSnsBind">请重新绑定</a>') : e.code == "804" ? Gns.nowGns("访问" + btnDes[Guide.shrData.identify] + "的时候超时，请重试") : Gns.nowGns("分享失败"), Guide.shrData = null
                }
            })
        },
        shrShow: function () {
            Interface.Current == Interface.SEARCH && Search.hide(), $(".drgbEvt").hide(), $("#maskUserMenu").show(), $("#tbCtn").addClass("gnBd"), Core.isFullScren = !0, $("#mscPlrCtn").css({
                "z-index": "999"
            }), Guide.isHide(), $("#shrBoxFtr").addClass(ConverSns[Guide.shrData.identify]);
            var e = Signup.userDetail.fid;
            Signup.userDetail.fidtiny != undefined ? e = Signup.userDetail.fidtiny : e.indexOf("http://") != 0 ? e = $.id2url(e, "US", "avatar") : e == "" && (e = IMG_URL + "/defaults/avatar/" + Core.A50 + ".jpg"), $("#shrAvt").prop("src", e), $("#shrGuide").show(), $("#shrTextarea").focus(), $("#shrTextarea").val(Guide.shrData.c), Guide.resize(), $("#shrGuide").animate({
                opacity: "0.95"
            }, 300)
        },
        step1: function () {
            Guide.start = "guide", Guide.isSmlAnimate = !1, $("#tbCtn").addClass("gnBd"), Guide.isHide(), Interface.Current == Interface.SEARCH && Search.hide(), Guide.resize(), $("#maskUserMenu").show(), $("#mscPlrCtn").css({
                "z-index": "999"
            }), $("#guide").show();
            if (Signup.userDetail.newbie != 0) {
                var e = "",
                    t = ["一个人孤单的旅行听什么？", "悲伤的安静的小清新英文歌曲。", "我想听中国人翻唱的英文歌曲有木有？", "我分手了心情不是很好，来点音乐听听！", "秋天来了，我怀念去年我们在一起的时候。", "我想听有吉他和钢琴的，安静一点的流行歌曲。"];
                for (var n = 0; n < t.length; ++n) {
                    var r = "";
                    n < 3 ? r = "left" : r = "rgt", e = '<a href="#" class="ntLngBlt ' + r + '" data-text="' + t[n] + '"><span class="ntLngBltHd"></span>' + t[n] + '<span class="ntLngBltTl"></span></a>', $("#guide").append(e)
                }
                $(".ntLngBlt").each(function (e) {
                    var t = (Core.bodyHeight - 113 - 75 - 31) / 2 - 40 + 60 * (e % 3);
                    e < 3 ? $(this).css({
                        left: Core.bodyWidth / 2 - 56.5 - 48 - $(this).width(),
                        top: t + "px",
                        "z-index": 6 - e
                    }).hide() : $(this).css({
                        left: Core.bodyWidth / 2 + 56.5 + 48,
                        top: t + "px",
                        "z-index": 6 - e
                    }).hide()
                }), $(".ntLngBlt").click(function () {
                    $(this).animate({
                        left: (Core.bodyWidth - $("#schFld").width()) / 2 + "px",
                        top: Core.bodyHeight - 75 + "px",
                        opacity: .3
                    }, 1e3, function () {
                        Search.setSchVal($(this).data("text")), Search.searchBtnClick(), $(this).remove()
                    })
                }), Core.isFullScren = !0, $("#step1Tip").show().animate({
                    left: (Core.bodyWidth - $("#step1Tip").width()) / 2 + "px"
                }, 1e3), $("#step1Ds").show().animate({
                    left: (Core.bodyWidth - $("#step1Ds").width()) / 2 + "px"
                }, 600), $("#step1Btn").show().animate({
                    left: (Core.bodyWidth - $("#step1Btn").width()) / 2 + "px"
                }, 800), $("#step1Btn>a").click(Guide.step2)
            } else {
                Core.ie68 ? ($("#guide").css({
                    opacity: "0.95"
                }), $("#guide").children(".prgrs, .apps, .lgt, .vcOvr").show(), $("#guide").children(".finish").show()) : $("#guide").animate({
                    opacity: "0.95"
                }, 500, function () {
                    $("#guide").children(".prgrs, .apps, .lgt, .vcOvr").show().animate({
                        opacity: "1"
                    }, 300), $("#guide").children(".finish").show().animate({
                        opacity: "1"
                    }, 300)
                });
                var i = function () {
                    $("#guideFinish").isDisplay() && (Signup.iconShake("guideFinish"), setTimeout(i, 3e3))
                };
                setTimeout(i, 3e3), $("#guide").children(".finish").click(Guide.step2)
            }
        },
        step2: function () {
            $("#step1Tip").animate({
                left: Core.bodyWidth + "px"
            }, 1200, function () {
                $(this).hide()
            }), $("#step1Ds").animate({
                left: Core.bodyWidth + "px"
            }, 800, function () {
                $(this).hide()
            }), $("#step1Btn").animate({
                left: Core.bodyWidth + "px"
            }, 1e3, function () {
                $(this).hide()
            }), $("#step2Tip").show().animate({
                left: (Core.bodyWidth - $("#step2Tip").width()) / 2 + "px"
            }, 1e3), $("#step2Ds").show().animate({
                left: (Core.bodyWidth - $("#step2Ds").width()) / 2 + "px"
            }, 600), $("#step2Btn").show().animate({
                left: (Core.bodyWidth - $("#step2Btn").width()) / 2 + "px"
            }, 800), $("#step2Btn>a").click(Guide.step3)
        },
        step3: function () {
            Core.fullScrenMenuHide(), $("#maskUserMenu").hide(), $("#step2Tip").animate({
                left: Core.bodyWidth + "px"
            }, 1200, function () {
                $(this).hide()
            }), $("#step2Ds").animate({
                left: Core.bodyWidth + "px"
            }, 800, function () {
                $(this).hide()
            }), $("#step2Btn").animate({
                left: Core.bodyWidth + "px"
            }, 1e3, function () {
                $(this).hide()
            }), $("#step3Ds").show().animate({
                left: (Core.bodyWidth - $("#step3Ds").width()) / 2 + "px"
            }, 600), $("#step3Tip").show().animate({
                left: (Core.bodyWidth - $("#step3Tip").width()) / 2 + "px"
            }, 1e3, function () {
                $(".ntLngBlt").show()
            })
        },
        finish: function () {
            $("#tbCtn").removeClass("gnBd");
            var e = function () {
                $(this).hide(), Core.isFullScren = !1
            };
            Core.ie68 ? ($("#guide").hide(), e()) : $("#guide").animate({
                opacity: "0"
            }, 300, e), setTimeout(function () {
                Signup.userDetail.newbie = 0, $("#playCtl").removeClass("play").addClass("pause"), Player.startRotate()
            }, 2e3), $.ajax({
                url: Core.API_VER + "/account/register_completed",
                data: {
                    uid: Signup.userDetail.id
                }
            })
        },
        appDtl: function (e, t) {
            $(".drgbEvt").hide(), $("#acptBtn").show(), Guide.obj = $(this), Guide.mid = $(this).data("mid"), $("#acptBtn").html("我知道了").addClass("selected"), Guide.start = "", Guide.mid == "at" || Guide.mid == "stct" ? $("#appGuideVcOvr").show() : $("#appGuideVcOvr").hide(), Guide.appGuideShow(Guide.mid)
        },
        appGuideShow: function (e) {
            $("#acptBtn").html("我知道了").addClass("selected"), e == "at" || e == "stct" ? $("#appGuideVcOvr").show() : $("#appGuideVcOvr").hide(), Interface.Current == Interface.SEARCH && Search.hide(), Core.getCookie("clsInstrc") == null || Core.getCookie("clsInstrc") == "" ? ($("#appGuide .clsInstrc").removeClass("hide"), Core.setCookie("clsInstrc", "true")) : $("#appGuide .clsInstrc").addClass("hide"), Guide.mid = e, $("#maskUserMenu").show(), $("#tbCtn").addClass("gnBd"), Core.isFullScren = !0, $("#mscPlrCtn").css({
                "z-index": "999"
            }), Guide.isHide(), Guide.resize();
            if (Guide.mid == "smlMsc") {
                Guide.smlGuideShow();
                return
            }
            if (Guide.mid == "psnRd") {
                Guide.psnRdGuideShow();
                return
            }
            $("#appGuide").children(".appGdCtn").addClass(e), $("#appGuide").show(), e == "bdgs" && ($("#acptBtn").hide(), $("#appGuideVcOvr").hide(), $("#appGuide .appGdCtn .appIll").css({
                right: "-" + $("#appGuide .appGdCtn").offset().left + "px"
            })), $("#appGuide").animate({
                opacity: "0.95"
            }, 300), Guide.resize()
        },
        appGideHide: function (e) {
            return $("#maskUserMenu").hide(), $("#tbCtn").removeClass("gnBd"), e.animate({
                opacity: "0"
            }, 300, function () {
                $(".drgbEvt").show(), $("#mscPlrCtn").css({
                    "z-index": "1002"
                }), Guide.isShow(), Guide.mid == "bdgs" && $("#appGuide .appGdCtn .appIll").css({
                    right: "0px"
                }), $(this).children(".appGdCtn").removeClass(Guide.mid), $(this).hide(), Core.isFullScren = !1
            }), !1
        },
        psnRdGuideShow: function () {
            $("#psnRdGuide").show(), Guide.start == "guide" ? $("#psnRdGuide").css({
                opacity: "0.95"
            }) : $("#psnRdGuide").animate({
                opacity: "0.95"
            }, 300)
        },
        smlGuideShow: function () {
            $("#smlGuide").show(), Guide.isSmlAnimate ? $("#smlGuide").animate({
                opacity: "0.95"
            }, 300) : $("#smlGuide").css({
                opacity: "0.95"
            })
        },
        resize: function () {
            $("#guide, #smlGuide, #psnRdGuide, #shrGuide").css({
                height: Core.FS_HEIGHT + "px",
                top: "3px",
                left: "0px"
            });
            var e = $("#rlsNote .rlsCtn").width(),
                t = $("#rlsNote .rlsCtn").height();
            $("#rlsNote .rlsCtn").css({
                "margin-top": "-" + (t / 2 + 75) + "px",
                "margin-left": "-" + e / 2 + "px"
            });
            var n = (Core.bodyHeight - 330 - 75) / 2;
            $("#step1Tip, #step2Tip").css("top", n + "px"), n += 173, $("#step1Ds, #step2Ds").css("top", n + "px"), n += 125, $("#step1Btn, #step2Btn").css("top", n + "px"), n = (Core.bodyHeight - 113 - 75 - 31) / 2, $("#step3Tip").css("top", n + "px");
            var r = new Array("step1Tip", "step1Ds", "step1Btn", "step2Tip", "step2Ds", "step2Btn", "step3Tip", "step3Ds");
            for (var i = 0; i < r.length; ++i) $obj = $("#" + r[i]), $obj.isDisplay() && $obj.css("left", (Core.bodyWidth - $obj.width()) / 2 + "px");
            $(".ntLngBlt").length == 6 && $(".ntLngBlt").each(function (e) {
                var t = (Core.bodyHeight - 113 - 75 - 31) / 2 - 40 + 60 * (e % 3);
                e < 3 ? $(this).css({
                    left: Core.bodyWidth / 2 - 56.5 - 48 - $(this).width(),
                    top: t + "px"
                }) : $(this).css({
                    left: Core.bodyWidth / 2 + 56.5 + 48,
                    top: t + "px"
                })
            })
        }
    }, Retina = {
        enabled: !1,
        suffix: "",
        className: "retina",
        init: function () {
            window.devicePixelRatio && (Retina.enabled || window.devicePixelRatio >= 2) && (Retina.suffix = "@2x", Retina.enabled = !0, Core.A30 += Retina.suffix, Core.A50 += Retina.suffix, Core.A64 += Retina.suffix, $("html").addClass(Retina.className))
        }
    }, ClientIdArr = new Array,
    Now = {
        disconnectTmo: 0,
        init: function () {
            window.now = nowInitialize(NOW_URL, {}), now.ready(function () {
                console.log("now enter ready");
                var e = Signup.userDetail.sid + "-" + Core.nowSuccess;
                ClientIdArr[ClientIdArr.length] = window.now.core.clientId, now.connectionUnique(Signup.userDetail.id, Signup.userDetail.nick, e, function () {
                    ++Core.nowSuccess, clearTimeout(Now.disconnectTmo), Now.disconnectTmo = 0, console.log("now connection regist success"), Core.nowIsReady = !0, $("#gnsCtt>.ctt").text().indexOf("你掉线了哦，检查下你的网络，或者刷新下页面试试") >= 0 && ($("#gnsCtn").data("tag", ""), Gns.nowGns("你的网络已经恢复，继续玩吧"))
                }, Selector_browser, Core.getCookie("jing.auth").split(",")[0])
            }), now.receiveTickerMessage = function (message) {
                message = eval("(" + message + ")");
                if (Signup.userDetail.sts.tckNtf != "true") return;
                ++Signup.tkrsCount, Signup.userDetail.id == About.ouid && (About.mid == "tkrsAll" || About.mid == "tkrsMe" && Signup.userDetail.id == message.uid ? (--Signup.tkrsCount, ++About.st, About.ctn.children(".abtTabCtn").next().children(".noCtt").remove(), About.ctn.children(".abtTabCtn").next().prepend(About.tkrsGnHtml(message, 0)), (message.t == "C" || message.t == "D" || message.t == "S" || message.t == "L") && Core.imgLoad(About.ctn.children(".abtTabCtn").next().children().first().find("img"), "", message.avt, 53), message.t == "L" && About.loveSelected(message.tid), About.scrollApi.reinitialise()) : Signup.userDetail.id != message.uid && $("#frdsTkrsCount").text(Signup.tkrsCount).show()), Signup.userDetail.id != message.uid && Signup.aboutCountShow()
            }, now.receivePrivateMessage = function (message) {
                message = eval("(" + message + ")");
                switch (message.t) {
                case "lisn":
                    if (Flw.isFlw) return;
                    Gns.friendListen(message);
                    break;
                case "chat":
                    Cht.receiveMessage(message);
                    break;
                case "acty":
                    Player.setVolumeDown("msgTone", 100), Gns.nowGns('你收到一条活动信息，<a href="#" class="trg gnsSysEvent">去看看</a>');
                    break;
                case "nson":
                    ++Signup.onlineCount, message.uid == About.ouid && About.startPt();
                    break;
                case "nsof":
                    --Signup.onlineCount, Signup.onlineCount < 0 && (Signup.onlineCount = 0), message.uid == About.ouid && About.stopPt();
                    break;
                case "umft":
                    Player.setVolumeDown("msgTone", 100), Gns.nowGns('<a href="#" class+"trg prflEvent" data-uid="' + message.uid + '" data-nick="' + message.nick + '">' + message.nick + "</a> 喜欢了你喜欢的 " + message.n);
                    break;
                case "utrc":
                    Player.setVolumeDown("msgTone", 100), Gns.nowGns('<a href="#" class="trg prflEvent" data-uid="' + message.uid + '" data-nick="' + message.nick + '">' + message.nick + "</a> 收藏了你的搜索条件 " + message.tit);
                    break;
                default:
                    Message.send(message, !0)
                }
            }, now.receiveInputMessage = function (e, t) {
                if (Cht.fuid != e) return;
                Cht.typingMessage(e, t)
            }, now.disconnect = function (e) {
                setTimeout(function () {
                    for (var t = 0; t < ClientIdArr.length; ++t) if (ClientIdArr[t] == e) return;
                    Gns.nowGns('你的帐号已在别的地方登陆了，我们将会在 <em class="serif big">5</em> 秒内退出此帐号。', Gns.level1), Core.setCookie("jing.auth", ""), setTimeout(function () {
                        window.location.reload()
                    }, 5e3)
                }, 5e3)
            }, now.disconnectRefresh = function () {
                setTimeout(function () {
                    Gns.nowGns("检测到你的网络连接异常，将会在5秒钟后重新加载应用，或者手动刷新。", Gns.level1)
                }, 1e4), setTimeout(function () {
                    window.location.reload()
                }, 15e3)
            }, now.followListenResponseFailOther = function (e, t) {
                $.ajax({
                    url: Core.API_VER + "/account/check_frdshp",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: e
                    },
                    success: function (n) {
                        $("#usrTpsCtn").remove();
                        var r = "";
                        n.success ? r = Flw.nick + " 正在跟听 " + t + '，你也要跟听吗？<a id="flwOther" data-fid="' + e + '" data-fnick="' + t + '" class="trg" href="#">跟听</a>' : r = Flw.nick + " 正在跟听别人", Flw.empty(), setTimeout(function () {
                            Gns.nowGns(r)
                        }, 500)
                    }
                })
            }, now.followListenRequestSuccess = function () {
                Gns.nowGns('跟听请求已发送给Ta，现在正在等待Ta的许可 <em id="flwCountdown" class="serif big">15</em>', Gns.level1);
                var e = function () {
                    if ($("#flwCountdown").length == 0) return;
                    var t = parseInt($("#flwCountdown").text()) - 1;
                    t > 0 && ($("#flwCountdown").text(t), setTimeout(e, 1e3))
                };
                setTimeout(e, 2e3), $("#usrTpsCtn").remove(), Flw.serverTmo = setTimeout(function () {
                    $("#gnsCtn").data("tag", ""), Gns.nowGns(Flw.nick + "可能不在电脑旁边哦～所以不能跟听啦"), Flw.empty()
                }, 17e3)
            }, now.followListenRequestAuthorize = function (e, t) {
                Player.setVolumeDown("msgTone", 100), Gns.nowGns(t + ' 想要让你带着Ta听音乐，<a href="#" id="flwOK" class="trg know" data-fuid="' + e + '">同意</a> 或 <a href="#" id="flwNO" class="trg know" data-fuid="' + e + '">拒绝</a> <em class="serif big" id="flwCountdown">15</em> 秒后默认为同意。', Gns.level1), n = 14;
                var r = function () {
                    n > 0 ? ($("#flwCountdown").text(n), Flw.reqTmo = setTimeout(r, 1e3), --n) : Flw.flwOK(e)
                };
                Flw.reqTmo = setTimeout(r, 2e3)
            }, now.followListenJoin = function (e, t) {
                if (Signup.userDetail.id == e) $("#gnsCtn").data("tag", ""), Gns.nowGns("你正在和 " + Flw.nick + " 收听同样的音乐哦，你可以喜欢、讨厌，但在退出跟听之前不能进行其他操作"), clearTimeout(Flw.serverTmo);
                else if (Flw.toid == "") {
                    var n = t + " 正在跟着你听音乐哦";
                    Flw.users == null && (n += "，点击随便听听查看跟听状态", Flw.users = new Object), Gns.nowGns(n), Flw.users[e] = t
                } else Gns.nowGns(t + " 加入了跟听")
            }, now.followListenLeave = function (e, t) {
                if (Flw.toid == e) Gns.nowGns(t + " 停止让你继续跟听Ta，跟听结束。"), $("#clsFlwLstn").click();
                else {
                    var n = t + " 退出了跟听";
                    if (Flw.toid == "") {
                        if (Signup.userDetail.id == e) return;
                        delete Flw.users[e], Core.objLength(Flw.users) == 0 && (Flw.empty(), n += "，跟听已结束")
                    }
                    Gns.nowGns(n)
                }
            }, now.followListenResponseAuthorizeSuccess = function (e) {
                if (!Flw.isFlw) return;
                Core.fullScrenMenuShow("你正在跟听 " + Flw.nick + '<span class="spl"></span><a href="#" id="clsFlwLstn" class="clsFlwLstn overlay">关闭跟听</a>', "flwLstn"), Gns.openGnsArr = new Array;
                var t = e.split(",");
                t[0] == "pausing" ? Flw.play(e, !0) : Flw.play(e, !1)
            }, now.followListenResponseAuthorizeRefuse = function () {
                $("#gnsCtn").data("tag", ""), Flw.empty(), Gns.nowGns("你的好友 " + Flw.nick + " 暂时不想让你跟听，试试收听Ta喜欢的音乐吧")
            }, now.receiveFollowListenMessage = function (e, t) {
                if (Signup.userDetail.id == e) return;
                var n = t.split(",");
                n[0] == "playing" ? Player.playCtl() : n[0] == "pause" ? (Gns.nowGns("你跟听的人，暂停播放了哦"), Player.pause()) : n[0] == "volume" ? (Player.vlmHideTmo == 0 && Gns.nowGns("你跟听的人，调节了音量哦"), Player.setVolume(Number(n[1]))) : n[0] == "hate" && Flw.toid == "" ? (Gns.nowGns("正在跟听你的" + n[1] + '，表示不想听这首。和他 <a href="#" class="trg offChtNickEvent" data-nick="' + n[1] + '" data-uid="' + e + '" data-avatar="' + n[2] + '" data-ol="true">聊聊</a>'), Player.setVolumeDown("msgTone", 100)) : n[0] == "love" && Flw.toid == "" ? Gns.nowGns("正在跟听你的" + n[1] + "，喜欢了这首歌哦") : Flw.play(t, !1)
            }, now.receiveOnlineFrdCountMessage = function (e) {
                e = Number(e);
                if (e == Signup.onlineCount) return;
                Signup.onlineCount = e
            }
        }
    }, Message = {
        isMessageAni: !1,
        isLoginInit: !1,
        msgOverObj: $("null"),
        init: function () {
            $(document).on("mouseenter", ".msgCtn", function () {
                Message.msgOverObj = $(this)
            }).on("mouseleave", ".msgCtn", function () {
                Message.msgOverObj = $("null")
            })
        },
        send: function (e, t) {
            var n = "";
            if (e.t == "uats") {
                n = '你已经喜欢3首 <a class="msgFlyEvent trg" href="#" data-fid="' + e.fid + '">' + e.arts + "</a> 的歌曲，现在解锁到你的音乐中心。";
                var r = Signup.userDetail.newview.b.length;
                Signup.userDetail.newview.b[r] = "ats-" + e.fid
            } else if (e.t == "ubdg") {
                n = '你刚刚解锁了勋章 <a class="msgFlyEvent trg" href="#" data-fid="' + e.fid + '">' + e.bdg + "</a> 赶快去体验一下吧。";
                if (e.bt.toLowerCase() != "sns") {
                    if (e.n == undefined) {
                        setTimeout(function () {
                            e.n = "true", Message.send(e, t)
                        }, 2e3);
                        return
                    }
                    var r = Signup.userDetail.newview.b.length;
                    Signup.userDetail.newview.b[r] = e.bt + "-" + e.fid
                }
            } else if (e.t == "flwd") {
                var i = "";
                e.me_flw || (i = '，你也要 <a href="#" class="trg gnsFlwd" data-frdid="' + e.flwer_id + '" data-nick="' + e.flwer + '">关注</a> 吗？'), e.flw_id == Signup.userDetail.id ? n = '<a data-uid="' + e.flwer_id + '" data-nick="' + e.flwer + '" href="#" class="trg prflEvent">' + e.flwer + "</a> 关注了你" + i : Signup.userDetail.id != e.flw_id && Signup.userDetail.id != e.flwer_id && (n = '你的好友 <a data-uid="' + e.flwer_id + '" data-nick="' + e.flwer + '" href="#" class="trg prflEvent">' + e.flwer + '</a> 关注了 <a data-uid="' + e.flw_id + '" data-nick="' + e.flw + '" href="#" class="trg prflEvent">' + e.flw + "</a>")
            } else if (e.t == "inhs") {
                if (Message.isLoginInit) {
                    Message.isLoginInit = !1, setTimeout(function () {
                        Message.send(e, t)
                    }, 1e4);
                    return
                }
                for (var s = 0; s < e.frds.length; ++s) {
                    n += '<a href="#" class="trg prflEvent" data-uid="' + e.frd_ids[s] + '" data-nick="' + e.frds[s] + '">' + e.frds[s] + "</a>，";
                    var r = Signup.userDetail.newview.b.length;
                    Signup.userDetail.newview.b[r] = "frdCt-inHs-" + e.frd_ids[s]
                }
                n = "你的好友 " + n.substring(0, n.length - 1) + " 入驻Jing了。", $("#frdCt").hasClass("selected") ? Frd.current == Frd.FRDS ? $(".iconEvent[mid='inHs']").children(".newApp").show() : $("#frds").find(".newApp").show() : Signup.iconShake("frdCtNewIcon")
            } else if (e.t == "atfd") {
                var o = e.cmbt.split(","),
                    u = "";
                for (var s = 0; s < o.length; ++s) u += '<a class="msgFlyEvent trg" href="#" data-fid="">' + o[s] + "</a>+";
                u != "" && (u = u.substring(0, u.length - 1), u = " " + u + " "), e.cmbt == "" && (u = ""), n = '<a data-uid="' + e.frd_id + '" data-nick="' + e.frd + '" href="#" class="trg prflEvent">' + e.frd + "</a> ", t ? n += "正在收听" : n += "收听过", n += "你喜欢的" + u + "音乐。"
            } else if (e.t == "uapp") {
                for (var s = 0; s < resData[e.pmid].length; ++s) if (resData[e.pmid][s].mid == e.mid) {
                        resData[e.pmid][s].display = "" + e.display;
                        if (e.display) {
                            n = "你解锁了" + resData[e.pmid][s].name + "菜单", Signup.userDetail.newview.m[Signup.userDetail.newview.m.length] = e.path;
                            break
                        }
                        ConverSns[e.mid] != undefined && $("#" + ConverSns[e.mid]).parent().children(".selected").remove();
                        return
                    }
            } else if (e.t == "ufnc") {
                Signup.userDetail.avbF[e.mid] = !0, n = "你解锁了" + e.n + "，快去试试吧。";
                if (e.mid == "smlMsc" || e.mid == "psnRd") n = "你已成功安装" + e.n + "，快去试试吧。", Signup.userDetail.avbF[e.mid] = ""
            } else e.t == "rmnd" && (n = e.frd + ' 想要你关注Ta，<a href="#" class="trg gnsFlwd" data-frdid="' + e.frd_id + '" data-nick="' + e.frd + '">关注</a>');
            Player.setVolumeDown("msgTone", 100), Gns.nowGns(n)
        }
    }, Search = {
        TIP_WIDTH: 145,
        TIP_HEIGHT: 170,
        PERCENTAGE: 5,
        INTERVAL: 100,
        rowCount: 0,
        colCount: 0,
        isFly: !1,
        oldStart: 0,
        oldEnd: 0,
        count: 0,
        maxCount: 0,
        keywords: "",
        updateKeywords: null,
        tickerSendTimeout: 0,
        autoKeywords: null,
        st: 0,
        total: 0,
        ps: 0,
        isSearch: !1,
        TYPE: 0,
        TIP: 0,
        TOP: 1,
        TOP_WIDTH: 290,
        TOP_HEIGHT: 324,
        topMaxCount: 0,
        topRowCount: 0,
        topColCount: 0,
        tid: 0,
        mt: "",
        response: null,
        afterKeywords: "",
        moods: null,
        ss: !0,
        init: function () {
            $("html").hasClass("gecko") && $("#schFld").prop("placeholder", ""), $("#schBxCtn").click(Search.schBxCtnClick), $("#schFld").keyup(Search.inputKeyup), $("#schFld").keydown(Search.inputKeydown), $("#schFld").focus(function () {
                Interface.Current != Interface.SEARCH && $("#schBxCtn").click()
            }), $("#schBtn").click(function () {
                return _gaq.push(["_trackEvent", "Search", "ClckSch", "ClickToSearch"]), Interface.Current == Interface.SEARCH && $(document).click(), Search.searchBtnClick(), !1
            }), Search.resize("init"), $(document).on("click", ".searchflyCtn", function () {
                Search.fly($(this), $(this).data("fid"), $(this).children("span").data("text"), !0, !1), setTimeout(function () {
                    Search.dataUpdate("")
                }, 1e3)
            }), $(document).on("click", "#instSchChange", function () {
                return _gaq.push(["_trackEvent", "Search", "instSchChange", "Search/InstantSearch"]), Search.dataUpdate("", !1), !1
            }), $(document).on("click", "#smtSch", function () {
                Search.ss = !1, Search.st = 99999999, Gns.nowGns("你已经关闭了智能引导，现在可以收听 " + $(this).data("artist") + " 全部的歌曲了")
            }), $(document).on("click", "#smtSchIgnore", function () {
                Gns.isOver = !1, Gns.nextGns()
            })
        },
        setSchHint: function () {
            if ($("#schFld").val() == "")(Core.ie69 || Core.gecko) && $("#schBxCtn .schHint").html("情绪 , 状态 , 艺人 , 风格 , 乐器 ... 随意组合").css({
                    left: "27px"
                }).show();
            else {
                if (Interface.Current == Interface.SEARCH) return;
                $("#schBxCtn .schHint").html("想听更多点击添加"), $("#ctnLength").html(Core.inputConver($("#schFld").val()));
                var e = $("#ctnLength").width() + 20;
                e < 198 ? $("#schBxCtn>.schHint").css({
                    left: e + "px"
                }).show() : $("#schBxCtn>.schHint").hide()
            }
        },
        schBxCtnClick: function (e) {
            if (Signup.userDetail.newbie != 0) return;
            $("#tps").hide();
            if (Interface.Current != Interface.SEARCH) {
                Search.updateKeywords = null, Interface.Current = Interface.SEARCH, Main.hide(), $("#schBxCtn .schHint").hide();
                var t = $("#schFld").val();
                t.length != 0 && Symbol[t.substring(t.length - 3)] == undefined && (t += " + "), $("#schFld").val(t), $("#schFld").setCursorPosition(t.length), About.isSchFldAnimate && ($("#schFld").css({
                    width: "344px"
                }), $("#mainMenuCtn").css({
                    width: "407px"
                })), $("#mdlHr, #flyAblum, #flySmlAblum").hide(), Core.playerCDHide(), $("#instSchCtn, #instSchCttCtn").show(), $("#leftAr").animate({
                    right: "+=37"
                }, 300), e == "榜单" ? Search.dataUpdate(e) : Search.dataUpdate(""), $("#schFld").focus()
            }
            return !1
        },
        searchBtnClick: function () {
            Signup.userDetail.newbie != 0 && Guide.finish();
            var e = !0;
            Player.loveCount = 0, Player.hateCount = 0, Player.isInsertPlay && Player.insertStop(), $("#atTipsCtn").html("").hide(), $(".tipCtn").length == 1 && Search.flyInput($(".tipCtn").children("span").data("text"), !0);
            var t = $("#schFld").val();
            t = Search.filterStr(t), $("#schFld").val(t), t = Search.removeLastSymbol(t);
            var n = t.toLowerCase();
            if (n == "jing歌曲榜" || n == "jing榜单" || n == "jing排行榜" || n == "jing top charts" || n == "音乐瀑布") {
                Top.show(), $("#schFld").val(Search.keywords);
                return
            }
            if (t == "") {
                $("#schFld").val("");
                return
            }
            Search.st = 0, Player.isPsnRdInsertPlay && Player.psnRdClick(), clearTimeout(Gns.gnsRcmdTmo), Gns.gnsRcmdTmo = setInterval(Gns.rcmd, 6e5), t == Search.keywords && (e = !1), Search.searchByKeywords(t, 0, 0, e)
        },
        searchByKeywords: function (e, t, n, r) {
            if (e == "") return;
            var i = "";
            i = Core.API_VER + "/search/jing/fetch_pls";
            var s = Search.tid,
                o = Search.mt;
            Search.tid = 0, Search.mt = "", Search.isSearch = !0, n == 0 && Player.addLoading(), $.ajax({
                url: i,
                data: {
                    q: e,
                    ps: 5,
                    st: n,
                    u: Signup.userDetail.id,
                    tid: s,
                    mt: o,
                    ss: Search.ss
                },
                success: function (n) {
                    Search.isSearch = !1;
                    if (!n.success) {
                        $("#playCtl").removeClass("loading"), Gns.nowGns("Jing 开了点小差，你过会儿再试试。");
                        return
                    }
                    n.result.hint != undefined && Search.st == 0 && Gns.nowGns(n.result.hint);
                    if (n.result.items.length == 0) {
                        Search.st != 0 ? (Search.st = 0, Search.searchByKeywords(Search.keywords, 0, 0)) : t == 0 && (Search.keywords == e ? Player.setPlayerDefault() : Search.setSchVal(Search.keywords), $("#playCtl").removeClass("loading"), $("#mscPlr").data("mouse") == "false" && $("#mscPlrMask").mouseleave(), Player.spin.data().spinner.stop(), Player.spin = null, $("#playCtl").hasClass("pause") && Player.startRotate());
                        return
                    }
                    n.result.moods == undefined ? (Search.moods = null, Search.moodids = "") : (Search.moods = n.result.moods, Search.moodids = n.result.moodids);
                    if (n.result.psnrdmode) {
                        Player.psnRdClick(), $("#schFld").val(Search.keywords);
                        return
                    }
                    n.result.g && Gns.nowGns("关闭智能引导，可以搜听 " + n.result.g + ' 所有的歌曲，<a id="smtSch" href="#" class="trg" data-artist="' + n.result.g + '">关闭</a> | <a id="smtSchIgnore" href="#" class="trg">忽略</a>');
                    if (n.result.choose == 1) {
                        $("#playCtl").removeClass("loading");
                        var i = "",
                            s = n.result.items;
                        for (var o = 0; o < s.length; ++o) i += '<a href="#" class="trg gnsSchSml" data-tid="' + s[o].tid + '">' + s[o].n + "</a>", o == s.length - 2 ? i += "，还是 " : i += "，";
                        Gns.nowGns("你想要搜索的歌曲是 " + i + " 演艺的？");
                        return
                    }
                    if (n.result.multiple != undefined) {
                        var i = "";
                        for (var o = 0; o < n.result.multiple.length; ++o) {
                            var u = n.result.multiple[o].split(":");
                            u[2] == "ats" ? i += "艺人" : u[2] == "tas" ? i += "电影原声" : u[2] == "sta" ? i += "原声专辑" : i += "标签", i += '：<a href="#" class="trg gnsMt" data-mt="' + n.result.multiple[o] + '">', u[0] == u[1] ? i += u[0] : i += u[0] + "（" + u[1] + "）", i += "</a>、"
                        }
                        Gns.nowGns("还有 " + i.substring(0, i.length - 1) + "，是否要听听？")
                    }
                    if (n.result.bdata != undefined) {
                        About.topObj = new Object, About.topChildObj = new Object;
                        var a = n.result.bdata.next,
                            f = n.result.bdata.fid,
                            l = n.result.bdata.title,
                            c = n.result.bdata.name,
                            s = n.result.bdata.items;
                        About.topObj[a] = new Array(f, l, c), About.topChildObj[a] = new Object;
                        for (var o = 0; o < s.length; ++o) {
                            var h = s[o];
                            About.topChildObj[a][h.next] = new Array(h.fid, h.title, h.name, !1)
                        }
                        About.currentMain = "top", About.switching({
                            ouid: a
                        })
                    }
                    var p = n.result.os;
                    if (p != undefined) {
                        osArr = p.split(":");
                        if (!(osArr[1] > 0)) {
                            Search.response = n, Gns.nowGns(osArr[0] + '今年才发行的专辑，还没有老歌呀，<a href="#" class="trg gnsTingOS" data-keywords="' + e + '">是否收听这盘专辑</a>呢？');
                            return
                        }
                        Gns.nowGns(osArr[0] + "的歌曲都不算太老哦，为你播放 " + osArr[0] + " 尽可能早的歌曲")
                    }
                    Search.searchAfter(e, n, t, r, !0)
                }
            })
        },
        searchAfter: function (e, t, n, r, i) {
            t.result.tn != undefined && Gns.nowGns('你正在收听 <a href="#" class="trg">' + t.result.tn + "</a> 的相似歌曲"), Player.smlMusic = null, $("#flySmlAblum").remove(), Search.total = t.result.total, t.result.cmbt != undefined ? (Search.keywords = t.result.cmbt, e = t.result.cmbt, Search.setSchVal(Search.keywords)) : Search.keywords = e, r == 1 && (Search.tickerSendTimeout != 0 && clearTimeout(Search.tickerSendTimeout), e.indexOf("@") == -1 && (Search.tickerSendTimeout = setTimeout(function () {
                $.ajax({
                    url: Core.API_VER + "/ticker/post_cmbt",
                    data: {
                        uid: Signup.userDetail.id,
                        content: e,
                        tid: t.result.tid,
                        usedCmbt: i
                    },
                    success: function (e) {
                        e.result != undefined && e.result.rd != undefined && (pHtml = Signup.userDetail.nick + '，试试 <a class="gnsRdFlyEvent trg" href="#">' + e.result.rd + "</a> 的组合", Gns.openGnsArr[Gns.openGnsArr.length] = new Array("rcmd", pHtml), Gns.showGns())
                    }
                })
            }, 1e4))), n == 0 && (Player.music = new Array);
            var s = n;
            for (var o = 0; o < t.result.items.length; ++o) {
                if (n == 1 && t.result.items[o].tid == Player.music[0].tid) continue;
                Search.setMusic(t.result.items[o], s), s += 1
            }
            Search.ps = Player.music.length, n == 0 && (Gns.isInsertPlay ? (Player.pos = -1, Gns.insertStop("next")) : (Player.pos = 0, Player.setVolumeDown("play", 0)), Player.playerUI(), Player.closeGns())
        },
        setMusic: function (e, t) {
            Player.music[t] = new Object, Player.music[t].fid = e.fid, Player.music[t].mid = e.mid, Player.music[t].tid = e.tid, Player.music[t].name = e.n, Player.music[t].duration = e.d, Player.music[t].an = e.an, Player.music[t].atn = e.atn, e.d < 1 && console.error("duration异常(" + e.d + "), name->" + e.n + ", fid->" + e.tid)
        },
        dataUpdate: function (e, t) {
            if (e == Search.updateKeywords && t == undefined) return;
            Search.updateKeywords = e, $("#atTipsCtn").html("").hide();
            var n = "";
            e == "" ? n = Core.API_VER + "/badge/fetch_pop" : n = Core.API_VER + "/search/ling/auto";
            if (e[0] == "%") return;
            e == "榜单" || e == "排行榜" || e == "榜單" || e == "bangdan" || e == "paihangbang" ? (Search.TYPE = Search.TOP, $.ajax({
                url: Core.API_VER + "/chart/fetch",
                data: {
                    uid: Signup.userDetail.id,
                    nodeids: "0"
                },
                success: function (e) {
                    if (!e.success) return;
                    var t = "",
                        n = e.result[0].items;
                    About.topObj = new Object, About.topChildObj = new Object, Search.count = n.length, t += '<a id="chartsArrLeft" class="chartsArr left" href="#"></a><div id="chartsCtn" class="chartsCtn">';
                    for (var r = 0; r < n.length; ++r) {
                        var i = n[r].next,
                            s = n[r].fid,
                            o = n[r].title,
                            u = n[r].name,
                            a = "";
                        About.topObj[i] = new Array(s, o, u), About.topChildObj[i] = new Object;
                        for (var f = 0; f < n[r].childs.length; ++f) {
                            var l = n[r].childs[f];
                            About.topChildObj[i][l.next] = new Array(l.fid, l.title, l.name, !1)
                        }
                        n[r].newest > 0 && Apps.offsetDate(n[r].newest, "top") && (a = '<em class="newApp overlay" style="top:258px;"></em>'), t += '<a class="chartCtn topObj" data-next="' + i + '" href="#">' + '<div class="chartBd"></div>' + '<div class="chart" data-fid="' + s + '"></div>' + '<div class="chartTit">' + o + "</div>" + '<div class="chartShdw"></div>' + a + "</a>"
                    }
                    t += '</div><a id="chartsArrRight" class="chartsArr rght" href="#"></a>', $("#instSchCttCtn").html(t), $(".chartTit").each(function () {
                        $(this).css("float", "left"), $(this).parent().children(".newApp").css("left", 90 + $(this).width() / 2 + "px"), $(this).css("float", "none")
                    }), $(".chart").each(function () {
                        var e = new Image;
                        e.obj = $(this), e.onload = function () {
                            this.obj.css("background-image", "url('" + this.src + "')")
                        }, e.src = $.id2url($(this).data("fid"), "BN", "chart")
                    }), $(".topObj").click(function () {
                        return $(".topObj>.chartBd").removeClass("selected"), $(this).children(".chartBd").addClass("selected"), About.currentMain = "top", About.switching({
                            ouid: $(this).data("next")
                        }), !1
                    }), $("#chartsArrLeft").click(function () {
                        if (Search.isAnimate) return !1;
                        Search.isAnimate = !0;
                        var e = parseInt($("#chartsCtn").css("margin-left").replace("px", "")) - 40;
                        return e > 0 ? !1 : (e < -(Search.topRowCount * Search.TOP_WIDTH) ? e = Search.topRowCount * Search.TOP_WIDTH : e = -e, $("#chartsCtn").animate({
                            "margin-left": "+=" + e
                        }, 1200, function () {
                            Search.isAnimate = !1
                        }), !1)
                    }), $("#chartsArrRight").click(function () {
                        if (Search.isAnimate) return !1;
                        Search.isAnimate = !0;
                        var e = $("#chartsCtn").width() + parseInt($("#chartsCtn").css("margin-left").replace("px", "")) - Search.topRowCount * Search.TOP_WIDTH - 40;
                        return e < 0 ? !1 : (e > Search.topRowCount * Search.TOP_WIDTH && (e = Search.topRowCount * Search.TOP_WIDTH), $("#chartsCtn").animate({
                            "margin-left": "-=" + e
                        }, 1200, function () {
                            Search.isAnimate = !1
                        }), !1)
                    }), Search.resize()
                }
            })) : ($("#chartsArrLeft").length == 1 && ($("#instSchCttCtn").html(""), $("#instSchChange").remove()), Search.TYPE = Search.TIP, $.ajax({
                url: n,
                data: {
                    q: e,
                    ps: Search.maxCount,
                    st: 0,
                    u: Signup.userDetail.id
                },
                success: function (t) {
                    if (!t.success) return;
                    Search.count = t.result.length;
                    var n = "",
                        r, i = $.makeArray($("#instSchCttCtn>.tipCtn")),
                        s = t.result.length,
                        o = s < i.length ? i.length : s;
                    for (r = 0; r < o; ++r) {
                        if (r >= s) {
                            $(i[r]).remove();
                            continue
                        }
                        var u = t.result[r].fid;
                        u == null && (u = "");
                        var a = t.result[r].t,
                            f = t.result[r].n,
                            l = "",
                            c = f;
                        a == "friend" ? (c = "@" + f, u == "" ? l = IMG_URL + "/defaults/avatar/100" + Retina.suffix + ".jpg" : u.indexOf("http://") != 0 ? l = $.id2url(u, "UM", "avatar") : l = u) : a == "artist" ? l = $.id2url(u, "SM", "artist") : l = Core.badgesUrl(u, 100);
                        var h = "";
                        r < i.length ? ($(i[r]).data("fid", u), $(i[r]).children(".tip").children("img").remove(), $(i[r]).children("span").text(f), $(i[r]).children("span").data("text", c), h = $(i[r]).children(".tip")) : (n = '<div data-fid="' + u + '" class="tipCtn searchflyCtn overlay">' + '<div class="tip"></div>' + '<a class="tipMask" href="#"></a>' + '<span data-text="' + c + '">' + f + "</span>" + "</div>", $("#instSchCttCtn").append(n), h = $("#instSchCttCtn").children().last().children(".tip")), h.append('<img src="" style="position:absolute;" />'), h = h.children("img"), a == "friend" && u.indexOf("http://") == 0 ? Core.imgLoad(h, "", l, 100, "searchTip") : h.attr({
                            src: l,
                            width: "100px",
                            height: "100px"
                        })
                    }
                    e == "" ? $("#instSchChange").length == 0 && $("#instSchCtn").append('<a href="#" id="instSchChange" class="instSchChng">换一批看看</a>') : $("#instSchChange").remove(), Search.resize()
                }
            }))
        },
        userUpdate: function (e) {
            $("#instSchCttCtn").html(""), Search.afterKeywords = e.substring(0, e.indexOf("@")), e = e.substring(e.indexOf("@")), $.ajax({
                url: Core.API_VER + "/search/at/auto",
                data: {
                    q: e,
                    ps: 100,
                    st: 0,
                    u: Signup.userDetail.id
                },
                success: function (t) {
                    if (!t.success || Interface.Current != Interface.SEARCH) return;
                    var n = "";
                    if (t.result.length == 0) {
                        $("#atTipsCtn").hide();
                        return
                    }
                    var r = new Array,
                        i = t.result.length + 1;
                    i > 5 && (i = 5);
                    for (var s = 0; s < i; ++s) {
                        var o = "",
                            u = "",
                            a = "",
                            f = "";
                        s == 0 ? (u = e.replace("@", ""), f = "hide selected") : (o = t.result[s - 1].fid, u = t.result[s - 1].n, a = t.result[s - 1].c), n += '<div class="atTip ' + f + '">' + '<div class="avtCtn">' + '<a class="avtMask tiny" href="#"></a>' + '<div class="avt tiny" style="overflow:hidden;position:relative;"><img style="position: absolute; width: 30px; height: 30px;" src="' + IMG_URL + "/defaults/avatar/" + Core.A30 + '.jpg" /></div>' + "</div>" + '<div class="atName">' + "<p>" + u + "</p>" + "</div>" + '<div class="fvtCnt">' + "<p>" + a + "首</p>" + "</div>" + "</div>", r[s] = o
                    }
                    $("#tbCtn").css("overflow", "visible"), $("#atTipsCtn").html(n).show(), $("#atTipsCtn").children().each(function (e) {
                        Core.imgLoad($(this).find("img"), "", r[e], 30)
                    }), $(".atTip").click(function () {
                        Search.flyInput(Search.afterKeywords + "@" + $(this).children(".atName").children("p").text(), !0), $("#atTipsCtn").hide()
                    })
                }
            })
        },
        fly: function (e, t, n, r, i) {
            if (Search.isFly) return !1;
            if (!e.hasClass("ntlPlyCtl") && $("#fsappMenu").find(".clsFlwLstn").length >= 1) {
                $("#fsappMenu").find(".clsFlwLstn").each(function () {
                    $(this).isDisplay() && $(this).click()
                }), setTimeout(function () {
                    Search.fly(e, t, n, r, i)
                }, 500);
                return
            }
            Flw.isFlw && Flw.toid != "" && $("#clsFlwLstn").click(), Search.isFly = !0, $("#schFld").width() == 329 && (Search.oldStart = $("#schFld").val().length);
            var s = e.offset().left,
                o = e.offset().top;
            n == "随便听听" && (s -= e.width(), o -= e.height());
            var u = "";
            t == "guide" ? e.parent().append('<div data-span="' + n + '" id="flySearchTip" style="position:fixed; left:' + s + "px; top:" + o + 'px; z-index:99999"><a>' + n + "</a></div>") : (n[0] == "@" ? t == "" ? u = IMG_URL + "/defaults/avatar/50" + Retina.suffix + ".jpg" : t.indexOf("http://") != 0 ? u = $.id2url(t, "UT", "avatar") : u = t : t.indexOf(".") >= 0 ? u = $.id2url(t, "ST", "artist") : u = Core.badgesUrl(t, 50), $("body").append('<img src="' + u + '" class="ufo hide" data-span="' + n + '" id="flySearchTip" style="left:' + s + "px; top:" + o + 'px;"/>'));
            var a = Core.bodyWidth / 2 - $("#schFld").width() / 2;
            NtrlLngTop.isOpen && (a += 420);
            var f = Core.bodyHeight - 55,
                l = $("#schFld").val(),
                c = Search.getStrWidth(l);
            c > 310 ? a += $("#schFld").width() / 2 : a += Search.getStrWidth(l.substring(0, Search.oldStart));
            var h = function (e) {
                var n = "";
                t == "guide" ? (n = $(this).data("span"), $(this).remove()) : (n = e.data("span"), e.remove()), r == 1 ? Search.flyInput(n, !0) : Search.setSchVal(n + " + "), i == 1 && ($(document).click(), Search.searchBtnClick()), Search.isFly = !1
            };
            return t == "guide" ? $("#flySearchTip").show().animate({
                left: a + "px",
                top: f - 30 + "px"
            }, 800, h) : $("#flySearchTip").show().animate({
                left: a + "px",
                top: f + "px"
            }, 800, function () {
                $("#flySearchTip").animate({
                    top: f - 50 + "px"
                }, 400, function () {
                    e.hasClass("ntlPlyCtl") ? (NtrlLngTop.isInsertPlay = !0, $(this).remove(), Core.fullScrenMenuShow('<span id="prevHtml">正在收听 ' + $(this).data("span") + ' <span class="spl"></span><a href="#" id="closeNtrlPlay" class="clsFlwLstn overlay">退出</a></span>', "ntrl"), $.ajax({
                        url: Core.API_VER + "/search/jing/fetch_pls",
                        data: {
                            q: $(this).data("span"),
                            ps: 100,
                            st: "0",
                            u: Signup.userDetail.id
                        },
                        success: function (e) {
                            Player.music = new Array;
                            for (var t = 0; t < e.result.items.length; ++t) Search.setMusic(e.result.items[t], t);
                            Player.pos = -1, $("#playCtl").hasClass("pause") && $("#playCtl").click(), NtrlLngTop.next()
                        }
                    }), $(document).click(), Search.isFly = !1) : h($(this))
                })
            }), !1
        },
        flyInput: function (e, t, n) {
            n == undefined && (n = "+");
            var r = $("#schFld").val(),
                i, s = !1;
            for (var o = Search.oldStart; o != 0; --o) if (Symbol[r[o]] != undefined || o == 0) {
                    i = r.substring(0, o + 1) + e, t && (Symbol[r[o]] != undefined ? i += " " + n + " " : i += " + "), s = !0;
                    break
                }
            s || (i = e, t && (n != undefined ? i += " " + n + " " : i += " + "));
            for (var o = Search.oldStart; o < r.length; ++o) if (Symbol[r[o]] != undefined) {
                    i += r.substring(o);
                    break
                }
            r = i, r = Search.filterStr(r), t && (r = Search.deleteRepeat(r)), Search.setSchVal(r);
            var u = r.indexOf(e) + e.length;
            t && (u += 3), $("#schFld").setCursorPosition(u), Search.oldStart = u, $("#schFld").focus()
        },
        removeLastSymbol: function (e) {
            var t = e;
            for (var n = e.length - 1; n >= 0; --n) {
                if (e[n] != " " && Symbol[e[n]] == undefined) break;
                t = e.substring(0, n)
            }
            return t
        },
        filterStr: function (e) {
            var t = 1;
            for (var n = 0; n < e.length; ++n) {
                if (t == 1) {
                    if (Symbol[e[n]] != undefined || e[n] == " ") {
                        e = e.substring(0, n) + e.substring(n + 1), --n;
                        continue
                    }
                    t = 0
                }
                if (Symbol[e[n]] != undefined) {
                    var r = e[n];
                    e[n - 1] != " " || e[n + 1] != " " ? e[n + 1] == " " ? (e = e.substring(0, n) + " " + r + e.substring(n + 1), n += 1) : e[n - 1] == " " ? (e = e.substring(0, n) + r + " " + e.substring(n + 1), n += 1) : (e = e.substring(0, n) + " " + r + " " + e.substring(n + 1), n += 2) : ++n, t = 1
                }
            }
            return e
        },
        deleteRepeat: function (e) {
            var t = e.split(/ \- | \+ /g);
            for (var n = 0; n < t.length - 1; ++n) for (var r = n + 1; r < t.length; ++r) if (t[n] == t[r]) {
                        var e = "";
                        for (var i = 0; i < t.length; ++i) {
                            if (i == r) continue;
                            e += t[i], i != t.length - 1 && (e += " + ")
                        }
                        return e
                    }
            return e
        },
        getStrWidth: function (e) {
            $("#mainBd").append("<div class='temp-tip'>" + e + "</div>");
            var t = $(".temp-tip").width();
            return $(".temp-tip").remove(), t
        },
        inputKeydown: function (e) {
            if (e.keyCode == 38 || e.keyCode == 40) return !1
        },
        inputKeyup: function (e) {
            var t = $(this).val(),
                n = "";
            if (t.length == 0 && Search.autoKeywords == "") return;
            var r = 1;
            t == Search.autoKeywords && (r = 0);
            if (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 13) r = 0;
            var i = t.substring(Search.oldStart - 1, Search.oldStart),
                s = "";
            $(".tipCtn").length == 1 && (s = $(".tipCtn").children("span").data("text"));
            if (r) {
                Search.autoKeywords = t;
                var o = new Array("+", "-", "&"),
                    u = -1,
                    a = -1;
                for (var f = 0; f < o.length; ++f) {
                    var a = t.lastIndexOf(o[f]);
                    a > u && (u = a)
                }
                n = t.substring(u + 1), n = $.trim(n);
                if (n == "@") {
                    $("#atTipsCtn").hide();
                    return
                }
                n.indexOf("@") >= 0 ? Search.userUpdate(n) : Search.dataUpdate(n)
            }
            if (Symbol[i] != undefined) {
                $("#atTipsCtn").hide(), Search.updateKeywords = "at", Search.dataUpdate("");
                if (s != "" && e.keyCode != 37 && e.keyCode != 39) {
                    $(this).val($(this).val().substring(0, $(this).val().length - 1)), Search.flyInput(s, !0, Symbol[i]);
                    return
                }
                if (Search.oldStart == 1) {
                    $(this).val("");
                    return
                }
                var l = t.substring(Search.oldStart - 4, Search.oldStart - 1);
                if (Symbol[l] != undefined && Symbol[l] != " " + Symbol[i] + " ") {
                    var c = t.substring(0, Search.oldStart - 4) + " " + Symbol[i] + " ";
                    $(this).val(c);
                    return
                }
                if (Symbol[l] != undefined && Symbol[l] == " " + Symbol[i] + " ") {
                    var c = t.substring(0, Search.oldStart - 1);
                    $(this).val(c);
                    return
                }
            }
            if (e.keyCode == 37) {
                if (Symbol[i] != undefined) {
                    var h = 0;
                    for (var f = 1;; ++f) {
                        if (Search.oldStart - f == 0) break;
                        var p = t.substring(Search.oldStart - f - 1, Search.oldStart - f);
                        if (Symbol[p]) {
                            $(this).setCursorPosition(Search.oldStart - f + 1), h = 1;
                            break
                        }
                    }
                    h || $(this).setCursorPosition(0)
                }
                return
            }
            if (e.keyCode == 39) {
                var l = t.substring(Search.oldStart - 4, Search.oldStart - 1);
                if (Search.oldStart == 1 || Symbol[l] != undefined) for (var f = 1;; ++f) {
                        if (Search.oldStart + f + 1 >= t.length) break;
                        var d = t.substring(Search.oldStart + f, Search.oldStart + f + 1);
                        if (Symbol[d] != undefined) {
                            $(this).setCursorPosition(Search.oldStart + f + 2);
                            break
                        }
                } else Search.oldStart == $("#schFld").val().length && s != "" && (Search.flyInput(s, !0), Search.dataUpdate(""));
                return
            }
            if (e.keyCode == 38) {
                if ($("#atTipsCtn").isDisplay()) {
                    var v = $.makeArray($("#atTipsCtn").children());
                    for (var f = 0; f < v.length; ++f) if ($(v[f]).hasClass("selected")) {
                            $(v[f]).removeClass("selected"), f == 0 ? ($(v[v.length - 1]).addClass("selected"), Search.flyInput(Search.afterKeywords + "@" + $(v[v.length - 1]).children(".atName").children().text(), !1)) : ($(v[f - 1]).addClass("selected"), Search.flyInput(Search.afterKeywords + "@" + $(v[f - 1]).children(".atName").children().text(), !1));
                            return
                        }
                }
                return
            }
            if (e.keyCode == 40) {
                if ($("#atTipsCtn").isDisplay()) {
                    var v = $.makeArray($("#atTipsCtn").children());
                    for (var f = 0; f < v.length; ++f) if ($(v[f]).hasClass("selected")) {
                            $(v[f]).removeClass("selected"), f == v.length - 1 ? ($(v[0]).addClass("selected"), Search.flyInput(Search.afterKeywords + "@" + $(v[0]).children(".atName").children().text(), !1)) : ($(v[f + 1]).addClass("selected"), Search.flyInput(Search.afterKeywords + "@" + $(v[f + 1]).children(".atName").children().text(), !1));
                            return
                        }
                }
                return
            }
            if (e.keyCode == 8) {
                if (Symbol[i] != undefined) {
                    var h = 0,
                        m = t.substring(Search.oldStart);
                    for (var f = 1;; ++f) {
                        if (Search.oldStart - f == 0) break;
                        var p = t.substring(Search.oldStart - f - 1, Search.oldStart - f);
                        if (Symbol[p] != undefined) {
                            var g = t.substring(0, Search.oldStart - f + 1);
                            $(this).val(g + m), $(this).setCursorPosition(g.length), h = 1;
                            break
                        }
                    }
                    h || ($(this).val(m), $(this).setCursorPosition(0))
                }
                return
            }
            if (e.keyCode == 13) {
                _gaq.push(["_trackEvent", "Search", "EntrSch", "UseKeyBoardToSearch"]), Search.searchBtnClick(), $(document).click();
                return
            }
            if (Symbol[i] != undefined) {
                var g = t.substring(0, Search.oldStart - 1) + " " + Symbol[i] + " ",
                    m = t.substring(Search.oldStart);
                $(this).val(g + m), $(this).setCursorPosition(g.length)
            }
        },
        update: function (e) {
            return Search.oldStart = Core.getSelectionStart(e), !0
        },
        setSchVal: function (e) {
            $("#schFld").val(e), Search.setSchHint()
        },
        hide: function () {
            $("#atTipsCtn, #instSchCttCtn").html("").hide(), $("#instSchChange").remove(), $("#instSchCtn").hide(), $("#tbCtn").css("overflow", "hidden"), $("#flyAblum, #flySmlAblum, #mdlHr, #msgCtn, #lstdCtn").show(), Core.playerCDShow(), About.isSchFldAnimate && ($("#schFld").css({
                width: "268px"
            }), $("#mainMenuCtn").css({
                width: "331px"
            })), Search.setSchHint(), $("#leftAr").animate({
                right: "-=37"
            }, 300)
        },
        resize: function (e) {
            Search.topRowCount = parseInt((Core.bodyWidth - 40 - 40) / Search.TOP_WIDTH), Search.topColCount = parseInt((Core.bodyHeight - 50 - 100) / Search.TOP_HEIGHT), Search.topMaxCount = Search.topRowCount * Search.topColCount, Search.rowCount = parseInt((Core.bodyWidth - 260) / Search.TIP_WIDTH), Search.colCount = parseInt((Core.bodyHeight - 50 - 185) / Search.TIP_HEIGHT), Search.maxCount = Search.rowCount * Search.colCount;
            if (e != undefined) return;
            $("#languageSearchCtn").remove();
            switch (Search.TYPE) {
            case Search.TIP:
                Search.rowCount > Search.count && (Search.rowCount = Search.count);
                if (Search.rowCount == 0) {
                    Search.colCount = 0, $("#instSchCttCtn").html('<p id="languageSearchCtn" class="ntLngTip">进行自然语言搜索...</p>'), $("#instSchCtn").css({
                        width: "160px",
                        height: "30px",
                        "margin-left": "-80px",
                        "margin-top": -50 - $("#gnsCtn").height() + "px"
                    }), $("#instSchCttCtn").css({
                        width: "160px",
                        height: "30px"
                    });
                    return
                }
                Search.colCount = Search.count / Search.rowCount + "", Search.maxCount >= Search.colCount ? Search.colCount.indexOf(".") >= 0 ? Search.colCount = parseInt(Search.colCount) + 1 : Search.colCount = parseInt(Search.colCount) : Search.colCount = Search.maxCount;
                var t = Search.TIP_WIDTH * Search.rowCount,
                    n = Search.TIP_HEIGHT * Search.colCount;
                while (n > Core.bodyHeight - 75 - 185) n -= Search.TIP_HEIGHT;
                $("#instSchCtn").css({
                    width: t + "px",
                    height: n + "px",
                    "margin-left": -(t / 2) + "px",
                    "margin-top": -n / 2 - 35 - $("#gnsCtn").height() + "px"
                }), $("#instSchCttCtn").css({
                    width: t + "px",
                    height: n + "px"
                }), $("#instSchChange").css({
                    left: ($("#instSchCtn").width() - 70) / 2,
                    top: $("#instSchCtn").height() + 30
                });
                break;
            case Search.TOP:
                var t = Search.TOP_WIDTH * Search.topRowCount + 40 + 40,
                    r = Search.count / Search.topRowCount + "";
                r.indexOf(".") >= -1 ? r = parseInt(r) + 1 : r = parseInt(r), r > Search.topColCount && (r = Search.topColCount);
                var n = Search.TOP_HEIGHT * r;
                $("#instSchCtn").css({
                    width: t + "px",
                    height: n + "px",
                    "margin-left": -(t / 2) + "px",
                    "margin-top": -n / 2 - 35 - $("#gnsCtn").height() + "px"
                }), $("#instSchCttCtn").css({
                    width: t + "px",
                    height: n + "px"
                }), $("#instSchChange").css({
                    left: ($("#instSchCtn").width() - 70) / 2,
                    top: $("#instSchCtn").height() + 30
                });
                var i = 0;
                Search.count <= Search.topMaxCount ? i = Search.topRowCount : (i = Core.objLength(About.topObj) / r + "", i.indexOf(".") == -1 ? i = parseInt(i) : i = parseInt(i) + 1), $("#chartsCtn").css({
                    width: Search.TOP_WIDTH * i + "px",
                    height: Search.TOP_HEIGHT * r + "px"
                })
            }
        }
    }, Player = {
        music: new Array,
        player: null,
        msgTone: null,
        pos: -1,
        heardTmo: 0,
        listenTmo: 0,
        isStart: !1,
        isPlay: !1,
        isInsertPlay: !1,
        isControl: !0,
        currentTime: 0,
        actualCurrentTime: 0,
        volume: .8,
        isNextResponse: !0,
        isVolumeDown: !1,
        seconds: 0,
        timeDot: undefined,
        isLoveAnimate: !1,
        isReady: !1,
        isMsgToneReady: !1,
        angle: 0,
        rotateTimeout: 0,
        cdRotate: "",
        isPreload: !0,
        preloadMscList: null,
        cacheMusic: null,
        cachePos: -1,
        cacheSt: 0,
        cacheSec: 0,
        cacheTotal: 0,
        smlMusic: null,
        cachePos: -1,
        cacheSeconds: 0,
        isNextCount: 0,
        isNextTmo: 0,
        isPsnRdInsertPlay: !1,
        psnRdTmo: 0,
        loveCount: 0,
        hateCount: 0,
        ipadInit: $("html").hasClass("ipad"),
        isGnsLoad: !0,
        netCount: 0,
        vlmHideTmo: 0,
        tid: 0,
        PLAYER_TYPE: "html, flash",
        msgTongType: "msg",
        switchSong: 0,
        isNetPause: !1,
        hbr: !1,
        isHalf: !1,
        span: null,
        insertDuration: 0,
        errorCount: 0,
        errorNextCount: 0,
        MIN_VLM: .05,
        init: function () {
            Player.cdRotate = Core.opera || Core.ie69;
            var e = navigator.platform == "Win32" || navigator.platform == "Windows",
                t = isWin2000 = isWinXP = !1,
                n = navigator.userAgent;
            e ? swfobject.ua.pv[0] ? Player.PLAYER_TYPE = "flash" : Player.PLAYER_TYPE = "html, flash" : Player.PLAYER_TYPE = "html, flash", Signup.userDetail.sts.h5 != "true" && (Player.PLAYER_TYPE = "flash");
            if (Core.ipad || !Core.webkit) Player.PLAYER_TYPE = "html, flash";
            Player.player = $("#playerCore"), Player.msgTone = $("#msgTone"), $("#mainMenuCtn, #userMenuCtn, #msgCtn").removeClass("hide"), $(document).on("click", ".ablumEvent", Gns.ablumEventClick), $(document).on("click", ".psnRdEvent, #clsPsnRd", Player.psnRdClick), $(document).on("click", ".timeDotEvent", function () {
                Player.addLoading(), Player.playSeconds(parseInt($(this).prop("id").replace("timeDot", "")) + 2)
            }), $(document).on("click", ".knowEvent", function () {
                var e = $(this).data("cookieid");
                e != undefined && e != "" && e != null && Core.setCookie(e, "true"), $("#gnsCtn").data("tag", ""), Gns.openGnsArr = new Array, Gns.isOver = !1, Gns.closeGns()
            }), $(".vcOvr").click(function () {
                if ($(this).hasClass("selected")) Player.pauseMsgTone();
                else {
                    $(this).addClass("selected");
                    var e = $(this).data("type");
                    e.split(",").length == 2 && (e = Guide.mid), Player.msgTongType = e;
                    var t = "http://img.jing.fm/misc/vo/guide/" + e + ".m4a";
                    Player.isPlay && Player.playCtl(), Player.msgTone.jPlayer("setMedia", {
                        m4a: t
                    }).jPlayer("play")
                }
            }), setTimeout(function () {
                $("#userMenuCtn").animate({
                    right: "25px"
                }, 300).animate({
                    right: "15px"
                }, 100).animate({
                    right: "20px"
                }, 100)
            }, 600);
            if (Signup.userDetail.newbie == 0) {
                var r = -16;
                $("#mainMenuCtn").animate({
                    bottom: r + "px"
                }, 5)
            }
            Core.ipad || ($(document).on("mouseenter", "#menuLove, #menuMore, #menuRfrsh, #menuVo", function () {
                if (Signup.userDetail.sts.tipNtf != "true") return;
                var e = $(this).prop("id");
                $("#tps>.tpsCtn>.ctt").text(btnDes[e]);
                var t = $("#tps").width() / 2 - 12;
                $("#tps").css({
                    left: $(this).offset().left - t,
                    top: $(this).offset().top - 56
                }).show()
            }), $(document).on("mouseleave", "#menuLove, #menuMore, #menuRfrsh, #menuVo", function () {
                if (Signup.userDetail.sts.tipNtf != "true") return;
                $("#tps").hide()
            }), $(".sqrBtn, .brdBtnEvt").mouseenter(function () {
                if (Signup.userDetail.sts.tipNtf != "true") return;
                var e = $(this).prop("id"),
                    t = e;
                e == "playCtl" && ($(this).hasClass("play") ? t = e + "Play" : t = e + "Pause"), $("#tps").children(".tpsCtn").children("p").text(btnDes[t]);
                var n = $(this).offset().left - $(this).width() / 2 + 2;
                e == "gns" ? n += 3 : e == "playCtl" ? n += 10 : e == "next" ? n += 10 : e == "tkrs" ? n -= 29 : e == "frdCt" ? n -= 28 : e == "about" && (n -= 39, $("#tps>.tpsAr").css("margin-left", "6px"));
                var r = $(this).offset().top - 55;
                $("#tps").css({
                    left: n,
                    top: r + "px"
                }), (!$(this).hasClass("selected") || e == "home" || e == "playCtl") && $("#tps").show()
            }).mouseleave(function () {
                $("#tps").hide(), $(this).prop("id") == "about" && $("#tps .tpsAr").css("margin-left", "-7px")
            })), Player.msgTone.jPlayer({
                ready: function () {
                    Player.msgTone.jPlayer("volume", "1"), Player.isMsgToneReady = !0
                },
                timeupdate: function (e) {
                    var t = e.jPlayer.status.duration,
                        n = e.jPlayer.status.currentTime;
                    t - n < .5 && n > 0 && t > 0 && Player.msgTongType != "msg" && Player.pauseMsgTone()
                },
                swfPath: "http://player.jing.fm/player22.swf",
                solution: Player.PLAYER_TYPE,
                supplied: "m4a"
            }), Player.player.jPlayer({
                ready: function (e) {
                    Player.isReady = !0
                },
                timeupdate: function (e) {
                    if (Player.music[Player.pos] == undefined) return;
                    var t = e.jPlayer.status.currentTime,
                        n = 0;
                    Gns.isInsertPlay || Top.isInsertPlay ? n = parseInt(Player.insertDuration) : n = Player.music[Player.pos].duration;
                    if (n == 0 || n == null || n == "" || isNaN(n) || n == undefined || n == "undefined") n = e.jPlayer.status.duration;
                    if (n == 0 || n == null || n == "" || isNaN(n) || n == undefined || n == "undefined") n = 400;
                    var r = -1;
                    Player.player.children("audio")[0] != undefined && Player.player.children("audio")[0].buffered.length > 0 && (r = Player.player.children("audio")[0].buffered.end(0) - Player.player.children("audio")[0].buffered.start(0));
                    if (!Search.isSearch && Player.switchSong == 0 && t > 2 && Player.isNextResponse && t + Player.seconds < n - 3 && $("#playCtl").hasClass("pause") && (t == Player.statusCurrentTime || r != -1 && r - t < 1)) {
                        Signup.userDetail.sts.hbr == "true" ? Core.getCookie("lowKnow") == "true" ? Gns.nowGns("你的网速很不给力，歌曲正在辛苦加载中，缓冲一会再点播放。") : Gns.nowGns('你的网速很不给力，歌曲正在辛苦加载中，缓冲一会再点播放。尝试使用低品质音乐，打开个人主页进行设置。<a href="#" class="trg knowEvent" data-cookieid="lowKnow">不再提醒</a>') : Gns.nowGns("你的网络实在有点慢，低品质音乐也跑不动，耐心等待吧。");
                        if (!$("#top").isDisplay()) {
                            Player.playCtl(), Player.isNetPause = !0;
                            if (r != -1) var i = setInterval(function () {
                                    if (Player.isNetPause) {
                                        var e = Player.player.children("audio")[0].buffered.end(0) - Player.player.children("audio")[0].buffered.start(0);
                                        e - t > 5 && ($("#gnsCtt>.ctt").text().indexOf("你的网速很不给力，歌曲正在辛苦加载中") >= 0 && ($(".knowEvent").length > 0 ? $(".knowEvent").click() : Gns.closeGns()), Player.playCtl(), clearTimeout(i))
                                    } else clearTimeout(i)
                                }, 1e3)
                        }
                        return
                    }
                    Player.statusCurrentTime = t, t > .5 && t < 2.5 && Player.isNextResponse && $("#playCtl").hasClass("loading") && (Signup.userDetail.id == About.ouid && About.startPt(), $("#playCtl").removeClass("loading"), $("#mscPlr").data("mouse") == "false" && $("#mscPlrMask").mouseleave(), Player.spin.data().spinner.stop(), Player.spin = null, Player.startRotate());
                    if (Top.isInsertPlay) {
                        t > 0 && n > 0 && n - t < 1 && Top.obj != null && !Player.isExec && Top.playOver();
                        return
                    }
                    if (Gns.isInsertPlay) {
                        t > 0 && n > 0 && n - t < 1 && Gns.insertStop("continue");
                        return
                    }
                    t = e.jPlayer.status.currentTime + Player.seconds;
                    if (Player.currentTime != parseInt(t) && Gns.timeDot != undefined && Player.currentTime > 5 && Gns.timeDot["timeDot" + Player.currentTime] != undefined) {
                        var s = Gns.timeDot,
                            o = s["timeDot" + Player.currentTime][1];
                        s["timeDot" + Player.currentTime][1] = o.substring(0, o.indexOf("在")) + "正在演奏", Gns.openGnsArr[Gns.openGnsArr.length] = s["timeDot" + Player.currentTime], Gns.showGns()
                    }
                    Player.actualCurrentTime = parseInt(t), Player.currentTime = parseInt(t), Player.currentTime > n / 2 && !Player.isHalf && (Player.isHalf = !0, $.ajax({
                        url: Core.API_VER + "/music/post_half",
                        data: {
                            uid: Signup.userDetail.id,
                            tid: Player.music[Player.pos].tid
                        }
                    }));
                    var u = t / n * 100;
                    u = Core.bodyWidth / 100 * u, $("#prgrsCtn .crt").css({
                        width: u + "px"
                    });
                    var a = parseInt(t / 60),
                        f = parseInt(t % 60);
                    a < 10 && (a = "0" + a), f < 10 && (f = "0" + f), $("#timeTps>.tpsCtn>p").text(a + ":" + f);
                    var l = 45;
                    u > Core.bodyWidth - 55 ? (l = 45 + u - (Core.bodyWidth - 55), l > 85 && (l = 85), u = Core.bodyWidth - 55) : u < 52 && (l = u - 6, l < 3 && (l = 3), u = 52), $("#timeTps>.tpsAr").css({
                        left: l + "px"
                    }), $("#timeTps").css({
                        top: "15px",
                        left: u - 45 + "px"
                    }), n - t < .5 && t > 0 && (Player.isVolumeDown || Player.next(0));
                    if (n - t < 60 && Player.isPreload && Player.music.length > Player.pos + 1) {
                        Player.isPreload = !1;
                        var c = new Image;
                        c.src = $.id2url(Player.music[Player.pos + 1].fid, "AM", "album")
                    }
                    if (t > 11 && t < 12) {
                        var c = new Image;
                        c.src = $.id2url(Player.music[Player.pos].fid, "AT", "album")
                    }
                },
                error: function (e) {
                    if (e.jPlayer.error.type != "e_flash") {
                        console.log("media error-->" + e.jPlayer.error.type);
                        var t = Player.errorCount;
                        if (t == 2) {
                            ++Player.errorNextCount;
                            if (Player.errorNextCount >= 4) return;
                            Player.nextTmo = setTimeout(function () {
                                $("#playerNext").click()
                            }, 1e3);
                            var n = new Object;
                            n.uid = Signup.userDetail.id + "", n.c = "703", n.p = "web/" + Core.VERSION, n.t = (new Date).toGMTString() + "", n.ext = new Object, n.ext.tid = Player.music[Player.pos].tid, n.ext.d = e.jPlayer.error.type;
                            var r = new Array;
                            r[0] = n, $.ajax({
                                type: "POST",
                                url: "/feedbacks/post_error",
                                data: JSON.stringify(r)
                            })
                        } else Player.play(), Player.errorCount = t + 1
                    }
                },
                volume: Player.volume,
                swfPath: "http://player.jing.fm/player22.swf",
                solution: Player.PLAYER_TYPE,
                wmode: "window",
                supplied: "m4a"
            }), setTimeout(function () {
                Player.savePlayingData(!0)
            }, 1e4)
        },
        savePlayingData: function (e) {
            if (e == 1) {
                var t = 0;
                Player.currentTime < 30 ? t = 10 : t = 30, setTimeout(function () {
                    Player.savePlayingData(!0)
                }, 1e3 * t)
            }
            if (Signup.userDetail.newbie != 0 || Player.isPsnRdInsertPlay || About.isSingleInsertPlay || About.isInsertPlay || About.isTopInsertPlay || About.isAtInsertPlay || NtrlLngTop.isInsertPlay || Top.isInsertPlay || Flw.isFlw && Flw.toid != "") return;
            if (Player.isPlay && Player.music[Player.pos] != undefined && Flw.cacheMusic == null) {
                var n = Search.keywords,
                    r = Player.currentTime,
                    i = Player.music[Player.pos].tid,
                    s = Player.music[Player.pos].atn;
                $.ajax({
                    url: Core.API_VER + "/click/playdata/post",
                    data: {
                        uid: Signup.userDetail.id,
                        cmbt: n,
                        tid: i,
                        ct: r
                    }
                }), Core.setCookie("jing.volume", Player.volume)
            }
            Player.music.length == 0 && $.ajax({
                url: Core.API_VER + "/click/playdata/post",
                data: {
                    uid: Signup.userDetail.id,
                    isclear: !0
                }
            })
        },
        load: function () {
            var e = "";
            Player.cdRotate ? e = '<div id="rotateCtn" class="cv"><div id="rotateFlash"></div></div>' : e = '<div id="rotateCD" class="cv"></div>';
            var t = "dspr";
            if ($("html").hasClass("ie6") || $("html").hasClass("ie7") || $("html").hasClass("ie8")) t = "";
            $("#mainBd").prepend('<div id="prgrsCtn" class="prgrsCtn ' + t + '" style="height:15px">' + '<div id="timeTps" class="tps time hide">' + '<div class="tpsAr"></div>' + '<div class="tpsLbd"></div>' + '<div class="tpsCtn">' + '<div class="inspt"></div>' + '<p class="ctt serif"></p>' + "</div>" + '<div class="tpsRbd"></div>' + "</div>" + '<span class="prgrs"></span>' + '<span class="prgrs crt"></span>' + '<div class="timeTpsCtn hide">' + '<div class="tpsAr"></div>' + '<div class="tpsCtn"></div>' + "</div>" + "</div>" + '<div id="mdlHr" class="mdlHr"></div>' + '<div id="mscPlrCtn" class="mscPlrCtn absCt">' + '<div id="vlmCtrl" class="vlmCtrl">' + '<div class="vlmIcon"></div>' + '<div class="vlmCtn">' + '<div class="vlmSlider">' + '<a href="#" class="sldr"></a>' + "</div>" + "</div>" + "</div>" + '<div id="rMenu" href="#" class="plyrBtns rMenu" data-tps="">' + '<div id="rMenuCtn" class="rMenuCtn">' + '<a href="#" class="rBtns" id="topAppBtn">音乐瀑布</a>' + '<a href="#" class="rBtns" id="loveRd">红心电台</a>' + '<a href="#" class="rBtns" id="psnRd">智能推荐</a>' + '<a href="#" class="rBtns" id="chart">排行榜</a>' + '<a href="#" class="rBtns" id="gnsBtn">随便听听</a>' + "</div>" + "</div>" + '<div id="mscPlrMask" class="mscPlrMask"><div id="playCtl" class="playCtlCtn"><a href="#" class="playCtl"></a></div></div>' + '<div id="mscPlr" class="mscPlr">' + e + '<p class="tit">Jing 已经为你准备好，搜索你想听的</p>' + "</div>" + '<div class="mscPlrBtnCtn">' + '<div data-tps="喜欢" class="plrBtnBg love">' + '<a id="playerLove" href="#" class="plrBtn lwRt overlay"></a>' + "</div>" + '<div data-tps="讨厌" class="plrBtnBg hate">' + '<a id="playerHate" href="#" class="plrBtn lwRt overlay"></a>' + "</div>" + '<div data-tps="单曲循环" class="plrBtnBg rptOne">' + '<a id="playerRptOne" href="#" class="plrBtn lwRt overlay"></a>' + "</div>" + '<div data-tps="换歌" class="plrBtnBg next">' + '<a id="playerNext" href="#" class="plrBtn lwRt overlay"></a>' + "</div>" + "</div>" + "</div>"), Core.ie68 || ($("#vlmCtrl").css("opacity", "0.2"), $(".plyrBtns").css("opacity", "0.5")), $("#ntrlLngBtn").show(), $("#mscPlrMask").mouseenter(function () {
                $("#mscPlr").data("mouse", "true"), Core.ie68 ? $("#playCtl").show() : $("#playCtl").css("opacity", "0").css({
                    opacity: "1"
                })
            }).mouseleave(function () {
                $("#mscPlr").data("mouse", "false");
                if ($("#playCtl").hasClass("loading") || $("#playCtl").hasClass("play")) return;
                Core.ie68 ? $("#playCtl").hide() : $("#playCtl").css({
                    opacity: "0"
                })
            }), $("#mscPlrMask").mouseleave(), $("#playCtl").click(Player.playCtl), $("#playerNext").click(function () {
                $("#playerRptOne").hasClass("selected") && $("#playerRptOne").click();
                var e = $("#schFld").val();
                e = Search.filterStr(e), e = Search.removeLastSymbol(e), e == Search.keywords || e == "" ? (e == "" && Search.setSchVal(Search.keywords), Player.currentTime < 10 && (clearTimeout(Player.isNextTmo), Player.isNextTmo = setTimeout(function () {
                    Player.isNextCount = 0
                }, 15e3), ++Player.isNextCount), Player.postNext(), clearTimeout(Gns.gnsRcmdTmo), Gns.gnsRcmdTmo = setInterval(Gns.rcmd, 6e5), Player.next(400), Player.isNextCount == 3 && (Player.isNextCount = 0, setTimeout(function () {
                    Gns.rcmd()
                }, 500)), _gaq.push(["_trackEvent", "Player", "Next", Player.music[Player.pos].tid])) : (_gaq.push(["_trackEvent", "Search", "NxtSch", "NextToSearch"]), Search.searchBtnClick())
            }), $(document).on("click", "#clsRptOne", function () {
                $("#playerRptOne").click()
            }), $("#playerRptOne").click(function () {
                $(this).hasClass("selected") ? ($(this).removeClass("selected"), Player.isPsnRdInsertPlay || About.isInsertPlay || About.isTopInsertPlay || About.isAtInsertPlay || NtrlLngTop.isInsertPlay || About.isSingleInsertPlay ? ($("#rptOneHtml").remove(), Core.fullScrenMenuShow("", "rptOne")) : Core.fullScrenMenuHide()) : (_gaq.push(["_trackEvent", "Player", "RptOne", Player.music[Player.pos].tid]), $(this).addClass("selected"), Player.isPsnRdInsertPlay || About.isInsertPlay || About.isTopInsertPlay || About.isAtInsertPlay || NtrlLngTop.isInsertPlay || About.isSingleInsertPlay ? Core.fullScrenMenuShow('<span id="rptOneHtml">单曲循环模式 <span class="spl"></span><a href="#" id="clsRptOne" class="clsFlwLstn overlay">退出</a></span>', "rptOne") : Core.fullScrenMenuShow('你正在单曲循环模式 <span class="spl"></span><a href="#" id="clsRptOne" class="clsFlwLstn overlay">退出</a>', "rptOne"))
            }), $(".plrBtnBg").mouseenter(function () {
                if (Signup.userDetail.sts.tipNtf != "true") return;
                var e = $(this).offset().top - 50;
                $("#tps").addClass("guide"), $("#tps").show().children(".tpsCtn").children("p").text($(this).data("tps"));
                var t = $(this).offset().left - $("#tps").width() / 2 + 50;
                $("#tps").css({
                    left: t,
                    top: e + "px"
                })
            }).mouseleave(function () {
                $("#tps").hide(), $("#tps").removeClass("guide")
            }), $("#vlmCtrl").mouseenter(function () {
                clearTimeout(Player.vlmHideTmo), Core.ie68 || $(this).css("opacity", "1")
            }).mouseleave(function () {
                Core.ie68 || $(this).css("opacity", "0.2")
            }), $(".plyrBtns").mouseenter(function () {
                clearTimeout(Player.vlmHideTmo), Core.ie68 || $(this).css("opacity", "1")
            }).mouseleave(function () {
                Core.ie68 || $(this).css("opacity", "0.5")
            }), $("#rMenu").mouseenter(function () {
                Player.isRMenuClose = !1;
                if (Player.isRMenu && $("#rMenuCtn").isDisplay()) return;
                Player.isRMenu = !0, setTimeout(function () {
                    Player.isRMenu = !1, Player.isRMenuClose && $("#rMenu").mouseleave()
                }, 1e3), $("#rMenuCtn").show(), $("#rMenuCtn>a").each(function (e) {
                    $(this).animate({
                        top: e * 27 + 15 - 15 / (e + 1) + "px"
                    }, 300, function () {
                        $(this).animate({
                            top: e * 27 + 15 + 10 / (e + 1) + "px"
                        }, 300, function () {
                            $(this).animate({
                                top: e * 27 + 15 + "px"
                            }, 300)
                        })
                    })
                })
            }).mouseleave(function () {
                if (Player.isRMenu) {
                    Player.isRMenuClose = !0;
                    return
                }
                Player.isRMenu = !0, $("#rMenuCtn>a").animate({
                    top: "160px"
                }, 300, function () {
                    $("#rMenuCtn").hide(), Player.isRMenu = !1
                })
            }), $("#topAppBtn").click(function () {
                return _gaq.push(["_trackEvent", "Main", "Top", "EnterJingTopCharts"]), Top.show(), !1
            }), $("#psnRd").click(function () {
                return _gaq.push(["_trackEvent", "Main", "PsnRd", "ListenToPersonalRadio"]), Player.psnRdClick(), !1
            }), $("#chart").click(function () {
                return _gaq.push(["_trackEvent", "Main", "Chart", "EnterChartsList"]), Search.schBxCtnClick("榜单"), !1
            }), $("#ntrlLngBtn").click(function () {
                return _gaq.push(["_trackEvent", "Main", "NtrlLngTop", "EnterChartsList"]), NtrlLngTop.switching(), !1
            }), $(".sldr").mousedown(function () {
                return Flw.isFlw && Flw.toid != "" ? Gns.nowGns("你正在跟听状态中，将跟对方音量同步，不能自己调节") : (document.onmousemove = function (e) {
                    e = e || window.event;
                    var t, n;
                    if (e.pageX || e.pageY) t = e.pageX, n = e.pageY;
                    t = e.clientX + document.body.scrollLeft - document.body.clientLeft, n = e.clientY + document.body.scrollTop - document.body.clientTop;
                    var r = t - $(".vlmSlider").offset().left;
                    r < 2.25 && (r = 2.25), r > 45 && (r = 45), $(".vlmSlider").css("width", r + "px"), Player.volume = r / 45, Player.player.jPlayer("volume", Player.volume)
                }, document.onmouseup = function (e) {
                    document.onmousemove = null, document.onmouseup = null
                }), !1
            });
            var n = Number(Core.getCookie("jing.volume"));
            if (n == null || isNaN(n) || n == "") n = 1;
            Player.setVolume(n, !1), Player.volume = n;
            var r = {
                wmode: "opaque"
            }, i = {}, s = {};
            swfobject.embedSWF("http://jing.fm/assets/vendor/rt.swf", "rotateFlash", "300", "300", "9.0.0", "", s, r, i), Player.resize(), $("#mscPlrCtn").animate({
                opacity: 1
            }, 500), $("#prgrsCtn").animate({
                opacity: 1
            }, 2e3), $("#playerHate").click(Player.hateClick), $("#playerLove").click(Player.loveClick), $("#playerPsnRd").click(Player.psnRdClick), $(".ovlCtnBg, .cls").click(function () {
                $(document).click()
            });
            var o;
            $("#prgrsCtn").mouseenter(function () {
                if (Player.currentTime == 0) return;
                o = setTimeout(function () {
                    $("#timeTps").show()
                }, 300)
            }).mouseleave(function () {
                clearTimeout(o), $("#timeTps").hide()
            }), Player.cdRotate || Player.ready()
        },
        ready: function () {
            Player.cdRotate && Player.swfObj.showCover(IMG_URL + "/defaults/cover/300.jpg");
            var e = Signup.userDetail.pld;
            if (e != null) {
                $("#schFld").val(e.cmbt), Search.setSchHint(), Player.music = new Array, Search.setMusic(e, 0), Player.pos = 0;
                var t = function () {
                    Player.isReady ? Player.ipadInit ? ($("#playCtl").removeClass("loading").addClass("play"), Player.spin.data().spinner.stop(), Player.spin = null, Player.setPlayerUI()) : (Player.player.jPlayer("volume", Player.volume), $("#playCtl").removeClass("play").addClass("pause"), Player.play(parseInt(e.ct))) : setTimeout(t, 300)
                };
                t(), Search.searchByKeywords(e.cmbt, 1, 0)
            }
            if (Signup.userDetail.newbie == 2) Guide.init(), Message.isLoginInit = !0, Guide.step1();
            else if (Signup.userDetail.newview.m.length != 0) {
                var n = Signup.userDetail.newview.m;
                for (var r = 0; r < n.length; ++r) {
                    var i = !0;
                    for (var s = 0; s < SnsArr.length; ++s) if (n[r].indexOf("-" + SnsArr[s]) >= 0) {
                            Signup.iconShake("frdCtNewIcon"), i = !1;
                            break
                        }
                    i && Signup.iconShake("aboutNewIcon")
                }
            } else Signup.userDetail.newview.t.length != 0 && Signup.iconShake("aboutNewIcon")
        },
        playSeconds: function (e) {
            Player.seconds = e, Player.setVolumeDown("jump", 50)
        },
        setPlay: function (e, t, n) {
            Player.errorCount = 0, clearTimeout(Player.switchSong), Player.switchSong = setTimeout(function () {
                Player.switchSong = 0
            }, 2e3), e == undefined && (e = 0), Player.tid = parseInt(n), Flw.isServer && Flw.send("play", e), Player.seconds = e;
            if (Player.statusCurrentTime >= 10) {
                var r = parseInt(Player.statusCurrentTime);
                r > Player.music[Player.pos].duration && (r = Player.music[Player.pos].duration), $.ajax({
                    url: Core.API_VER + "/click/playduration/post",
                    data: {
                        uid: Signup.userDetail.id,
                        tid: Player.music[Player.pos].tid,
                        d: r
                    }
                })
            }
            var i = "";
            Signup.userDetail.sts.hbr == "true" ? i = "NO" : i = "MM";
            if (Core.ipad) {
                var s = $.id2mediaUrl(t, "audio") + "?start=" + e;
                Player.player.jPlayer("setMedia", {
                    m4a: s
                }).jPlayer("play"), e < 1 ? Player.player.jPlayer("volume", Player.volume) : Player.setVolumeUp()
            } else $.ajax({
                    url: Core.API_VER + "/media/song/surl",
                    data: {
                        mid: t,
                        type: i
                    },
                    success: function (t) {
                        if (!t.success) return;
                        var n = t.result + "?start=" + e;
                        Player.player.jPlayer("setMedia", {
                            m4a: n
                        }).jPlayer("play"), e < 1 ? Player.player.jPlayer("volume", Player.volume) : Player.setVolumeUp()
                    }
                });
            $("#mscPlrCtn").data({
                tid: n,
                mid: t
            }), Player.currentTime = 0, Player.isPlay = !0, Player.isControl = !1, About.ouid == Signup.userDetail.id && About.startPt()
        },
        setPlayerUI: function () {
            if (Player.music[Player.pos] == undefined) return;
            var e = $("#mscPlr").children("p").text(),
                t = $("#mscPlr").children("p").data("fid");
            if (e == Player.music[Player.pos].name && t == Player.music[Player.pos].fid) {
                $("#playCtl").hasClass("play") && Player.playCtl();
                return
            }
            var n = $.id2url(Player.music[Player.pos].fid, "AM", "album");
            $("#mscPlr").children("p").text(Player.music[Player.pos].name).append('<a href="#" id="shrIcon" class="shrIcon"></a>'), Core.ie68 || $("#shrIcon").css("opacity", "0.5"), $("#shrIcon").mouseenter(function () {
                if (Guide.shrData != null) return;
                Core.ie68 || $(this).css("opacity", "1"), Apps.friendMenu("shr", $(this)), $(".usrTps").click(function () {
                    var e = $(this).data("identify"),
                        t = !0;
                    for (var n = 0; n < resData.frdCt.length; ++n) if (resData.frdCt[n].mid == e && resData.frdCt[n].display == "true") {
                            t = !1;
                            var r = {
                                uid: Signup.userDetail.id,
                                tid: Player.music[Player.pos].tid,
                                c: "",
                                identify: ConverSns[e + "1"]
                            };
                            if (Search.moods != null) {
                                var i = "";
                                for (var n = 0; n < Search.moods.length; ++n) n + 2 == Search.moods.length ? i += Search.moods[n] + "和" : i += Search.moods[n] + "、";
                                r.c = "我" + i.substring(0, i.length - 1) + "的时候喜欢听这首 ♫" + Player.music[Player.pos].name
                            } else r.c = "我分享了一首歌 ♫" + Player.music[Player.pos].name + " ，快来听听吧。";
                            Guide.shrData = r, Signup.userDetail.sts.autoSnc == "true" ? (_gaq.push(["_trackEvent", "Player", "Shr" + ConverSns[Guide.shrData.identify].toUpperCase(), Player.music[Player.pos].tid]), Guide.shrSend()) : Guide.shrShow();
                            return
                        }
                    t && Gns.nowGns("你还没绑定 " + btnDes[ConverSns[e + "1"]] + '，暂时不能分享。<a href="#" class="trg gnsSnsBind" data-identify="' + e + '">需要绑定吗？</a>')
                })
            }).mouseleave(function () {
                Core.ie68 || $(this).css("opacity", "0.5"), $("#usrTpsCtn").remove()
            }), $("#mscPlr").children("p").data("fid", Player.music[Player.pos].fid), Player.cdRotate ? (Player.swfObj.showCover(n), $("#playCtl").hasClass("play") && Player.playCtl()) : $("#rotateCD").animate({
                opacity: "0"
            }, 300, function () {
                $("#rotateCD").css({
                    "background-image": 'url("' + n + '")'
                }), $(this).animate({
                    opacity: "1"
                }, 300, function () {
                    $("#playCtl").hasClass("play") && !Player.ipadInit && Player.playCtl()
                })
            })
        },
        play: function (e) {
            var t = Player.music[Player.pos];
            Player.music.length == 0 && Player.setPlayerDefault();
            if (Player.music.length <= Player.pos || t == undefined) return;
            if (t.duration < 1) {
                Player.next();
                return
            }
            $("#playerLove, #playerHate").removeClass("selected"), Player.ipadInit ? (Player.addLoading(), Player.ipadInit = !1) : Player.setPlayerUI(), e == undefined && (e = 0), Player.setPlay(e, t.mid, t.tid), Player.isPreload = !0, e == 0 && (Player.isHalf = !1), Player.hbr ? (Player.hbr == "true" ? Gns.nowGns("已经切换至高品质音乐") : Player.hbr == "false" && Gns.nowGns("已经切换至低品质音乐"), $("#playCtl").removeClass("play").addClass("pause"), Player.addLoading()) : ($.ajax({
                url: Core.API_VER + "/music/fetch_track_infos",
                data: {
                    uid: Signup.userDetail.id,
                    tid: t.tid
                },
                success: Gns.mscInfoHtml
            }), clearTimeout(Player.heardTmo), Player.heardTmo = setTimeout(function () {
                $.ajax({
                    url: Core.API_VER + "/music/post_heard_song",
                    data: {
                        uid: Signup.userDetail.id,
                        tid: t.tid
                    }
                })
            }, t.duration / 2 * 1e3), clearTimeout(Player.listenTmo), Player.listenTmo = setTimeout(function () {
                Core.nowIsReady && now.postListening(Signup.userDetail.id, t.tid)
            }, 5e3)), Player.hbr = !1
        },
        closeGns: function () {
            var e = new Array;
            for (var t = 0; t < Gns.openGnsArr.length; ++t) Gns.openGnsArr[t][0] == "now" && (e[e.length] = Gns.openGnsArr[t]);
            Gns.openGnsArr = e, Gns.openGnsArr.length == 0 && (clearTimeout(Gns.closeGnsTmo), Gns.closeGns())
        },
        next: function (e) {
            if (About.isInsertPlay || About.isTopInsertPlay || About.isAtInsertPlay || About.isSingleInsertPlay) {
                About.next();
                return
            }
            if (NtrlLngTop.isInsertPlay) {
                NtrlLngTop.next();
                return
            }
            if (!Player.isNextResponse || Search.isSearch || Flw.isFlw && !Flw.isServer) return;
            setTimeout(function () {
                Player.isNextResponse = !0
            }, 1e3), Player.isNextResponse = !1, Player.closeGns(), e != 0 && (e = 400), $("#playCtl").removeClass("play").addClass("pause"), Player.addLoading(), $("#lstdCtn").html(""), Gns.isInsertPlay && Gns.insertStop("next"), Player.isControl = !0, $("#playerRptOne").hasClass("selected") || ++Player.pos;
            if (Player.music.length <= Player.pos) {
                if (Player.isPsnRdInsertPlay && Player.psnRdTmo == 0) {
                    var t = function () {
                        Player.psnRdCount <= Search.st && (Search.st = 0), $.ajax({
                            url: Core.API_VER + "/app/fetch_psnrd",
                            data: {
                                uid: Signup.userDetail.id,
                                st: Search.st,
                                ps: 5
                            },
                            success: function (n) {
                                n.success || Gns.nowGns("Jing 开了点小差，你过会儿再试试。");
                                if (n.result.items.length == 0) {
                                    Search.st != 0 ? (Search.st = 0, t()) : ($("#gnsCtn").data("tag") == Gns.level1 && $("#gnsCtn").data("tag", ""), Gns.nowGns("你需要至少喜欢3首歌曲之后，Jing才能为你开启智能推荐功能。"), Player.psnRdClick());
                                    return
                                }
                                $("#gnsCtn").data("tag") == Gns.level1 && Core.fullScrenMenuShow('<span id="prevHtml">你正在收听智能推荐电台 <span class="spl"></span><a href="#" id="clsPsnRd" class="clsFlwLstn overlay">退出</a></span>', "psnRd"), Player.psnRdCount = n.result.total, Search.st = Search.st + n.result.items.length, $("#gnsCtn").data("tag", ""), Core.getCookie("psnRdKnow") == "true" ? Gns.closeGns() : Core.ipad ? Gns.nowGns('你已经启用个人电台，下一首双指向左滑动封面，暂停请用双指点击封面，再次点击个人电台图标退出，<a href="#" class="trg knowEvent" data-cookieid="psnRdKnow">我知道了</a>。', Gns.level1) : Gns.nowGns('你已经启用个人电台，下一首请按 -> 键，暂停请按空格键，再次点击个人电台图标退出，<a href="#" class="trg knowEvent" data-cookieid="psnRdKnow">我知道了</a>。', Gns.level1), Player.music = new Array;
                                for (var r = 0; r < n.result.items.length; ++r) Search.setMusic(n.result.items[r], r);
                                Player.pos = 0, Player.setVolumeDown("play", e)
                            }
                        })
                    };
                    t();
                    return
                }
                Player.loveCount > 0 ? $.ajax({
                    url: Core.API_VER + "/search/fetch_rcmd_next",
                    data: {
                        tid: Player.music[Player.music.length - 1].tid,
                        uid: Signup.userDetail.id,
                        cmbt: Search.keywords
                    },
                    success: function (t) {
                        Player.loveCount = 0;
                        if (!t.success || t.result.items.length == 0) {
                            Search.st = Search.st + Search.ps, Search.total <= Search.st && (Search.st = 0), Search.searchByKeywords(Search.keywords, 0, Search.st);
                            return
                        }
                        for (var n = 0; n < t.result.items.length; ++n) Search.setMusic(t.result.items[n], n);
                        Player.pos = 0, Player.setVolumeDown("play", e)
                    }
                }) : (Search.st = Search.st + Search.ps, Search.total <= Search.st && (Search.st = 0), Search.searchByKeywords(Search.keywords, 0, Search.st))
            } else Player.setVolumeDown("play", e)
        },
        playCtl: function (e) {
            if (Player.music.length != 0 && !$("#playCtl").hasClass("loading") || typeof e == "string") {
                Player.isNetPause = !1, clearTimeout(Player.switchSong), Player.switchSong = setTimeout(function () {
                    Player.switchSong = 0
                }, 2e3), $("#tps").hide();
                if ($("#playCtl").hasClass("play") || e == "play") Player.msgTongType != "msg" && ($("#menuVo").removeClass("selected"), Player.pauseMsgTone()), Player.playing(), Signup.userDetail.id == About.ouid && About.startPt(), $("#mscPlr").data("mouse") == "false" && $("#mscPlrMask").mouseleave();
                else {
                    Player.pause();
                    var t = $("#mscPlr").data("mouse");
                    $("#mscPlrMask").mouseenter(), $("#mscPlr").data("mouse", t), Signup.userDetail.id == About.ouid && About.stopPt()
                }
                return !1
            }
            return
        },
        playerUI: function () {
            $("#playCtl").removeClass("play").addClass("pause"), Player.startRotate(), Player.isPlay = !0, Player.setPlayerUI()
        },
        playing: function () {
            if (Player.isVolumeDown) return;
            Flw.isServer && Flw.send("playing");
            if (Player.ipadInit) {
                Player.player.jPlayer("volume", Player.volume), Player.play(parseInt(Signup.userDetail.pld.ct));
                return
            }
            Gns.isInsertPlay ? (Player.setVolumeDown("pause", 400), Gns.insertStop("continue")) : (Player.setPlay(Player.currentTime, Player.music[Player.pos].mid, Player.music[Player.pos].tid), $("#playCtl").removeClass("play").addClass("pause"), Player.addLoading())
        },
        pause: function () {
            Flw.isServer && Flw.send("pause"), $("#playCtl").removeClass("pause").addClass("play"), Player.stopRotate(), Player.setVolumeDown("pause", 400), Flw.isFlw && Flw.toid != "" ? Player.player.jPlayer("pause") : Player.setVolumeDown("pause", 400), Player.isPlay = !1
        },
        pauseMsgTone: function () {
            Player.msgTongType = "msg", $(".vcOvr").removeClass("selected"), Player.msgTone.jPlayer("pause"), Player.isPlay || Player.playCtl()
        },
        setVolumeDown: function (e, t) {
            if (e == "msgTone" && Signup.userDetail.sts.rmdTone != "true") return;
            if (Flw.isFlw && Flw application.js: 9TypeError: 'undefined'
            is not an object(evaluating 'Player.music[Player.pos].tid')
                .toid != "") return;
            var n = Player.MIN_VLM;
            e == "msgTone" && (n = .2), Player.isVolumeDown = !0;
            var r = 0,
                i, s = function () {
                    ++r;
                    var s = Player.player.jPlayer("option", "volume") - .02;
                    if (s <= n || r >= t) Player.isVolumeDown = !1, Player.player.jPlayer("volume", n), clearInterval(i), e == "pause" ? Player.player.jPlayer("pause") : e == "play" ? ($("#playerLove").removeClass("selected"), Player.play()) : e == "jump" ? Player.setPlay(Player.seconds, Player.music[Player.pos].mid, Player.music[Player.pos].tid) : e == "msgTone" && (Player.msgTone.jPlayer("setMedia", {
                            m4a: "http://player.jing.fm/msgTone.m4a"
                        }).jPlayer("play"), setTimeout(function () {
                            Player.setVolumeUp()
                        }, 1e3));
                    Player.player.jPlayer("volume", s)
                };
            Player.isPlay || Player.player.jPlayer("volume", "0"), t == 0 ? s() : i = setInterval(s, t / 50)
        },
        setVolumeUp: function (e) {
            if (Flw.isFlw && Flw.toid != "") return;
            e = 0;
            var t = 0,
                n, r = function () {
                    if (Player.isVolumeDown) return;
                    ++t;
                    var r = Player.player.jPlayer("option", "volume") + .02;
                    if (r >= Player.volume || t > e) {
                        clearInterval(n), Player.player.jPlayer("volume", Player.volume);
                        return
                    }
                    Player.player.jPlayer("volume", r)
                };
            r()
        },
        setVolume: function (e, t) {
            if (Flw.isFlw && Flw.toid != "" && Player.volume == e) return;
            Player.player.jPlayer("volume", e), t != 0 && Player.showVolume(), $(".vlmSlider").css("width", .45 * e * 100 + "px"), Player.hideVolume()
        },
        showVolume: function () {
            clearTimeout(Player.vlmHideTmo), Core.ie68 || $("#vlmCtrl").css("opacity", "1")
        },
        hideVolume: function () {
            clearTimeout(Player.vlmHideTmo), Player.vlmHideTmo = setTimeout(function () {
                Player.vlmHideTmo = 0, Core.ie68 || $("#vlmCtrl").css("opacity", "0.2")
            }, 2e3)
        },
        volumeUp: function () {
            var e = Player.volume + .2;
            e > 1 && (e = 1), Flw.isServer && Flw.send("volume," + e), Player.setVolume(e), Player.volume = e
        },
        volumeDown: function () {
            var e = Player.volume - .2;
            e < .2 && (e = Player.MIN_VLM), Flw.isServer && Flw.send("volume," + e), Player.setVolume(e), Player.volume = e
        },
        musicUpdate: function (e) {
            var t = Player.music;
            Player.music = new Array, Player.music[0] = t[Player.pos];
            for (var n = 0; n < e.length; ++n) Search.setMusic(e[n], n + 1);
            Player.pos = 0
        },
        loveClick: function () {
            if (Player.pos == -1) {
                Gns.nowGns("你貌似还没有播放呢，在搜索框输入你想听的音乐吧！");
                return
            }
            if (Player.isLoveAnimate) return;
            if (Player.currentTime < 15 && !$("#playerLove").hasClass("selected")) {
                Gns.nowGns("如果你真的喜欢，至少会听15秒～");
                return
            }
            _gaq.push(["_trackEvent", "Player", "Love", Player.music[Player.pos].tid]), Player.isLoveAnimate = !0, ++Player.loveCount, Player.hateCount = 0;
            var e = "";
            Player.isPsnRdInsertPlay ? e = "psn" : e = Search.keywords, clearTimeout(Gns.gnsRcmdTmo), Gns.gnsRcmdTmo = setInterval(Gns.rcmd, 6e5), $("#playerLove").toggleClass("selected"), $("#playerHate").removeClass("selected"), $("#playerLove").hasClass("selected") && (Flw.isFlw && Flw.toid != "" && Flw.send("love," + Signup.userDetail.nick), Player.loveAnimate(20, 15, 50)), $.ajax({
                url: Core.API_VER + "/music/post_love_song",
                data: {
                    uid: Signup.userDetail.id,
                    tid: Player.music[Player.pos].tid,
                    c: Player.loveCount,
                    cmbt: e,
                    moodTagIds: Search.moodids
                },
                success: function (e) {
                    $("#playerLove").hasClass("selected") && (e.result.items.length > 0 ? Player.musicUpdate(e.result.items) : --Player.loveCount), Player.isLoveAnimate = !1
                }
            })
        },
        loveAnimate: function (e, t, n) {
            var r = $.id2url(Player.music[Player.pos].fid, "AT", "album"),
                i = Core.bodyWidth / 2 - 25,
                s = Core.bodyHeight / 2;
            $("#gnsCtn").isDisplay() && (s += 74), $("#mainBd").append("<div id='flyLoveTip' class='ufo hide' style='right:" + i + "px; bottom:" + s + "px; background-image:url(" + r + ");'/>"), $("#flyLoveTip").show().animate({
                right: "-=180",
                bottom: "+=160"
            }, 500, function () {
                $(this).animate({
                    right: e + "px",
                    bottom: t + "px"
                }, 600, function () {
                    $(this).animate({
                        opacity: "0"
                    }, 500, function () {
                        $(this).remove(), $("#mainBd").append('<img id="flyPlusTip" src="' + IMG_URL + "/plusOne" + Retina.suffix + '.png" style="right:' + (e + 19) + "px; bottom:" + (t + 14) + 'px; position:absolute; z-index:9999"/>'), Core.ie68 ? $("#flyPlusTip").animate({
                            bottom: "+=" + n
                        }, 1e3, function () {
                            $(this).remove(), Player.isLoveAnimate = !1
                        }) : $("#flyPlusTip").animate({
                            bottom: "+=" + n,
                            opacity: "0"
                        }, 1e3, function () {
                            $(this).remove(), Player.isLoveAnimate = !1
                        })
                    })
                })
            })
        },
        hateClick: function () {
            if (Player.pos == -1) {
                Gns.nowGns("你貌似还没有播放呢，在搜索框输入你想听的音乐吧！");
                return
            }
            if (Player.music[Player.pos] == undefined || $(this).hasClass("selected")) return;
            _gaq.push(["_trackEvent", "Player", "Hate", Player.music[Player.pos].tid]), ++Player.hateCount, Player.loveCount = 0;
            var e = "";
            Player.isPsnRdInsertPlay ? e = "psn" : e = Search.keywords, clearTimeout(Gns.gnsRcmdTmo), Gns.gnsRcmdTmo = setInterval(Gns.rcmd, 6e5), $(this).addClass("selected"), $("#playerLove").removeClass("selected"), $.ajax({
                url: Core.API_VER + "/music/post_hate_song",
                data: {
                    uid: Signup.userDetail.id,
                    tid: Player.music[Player.pos].tid,
                    c: Player.hateCount,
                    cmbt: e
                },
                success: function (e) {
                    if (Flw.isFlw && Flw.toid != "") {
                        Core.getCookie("hateKnow") != "true" && Gns.nowGns('你在跟听状态，讨厌歌曲不会切换，但是已经记录了哦。<a href="#" class="trg knowEvent" data-cookieid="hateKnow">我知道了</a>'), Flw.send("hate," + Signup.userDetail.nick + "," + Signup.userDetail.fid);
                        return
                    }
                    e.result.items.length > 0 ? Player.musicUpdate(e.result.items) : --Player.hateCount, Player.next(), Player.savePlayingData()
                }
            })
        },
        psnRdClick: function () {
            if (Flw.isFlw && Flw.toid != "") {
                Gns.nowGns("你正在跟听中，暂时不能做其他操作哦。");
                return
            }
            if (Player.isPsnRdInsertPlay && Player.psnRdTmo == 0) {
                Core.fullScrenMenuHide(), $("#playerRptOne").removeClass("selected");
                var e = new Array;
                for (var t = 0; t < Gns.openGnsArr.length; ++t) Gns.openGnsArr[t][0] == "now" && (e[e.length] = Gns.openGnsArr[t]);
                Gns.openGnsArr = e, clearTimeout(Gns.closeGnsTmo), $("#playerPsnRd").removeClass("selected"), Player.isPsnRdInsertPlay = !1, Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.play(Player.cacheSec), Player.cacheMusic = null
            } else Player.isPsnRdInsertPlay || (About.isInsertPlay ? About.closeMoodPlay() : About.isAtInsertPlay ? About.closeAtPlay() : NtrlLngTop.isInsertPlay && NtrlLngTop.closeNtrlPlay(), $("#playerPsnRd").addClass("selected"), Player.isPsnRdInsertPlay = !0, Gns.nowGns("我们正在为你准备智能推荐电台。", Gns.level1), Player.cacheMusic == null && (Player.cacheMusic = Player.music, Player.cachePos = Player.pos, Player.cacheSt = Search.st, Player.cacheSec = Player.currentTime, Player.cacheTotal = Search.total), Player.psnRdTmo = setTimeout(function () {
                    Player.psnRdTmo = 0, Player.pos = 100, Search.st = 0, Player.music = new Array, About.isInsertPlay = !1, About.isTopInsertPlay = !1, About.isAtInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, Player.next()
                }, 2e3))
        },
        startRotate: function () {
            if (Signup.userDetail.sts.rtCv == "false" || $("#playCtl").hasClass("loading")) return;
            Player.cdRotate ? Player.swfObj.isRotate() || Player.swfObj.beginRotate() : $("#rotateCD").removeClass("stRt").addClass("rt")
        },
        stopRotate: function () {
            Player.cdRotate ? Player.swfObj.isRotate() && Player.swfObj.stopRotate() : $("#rotateCD").addClass("stRt")
        },
        setRotate: function () {
            if (Player.music[Player.pos] == undefined) return;
            var e = $.id2url(Player.music[Player.pos].fid, "AM", "album");
            Player.cdRotate ? Player.swfObj.showCover(e) : $("#rotateCD").removeClass("rt")
        },
        postNext: function () {
            if (Player.music[Player.pos] == undefined) return;
            var e, t;
            Player.actualCurrentTime > 10 ? e = "true" : e = "false", Player.actualCurrentTime > Player.music[Player.pos].duration / 2 ? t = "true" : t = "false", $.ajax({
                url: Core.API_VER + "/music/post_next",
                data: {
                    uid: Signup.userDetail.id,
                    tid: Player.music[Player.pos].tid,
                    next: e,
                    half: t
                }
            })
        },
        setPlayerDefault: function () {
            Player.pause(), Player.cdRotate ? Player.swfObj.showCover(IMG_URL + "/defaults/cover/300.jpg") : $("#rotateCD").css({
                background: 'url("' + IMG_URL + '/defaults/cover/300.jpg") no-repeat scroll 0 0 #E3E3E1'
            }), $("#mscPlr").children("p").text("Jing 已经为你准备好，搜索你想听的"), Player.music = new Array, Player.savePlayingData()
        },
        addLoading: function () {
            $("#playCtl").addClass("loading");
            var e = $("#mscPlr").data("mouse");
            $("#mscPlrMask").mouseenter(), $("#mscPlr").data("mouse", e);
            var t = {
                lines: 8,
                length: 4,
                width: 4,
                radius: 5,
                corners: 1,
                rotate: 0,
                color: "#fff",
                speed: .8,
                trail: 10,
                shadow: !0,
                hwaccel: !1,
                className: "abtSpin",
                zIndex: 2e9,
                top: "21px",
                left: "21px"
            };
            Player.spin = $("#playCtl").spin(t), Player.stopRotate()
        },
        resize: function () {
            var e = Core.bodyWidth - Core.MIN_BODY_WIDTH;
            e = parseInt(e / ((Core.MAX_BODY_WIDTH - Core.MIN_BODY_WIDTH) / 100)), e < 0 && (e = 0), (About.isOpen || NtrlLngTop.isOpen) && Core.bodyWidth < 830 && ($(".mscPlrBtnCtn>.hate").hide(), $(".mscPlrBtnCtn>.love").css("left", "-18%"), $(".mscPlrBtnCtn>.rptOne").hide(), $(".mscPlrBtnCtn>.next").css("right", "-18%")), Core.bodyWidth > 830 && ($(".mscPlrBtnCtn>.hate").show(), $(".mscPlrBtnCtn>.love").css("left", "-48%"), $(".mscPlrBtnCtn>.rptOne").show(), $(".mscPlrBtnCtn>.next").css("right", "-48%")), About.isAnimate ? ($("#vlmCtrl").animate({
                left: Core.bodyWidth / 2 + $("#mscPlrCtn").width() / 2 - 70 - 62 + "px",
                top: Core.bodyHeight / 2 + $("#mscPlrCtn").height() / 2 - 88 + "px"
            }, 300), $("#rMenu").animate({
                left: -(Core.bodyWidth / 2 - $("#mscPlrCtn").width() / 2 - 28) + "px",
                top: Core.bodyHeight / 2 + $("#mscPlrCtn").height() / 2 - 88 + "px"
            }, 300), $("#mscPlrCtn>.mscPlrBtnCtn").animate({
                width: 100 + e + "%",
                left: "-" + e / 2 + "%"
            }, {
                duration: 300,
                step: function () {
                    $("#mscPlrCtn>.mscPlrBtnCtn").css("overflow", "visible")
                }
            })) : ($("#vlmCtrl").css({
                left: Core.bodyWidth / 2 + $("#mscPlrCtn").width() / 2 - 70 - 62 + "px",
                top: Core.bodyHeight / 2 + $("#mscPlrCtn").height() / 2 - 88 + "px"
            }), $("#rMenu").css({
                left: -(Core.bodyWidth / 2 - $("#mscPlrCtn").width() / 2 - 28) + "px",
                top: Core.bodyHeight / 2 + $("#mscPlrCtn").height() / 2 - 88 + "px"
            }), $("#mscPlrCtn>.mscPlrBtnCtn").css({
                width: 100 + e + "%",
                left: "-" + e / 2 + "%"
            }))
        }
    }, Apps = {
        tickerCache: null,
        MAX_INVT: 3,
        isInvtAnimate: !1,
        mtFrdsData: null,
        timeOutId: 0,
        fid: "",
        init: function () {
            $(document).on("click", ".tkrsFlyEvent", function () {
                Search.fly($(this), $(this).data("fid"), $(this).data("span"), !1, !0)
            }), $(document).on("click", ".snsBindEvt", function () {
                Frd.olFrdObj = {
                    uid: "ext"
                }, $("#abtCtn").isDisplay() && About.ouid == Signup.userDetail.id ? $("#abtMenu>.frds").click() : $("#about").click(), $("#usrTpsCtn").hide()
            })
        },
        tpsMenu: function (e, t) {
            var n = e.data("uid"),
                r = e.offset().left - 97,
                i = e.offset().top - 56,
                s = '<a href="#" class="trg overlay snsBindEvt">绑定一个SNS才能分享&nbsp;&nbsp;绑定</a>',
                o = '<div style="bottom:-24px; left:0px; position:absolute; width:200px; height:25px;"></div>',
                u = '<div id="usrTpsCtn" class="tps" style="position:fixed; z-index:9999; left:' + r + "px; top:" + i + 'px">' + '<div class="tpsAr"></div>' + '<div class="tpsLbd"></div>' + '<div class="tpsCtn">' + '<div class="inspt"></div>' + '<p class="ctt">' + s + "</p>" + "</div>" + '<div class="tpsRbd"></div>' + o + "</div>";
            e.append(u), $("#rmnd").click(function () {
                $.ajax({
                    url: Core.API_VER + "/account/remind_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: n
                    }
                }), Gns.nowGns("提醒已发送"), $("#usrTpsCtn").remove()
            })
        },
        friendMenu: function (e, t, n) {
            if (t.children(".selected").length == 1) return;
            var r = "",
                i = 0,
                s = new Array,
                o = resData.frdCt;
            for (var u = 0; u < o.length; ++u) ConverSns[o[u].mid] != undefined && o[u].display == "true" && (s[s.length] = ConverSns[o[u].mid + "1"]);
            for (var u = 0; u < s.length; ++u) {
                var a = "";
                u == s.length - 1 && (a = "last"), r += '<a class="usrTps ' + ConverSns[s[u]] + " " + a + '" data-identify="' + ConverSns[s[u]] + '" href="#"></a>', ++i
            }
            i == 0 && Apps.tpsMenu(t, "shr");
            var f = 233 - (3 - i) * 77;
            if (i == 0) return;
            var l = f / 2,
                c = $("#shrIcon").offset().top - 53,
                h = $("#shrIcon").offset().left - f / 2 + 6,
                p = '<div style="bottom:-24px; left:0px; position:absolute; width:' + f + 'px; height:25px;"></div>';
            r = '<div id="usrTpsCtn" class="usrTpsCtn overlay" style="position:fixed; z-index:9999; left:' + h + "px; top:" + c + 'px;">' + '<div class="usrTpsAr rvs" style="left:' + l + 'px"></div>' + p + '<div class="usrTpsLbd"></div>' + r + '<div class="usrTpsRbd"></div>' + "</div>", t.append(r)
        },
        refreshFriends: function () {
            Core.setCookie("snsUpdateTime", (new Date).getTime());
            var e = $(this).data("mid");
            $.ajax({
                url: Core.API_VER + "/oauth/fetch_updates",
                data: {
                    uid: Signup.userDetail.id,
                    identify: ConverSns[e + "1"]
                },
                success: function (t) {
                    t.code == "802" ? Gns.nowGns("你的" + btnDes[ConverSns[e + "1"]] + '绑定已过期，<a data-identify="' + e + '" href="#" class="trg gnsSnsBind">请重新绑定</a>') : t.code == "804" ? Gns.nowGns("访问" + btnDes[ConverSns[e + "1"]] + "的时候超时，请重试") : t.success && Gns.nowGns("正在更新你的好友关系，稍候重新查看。")
                }
            })
        },
        hateBtnClick: function () {
            $(this).addClass("selected"), $.ajax({
                url: Core.API_VER + "/music/post_hate_song",
                data: {
                    uid: Signup.userDetail.id,
                    tid: $(this).parent().prop("id")
                }
            });
            var e = $("#appsCttCtn").children(".true").length - 1;
            $(this).parent().animate({
                height: "0px"
            }, 300, function () {
                $(this).remove(), $("#appsCttCtn").children().length == 0 && ($("#menuMore").length == 0 ? $("#appsCtn").animate({
                    width: "390px",
                    height: "258px"
                }, 300, function () {
                    $("#appsCttCtn").html('<div class="noCtt"><div class="icon box"></div><p class="ctn">这里现在是空的</p></div>')
                }) : $("#menuMore").click())
            })
        },
        openSns: function (e) {
            var t = snsWindowWh[e][0],
                n = snsWindowWh[e][1],
                r = (Core.screenWidth - t) / 2,
                i = (Core.screenHeight - n - 68) / 2,
                s = resSns[e],
                o = Core.API_VER + "/oauth/proxyauthorize?action=BIND&identify=" + s + "&uid=" + Signup.userDetail.id + "&standalone=" + Core.standalone;
            window.open(o, "_blank", "width=" + t + "px, height=" + n + "px, left=" + r + "px, top=" + (Core.screenHeight - n - 68) / 2 + "px, directories=0, location=0, menubar=0, resizable=0, status=0, toolbar=0")
        },
        offsetDate: function (e, t) {
            var n = new Date,
                r = parseInt((n.getTime() - parseInt(e)) / 1e3);
            if (r / 60 < 1) return r < 30 ? "刚才" : r + "秒前";
            if (r / 3600 < 1) return parseInt(r / 60) + "分钟前";
            if (r / 86400 < 1) return parseInt(r / 3600) + "小时前";
            if (r / 2592e3 < 1) {
                var i = parseInt(r / 86400);
                return t == "snsUpdate" ? i >= 7 ? !0 : !1 : t == "top" ? i <= 2 ? !0 : !1 : i + "天前"
            }
            return r / 31104e3 < 1 ? t == "top" ? !1 : parseInt(r / 2592e3) + "个月前" : t == "top" ? !1 : "1年前"
        }
    }, Gns = {
        openGnsArr: new Array,
        closeGnsTmo: 0,
        isOver: !1,
        level1: "level1",
        gnsRcmdTmo: 0,
        isInsertPlay: !1,
        isAnimate: !1,
        modelArr: new Array("mscPlrCtn", "mdlHr", "instSchCtn", "topApp", "guide", "appGuide", "rlsNote", "smlGuide", "psnRdGuide", "shrGuide"),
        timeDot: null,
        init: function () {
            $(document).on("click", ".gnsSchSml", function () {
                Search.tid = $(this).data("tid"), Search.searchBtnClick()
            }), $(document).on("click", ".gnsTingOS", function () {
                Search.searchAfter($(this).data("keywords"), Search.response, 0, !0, !1)
            }), $(document).on("click", ".gnsMt", function () {
                Search.mt = $(this).data("mt"), Search.searchBtnClick()
            }), $(document).on("click", ".gnsSnsBind", function () {
                Apps.openSns(ConverSns[$(this).data("identify")])
            }), $(document).on("click", ".gnsFlyEvent", function () {
                Search.fly($(this), "CmbtFlyBadge", $(this).text(), !1, !1)
            }), $(document).on("click", ".gnsRdFlyEvent", function () {
                Search.fly($(this), "CmbtFlyBadge", $(this).text(), !1, !0)
            }), $(document).on("click", ".gnsAttrTextFly", function () {
                Search.fly($(this), "CmbtFlyBadge", $(this).data("text"), !1, !0)
            }), $(document).on("click", "#gnsSnsUpdate", function () {
                Core.setCookie("snsUpdateTime", (new Date).getTime()), Frd.olFrdObj = {
                    uid: "ext"
                }, $("#abtCtn").isDisplay() && About.ouid == Signup.userDetail.id ? $("#abtMenu>.frds").click() : $("#about").click()
            }), $(document).on("click", ".closeFullScreenEvt", function () {
                $(".cls").each(function () {
                    if ($(this).parent().isDisplay()) return $(this).click(), Gns.isOver = !1, Gns.closeGns(), !1
                })
            }), $(".cls").click(function () {
                $(".closeFullScreenEvt").length > 0 && Gns.closeGns()
            }), $(document).on("click", ".uappEvt", function () {
                $("#frdCt").hasClass("selected") ? Frd.jump(Frd.FRDS) : (Frd.tab = Frd.FRDS, $("#frdCt").click())
            }), $(document).on("click", ".gnsFlwd", function () {
                var e = $(this).data("nick");
                $.ajax({
                    url: Core.API_VER + "/account/follow_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: $(this).data("frdid")
                    },
                    success: function (t) {
                        if (!t.success) {
                            Gns.nowGns(t.codemsg);
                            return
                        }
                        Gns.nowGns("你已成功关注了 " + e)
                    }
                })
            }), $(document).on("click", "#gnsCtt a", function () {
                if ($(this).hasClass("know") > -1) return;
                Core.isFullScren && ($("#guide").isDisplay() ? $("#guideFinish").click() : $("#topApp").isDisplay() ? $("#topApp .cls").click() : $("#appGuide").isDisplay() && $("#appGuide .cls").click())
            }), $(document).on("click", ".gnsKnownEvent", function () {
                var e = $(this).data("n"),
                    t = $(this).parent().children(".gnsFlyEvent").text();
                $.ajax({
                    url: Core.API_VER + "/music/post_genius_rcmd",
                    data: {
                        uid: Signup.userDetail.id,
                        rd: t,
                        n: e
                    }
                }), $(this).remove()
            }), $("#gnsCtn").mouseenter(function () {
                Gns.isOver = !0
            }).mouseleave(function () {
                Gns.isOver = !1
            }), $("#gnsBtn").click(function () {
                return $.ajax({
                    url: Core.API_VER + "/app/fetch_genius",
                    data: {
                        uid: Signup.userDetail.id
                    },
                    success: function (e) {
                        e.success || (e.result = Signup.userDetail.nick + "，是你在呼叫我么？我在这～");
                        if (Flw.isServer) {
                            var t = "",
                                n = Core.objLength(Flw.users);
                            if (n > 1) t = '你有 <em class="serif big" id="showFlwUsers" style="cursor:pointer; border-bottom:2px dashed #888; padding-bottom:4px;">' + n + '</em> 个好友在跟听<span class="spl"></span>';
                            else {
                                var r = "",
                                    i = "";
                                for (i in Flw.users) r = Flw.users[i];
                                t = r + ' 在跟听你 <a href="#" id="stopFlw" class="trg overlay">关闭跟听</a><span class="spl"></span>'
                            }
                            e.result = t + e.result, Gns.nowGns(e.result + ' 如果你不知道听什么，点击 <a class="trg gnsAttrTextFly" data-text="随便听听" href="#">随便听听</a>')
                        } else Search.fly($("#gnsBtn"), "CmbtFlyBadge", "随便听听", !1, !0)
                    }
                }), !1
            })
        },
        mscInfoHtml: function (e) {
            if (Player.music[Player.pos] == undefined || e.result.tid != Player.music[Player.pos].tid) return;
            Gns.mscInfo(e), Gns.timeDotHtml(e);
            if (Signup.userDetail.sts.frdlvd == "true") {
                var t = Player.music[Player.pos].tid;
                $.ajax({
                    url: Core.API_VER + "/music/fetch_frdlvd",
                    data: {
                        tid: t,
                        uid: Signup.userDetail.id
                    },
                    success: function (e) {
                        if (e.result.items.length == 0 || t != Player.music[Player.pos].tid) return;
                        Gns.friendLove(e)
                    }
                })
            }
            $(".tmDtsCtn").remove(), e.result.lvd == "l" ? $("#playerLove").addClass("selected") : e.result.lvd == "h" && $("#playerHate").addClass("selected")
        },
        friendLove: function (e) {
            var t = "",
                n = e.result.items;
            for (var r = 0; r < n.length; ++r) {
                var i = n[r].nick,
                    s = Search.keywords.toLowerCase().indexOf(i.toLowerCase());
                if (s >= 0 && Search.keywords.substring(s - 1, s) == "@") continue;
                var s = $("#prevHtml").text().toLowerCase().indexOf(i.toLowerCase());
                if (s >= 0) continue;
                t += '<a data-uid="' + n[r].uid + '" data-avatar="' + n[r].avatar + '" data-nick="' + i + '" data-frdshp="' + n[r].frdshp + '" data-ol="' + n[r].ol + '" href="#" class="trg offChtNickEvent">' + i + "</a>、"
            }
            if (t == "") return;
            t = t.substring(0, t.length - 1) + " 喜欢过这首歌曲~", Gns.openGnsArr[Gns.openGnsArr.length] = new Array("love", t), Gns.showGns()
        },
        friendListen: function (e) {
            var t = "";
            for (var n = 0; n < e.items.length; ++n) {
                var r = e.items[n].avatar;
                t += '<a data-uid="' + e.items[n].uid + '" data-nick="' + e.items[n].nick + '" href="#" class="trg prflEvent">' + e.items[n].nick + "</a>、"
            }
            if (t == "") return;
            t = t.substring(0, t.length - 1) + " ", t += "正在听这首歌曲~", Gns.openGnsArr[Gns.openGnsArr.length] = new Array("listen", t), Gns.showGns()
        },
        ablumEventClick: function () {
            Player.cacheSec = Player.currentTime;
            var e = $.id2url($(this).data("fid"), "AT", "album"),
                t = $(this).data("mid"),
                n = $(this).data("tid");
            Player.insertDuration = $(this).data("d");
            var r = $(this).offset().top - $("#mscPlrCtn").offset().top,
                i = $(this).offset().left - $("#mscPlrCtn").offset().left;
            $("#mscPlrCtn").append('<a href="#" id="flyAblum" class="ufo rt hide" style="left:' + i + "px; top:" + r + "px; background-image:url(" + e + ');" ></a>'), $("#flyAblum").show().animate({
                left: "335px",
                top: "315px"
            }, 500, function () {
                Player.isPlay && Player.playCtl("pause"), Gns.isInsertPlay = !0, $("#flyAblum").css({
                    "z-index": "1004"
                });
                var e = function () {
                    Player.isVolumeDown ? setTimeout(e, 200) : (Player.setPlay(0, t, n), Player.setVolumeUp())
                };
                e()
            }), $("#flyAblum").click(function () {
                $("#playCtl").click()
            })
        },
        mscInfo: function (e) {
            var t = e.result,
                n = "";
            for (var r in t) {
                if (Core.isEmpty(t[r]) || r == "ply_info" || r == "lvd" || r == "tid") continue;
                var i = "";
                switch (r) {
                case "feat":
                    var s = t[r].atst,
                        o = t[r].ftw.split(","),
                        u = "",
                        a = 0;
                    s == "Various Artists" && (s = o[0], a = 1);
                    for (; a < o.length; ++a) u += '<a class="gnsFlyEvent trg" href="#">' + o[a] + "</a>、";
                    if (u == "") continue;
                    u = u.substring(0, u.length - 1);
                    var f = '<a class="gnsFlyEvent trg" href="#">' + s + "</a> 和 " + u + " 共同演艺这首歌噢！";
                    Gns.openGnsArr[Gns.openGnsArr.length] = new Array("feat", f);
                    break;
                case "vsn":
                    for (var a = 0; a < t[r].length; ++a) {
                        if (t[r][a] == "原版" || t[r][a] == "纯音乐") continue;
                        n += '<a href="#" class="gnsFlyEvent trg">' + t[r][a] + "</a>、"
                    }
                    if (n == "") continue;
                    n = n.substring(0, n.length - 1);
                    var f = "你正在收听 " + n + " 的 " + Player.music[Player.pos].name;
                    Gns.openGnsArr[Gns.openGnsArr.length] = new Array("vsn", f);
                    break;
                case "orgn":
                    for (var a = 0; a < t[r].length; ++a) {
                        var l = "";
                        if (t[r][a].tid == Player.music[Player.pos].tid) continue;
                        !Core.isEmpty(t[r][a].mid) && (!Flw.isFlw || Flw.toid == "") && (l = '，<a class="ablumEvent trg" data-fid="' + t[r][a].fid + '" data-mid="' + t[r][a].mid + '" data-tid="' + t[r][a].tid + '" data-du="' + t[r][a].d + '" href="#">要听听</a> ？');
                        var f;
                        t["cmps_info"].singer == t[r][a].atst ? f = '<a class="gnsFlyEvent trg" href="#">' + t[r][a].atst + "</a> 还唱过另外一个版本的'" + Player.music[Player.pos].name + "'" + l : f = '<a class="gnsFlyEvent trg" href="#">' + t[r][a].atst + "</a> 也演艺过这首歌曲哦" + l, Gns.openGnsArr[Gns.openGnsArr.length] = new Array("orgn", f)
                    }
                    break;
                case "mov_info":
                    var c = "",
                        h = "",
                        p = 'target="_blank"',
                        d = {};
                    for (var v in t[r]) {
                        if (Core.isEmpty(t[r][v]) || v == "movie_url") continue;
                        v == "jingle" ? c = "广告歌曲、" : v == "in_song" ? c = "插曲、" : v == "ending_song" ? c = "片尾曲、" : v == "opening_song" ? c = "片头曲、" : v == "theme_song" && (c = "主题曲、"), d[t[r][v]] == undefined && (d[t[r][v]] = ""), d[t[r][v]] += c
                    }
                    var f = "";
                    for (var m in d) {
                        var g = m.split(","),
                            y = "";
                        for (var a = 0; a < g.length; ++a) y += "&lt;" + g[a] + "&gt;、";
                        f += '<a href="#" class="trg"> ' + y.substring(0, y.length - 1) + " </a> 的" + d[m]
                    }
                    f != "" && (f = f.substring(0, f.length - 1) + "哦！", Gns.openGnsArr[Gns.openGnsArr.length] = new Array("mov", f));
                    break;
                case "cmps_info":
                    var b = "",
                        w = new Object;
                    t[r].occp == undefined && (t[r].occp = "");
                    var E = t[r].occp,
                        S = t[r].singer;
                    for (var v in t[r]) {
                        if (Core.isEmpty(t[r][v]) || v == "occp" || v == "singer") continue;
                        var x = "";
                        v == "composers" ? x = "作曲" : v == "songwriters" ? x = "作词" : v == "arrangers" ? x = "编曲" : v == "producers" && (x = "制作人");
                        var T = t[r][v].split(",");
                        for (var a = 0; a < T.length; ++a) Search.keywords.indexOf(T[a]) >= 0 && (w[T[a]] == undefined ? w[T[a]] = x + "、" : w[T[a]] += x + "、")
                    }
                    var N = !0;
                    for (var r in w) if (S.indexOf(r) >= 0) {
                            N = !1;
                            break
                        }
                    for (var r in w) {
                        var f = '这首歌是 <a class="gnsFlyEvent trg" href="#" >' + r + "</a> ，" + w[r].substring(0, w[r].length - 1);
                        E.indexOf(r) >= 0 && N && (Gns.openGnsArr[Gns.openGnsArr.length] = new Array("cmps", f))
                    }
                }
            }
            Gns.openGnsArr.length != 0 && Gns.showGns()
        },
        nowGns: function (e, t) {
            if (e == $("#gnsCtt .ctt").text()) return;
            var n = (new Date).getTime() - parseInt($("#gnsCtn").data("time"));
            if (1500 > n && t != Gns.level1) {
                setTimeout(function () {
                    Gns.nowGns(e, t)
                }, 2e3 - n);
                return
            }
            $("#gnsCtn").data({
                time: (new Date).getTime()
            }), e == undefined && (e = "Jing 开了点小差，你过会儿再试试。");
            var r = "now";
            t == Gns.level1 && (r = t), $("#gnsCtn").height() == 74 ? (clearTimeout(Gns.closeGnsTmo), Gns.openGnsArr = (new Array(new Array(r, e))).concat(Gns.openGnsArr), Gns.closeGns()) : (Gns.openGnsArr[0] = new Array(r, e), Gns.showGns())
        },
        showGns: function () {
            if (Gns.openGnsArr.length == 0) return;
            if ($("#gnsCtn").isDisplay() && $("#gnsCtn").height() == 74) return;
            if ($("#gnsCtn").isDisplay() && $("#gnsCtn").height() < 74) {
                setTimeout(Gns.showGns, 1e3);
                return
            }
            var e = "";
            for (var t = 0; t < Gns.modelArr.length; ++t) $("#" + Gns.modelArr[t]).isDisplay() ? $("#" + Gns.modelArr[t]).animate({
                    "margin-top": "-=74"
                }, 300) : e += "#" + Gns.modelArr[t] + ",";
            $(e.substring(0, e.length - 1)).css({
                "margin-top": "-=74"
            }), $("#shrIcon").children("#usrTpsCtn").remove(), $("#atSgstCtn").hide(), Gns.isAnimate = !0, $("#gnsCtn").show().animate({
                height: "74px"
            }, 300, function () {
                Gns.isAnimate = !1, Gns.nextGns()
            })
        },
        nextGns: function () {
            clearTimeout(Gns.closeGnsTmo);
            if (Gns.openGnsArr.length == 0) {
                Gns.closeGns();
                return
            }
            if ($("#gnsCtn").data("tag") == Gns.level1) return;
            $("#gnsCtn").data({
                tag: Gns.openGnsArr[0][0],
                time: (new Date).getTime()
            }), $("#gnsCtt").css({
                left: "0px"
            }).html('<p class="ctt">' + Gns.openGnsArr[0][1] + "</p>"), $("#gnsCtt").css({
                width: "auto"
            });
            var e = $("#gnsCtt").width(),
                t = $("#gnsCtt").height(),
                n = $("#gnsCttCt").width(),
                r = Gns.openGnsArr[0];
            $("#gnsCtt").css({
                left: "-" + $("#gnsCtt").width() + "px",
                "margin-top": -t / 2 + "px"
            }).animate({
                left: (n - e) / 2 + "px"
            }, 500, function () {
                $(this).css({
                    width: e + 5 + "px"
                })
            }), Gns.openGnsArr[0][0] != Gns.level1 && (Gns.closeGnsTmo = setTimeout(Gns.closeGns, 8e3)), Gns.openGnsArr = Gns.openGnsArr.slice(1, Gns.openGnsArr.length)
        },
        closeGns: function () {
            if ($("#gnsCtn").data("tag") == Gns.level1 || $("#gnsCtn").height() != 74 || Gns.isAnimate) return;
            var e = $("#gnsCttCt").width();
            if (Gns.openGnsArr.length != 0) $("#gnsCtt").animate({
                    left: e + "px"
                }, 500, function () {
                    $(this).css({
                        width: "auto"
                    }), Gns.nextGns()
                });
            else {
                if (Gns.isOver) {
                    Gns.closeGnsTmo = setTimeout(Gns.closeGns, 3e3);
                    return
                }
                $("#gnsCtn").data("tag", ""), $("#gnsCtt").html("").css({
                    width: "auto"
                });
                var t = "";
                for (var n = 0; n < Gns.modelArr.length; ++n) $("#" + Gns.modelArr[n]).isDisplay() ? $("#" + Gns.modelArr[n]).animate({
                        "margin-top": "+=74"
                    }, 300) : t += "#" + Gns.modelArr[n] + ",";
                $(t.substring(0, t.length - 1)).css({
                    "margin-top": "+=74"
                }), $("#shrIcon").children("#usrTpsCtn").remove(), $("#atSgstCtn").hide(), Gns.isAnimate = !0, $("#gnsCtn").animate({
                    height: "0px"
                }, 300, function () {
                    $(this).hide(), Gns.isAnimate = !1
                })
            }
        },
        insertStop: function (e) {
            Gns.isInsertPlay = !1;
            if (e == "continue") {
                Player.setPlay(Player.cacheSec, Player.music[Player.pos].mid, Player.music[Player.pos].tid);
                var t = function () {
                    Player.isVolumeDown ? setTimeout(t, 200) : Player.playCtl()
                };
                t()
            } else e == "next" && Player.next();
            $("#flyAblum").remove()
        },
        converTime: function (e) {
            var t = parseInt(e / 60),
                n = parseInt(e % 60),
                r = "";
            return t > 0 ? (t < 10 && (t = "0" + t), n < 10 && (n = "0" + n), r = "在" + t + "分" + n + "秒出现") : r = "在" + n + "秒出现", r
        },
        timeDotHtml: function (response) {
            if (!response.success) return;
            Gns.timeDot = undefined;
            if (response.result.ply_info != null && response.result.ply_info != "") {
                var isShowGns = !1,
                    info = eval("(" + response.result.ply_info + ")"),
                    key, childKey;
                for (key in info.timeDot) for (var i = 0; i < info.timeDot[key].length; ++i) {
                        childKey = "";
                        var infoStr = "",
                            pHtml = "",
                            dotSeconds = 0;
                        if (typeof info.timeDot[key][i] == "string") {
                            dotSeconds = parseInt(info.timeDot[key][i]), infoStr = key + "从这里开始";
                            var listenHtml = ' <a href="#" id="timeDot' + dotSeconds + '" class="timeDotEvent trg">去听听</a>';
                            Flw.isFlw && Flw.toid != "" && (listenHtml = ""), pHtml = '<a href="#" class="gnsFlyEvent trg" >' + key + "</a> " + Gns.converTime(dotSeconds) + listenHtml
                        } else if (typeof info.timeDot[key][i] == "object") for (childKey in info.timeDot[key][i]) {
                                dotSeconds = parseInt(info.timeDot[key][i][childKey]), infoStr = key + childKey + "从这里开始";
                                var listenHtml = ' <a href="#" id="timeDot' + dotSeconds + '" class="timeDotEvent trg">去听听</a>';
                                Flw.isFlw && Flw.toid != "" && (listenHtml = ""), childKey == "" && (childKey = key), pHtml = '<a href="#" class="gnsFlyEvent trg" >' + childKey + "</a> " + Gns.converTime(dotSeconds) + listenHtml
                        }
                        if (dotSeconds != 0 && !isNaN(dotSeconds)) {
                            isShowGns = !0;
                            var timeDotObj = new Object;
                            timeDotObj.action = "timedot", timeDotObj.classStyle = "play";
                            if (childKey != "" && (Search.keywords.indexOf(key) != -1 || Search.keywords.indexOf(childKey) != -1) || childKey == "" && Search.keywords.indexOf(key) != -1) Gns.openGnsArr[Gns.openGnsArr.length] = new Array(timeDotObj, pHtml);
                            if (Gns.timeDot == null || Gns.timeDot == undefined) Gns.timeDot = new Object;
                            Gns.timeDot["timeDot" + (dotSeconds - 2)] = new Array(timeDotObj, pHtml)
                        }
                }
                isShowGns && Gns.showGns()
            }
        },
        rcmd: function () {
            if (Signup.userDetail.sts.rcmd != "true" || !$("#mainMenuCtn").isDisplay()) return;
            $.ajax({
                url: Core.API_VER + "/music/fetch_genius_rcmd",
                data: {
                    uid: Signup.userDetail.id,
                    cmbt: Search.keywords
                },
                success: function (e) {
                    if (!e.success || e.result.rd == null) return;
                    var t = e.result.rd,
                        n = e.result.n;
                    pHtml = Signup.userDetail.nick + '，试试 <a class="gnsRdFlyEvent trg" href="#">' + t + "</a> 的组合", Gns.openGnsArr[Gns.openGnsArr.length] = new Array("rcmd", pHtml), Gns.showGns()
                }
            })
        },
        resize: function () {
            $("#gnsCttCt").css({
                width: Core.bodyWidth - 112 + "px"
            })
        }
    }, Top = {
        unitWH: 0,
        fontsize: 18,
        count: 0,
        direction: "",
        ANI_TIME: 50,
        TIME_ARR: new Array(2, 10, 20, 30, 40),
        isInsertPlay: !1,
        obj: null,
        colCount: 0,
        rowCount: 0,
        firstPlay: !0,
        isExec: !1,
        init: function () {
            $(document).on("click", "#clsTop", function () {
                Top.firstPlay = !0, $(".knowEvent").data("cookieid") == "topKnow" && $(".knowEvent").click(), Top.obj = null, clearTimeout(Top.showTpsTmo), Core.fullScrenMenuHide();
                var e = function () {
                    $(this).hide(), Core.isFullScren = !1, $("#topApp .slide").stop().html(""), $(document).clearQueue("topQueue"), $("#tbCtn").removeClass("fsappMd"), Core.playerCDShow(), $("#topApp").css({
                        "z-index": "1004"
                    });
                    if (Top.isInsertPlay || Player.isPsnRdInsertPlay || About.isInsertPlay || About.isTopInsertPlay || About.isAtInsertPlay || NtrlLngTop.isInsertPlay || About.isSingleInsertPlay) Player.setVolumeDown("pause", 400), $("#playCtl").hasClass("pause") && $("#playCtl").click(), Top.insertStop(), Player.isPsnRdInsertPlay = !1, About.isInsertPlay = !1, About.isTopInsertPlay = !1, About.isAtInsertPlay = !1, NtrlLngTop.isInsertPlay = !1, About.isSingleInsertPlay = !1;
                    $("#prgrsCtn").show()
                };
                Core.ie68 ? ($("#topApp").hide(), e()) : $("#topApp").animate({
                    opacity: "0"
                }, 300, e)
            })
        },
        insertStop: function () {
            Top.isInsertPlay = !1, Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.cacheMusic = null, Player.setPlay(Player.cacheSec, Player.music[Player.pos].mid, Player.music[Player.pos].tid);
            var e = function () {
                Player.isVolumeDown ? setTimeout(e, 200) : $("#playCtl").hasClass("play") && Player.playCtl()
            };
            e()
        },
        show: function () {
            About.isOpen && About.closeAbout(), Core.isFullScren = !0, Core.getCookie("topKnow") != "true" && Core.ipad && Gns.nowGns('用双指滑动封面进行翻页。<a href="#" data-cookieid="topKnow" class="trg knowEvent">我知道了</a>', Gns.level1), $("#prgrsCtn").hide(), $("#topApp .slide").css("left", "0px"), Top.resize("true"), Main.hide("apps"), Interface.Current == Interface.SEARCH && Search.hide(), Core.fullScrenMenuShow('你正在使用音乐瀑布 <span class="spl"></span><a href="#" id="clsTop" class="clsFlwLstn overlay">退出</a>', "topApp"), Core.playerCDHide();
            var e = function () {
                $("#tbCtn").addClass("fsappMd"), $(this).css({
                    height: "100%",
                    "z-index": "999"
                }), $.ajax({
                    url: Core.API_VER + "/app/fetch_top",
                    data: {
                        ps: "100"
                    },
                    success: function (e) {
                        var t = e.result.top,
                            n = "";
                        for (var r = 0; r < t.length; ++r) {
                            var i = "";
                            r >= Top.count && (i = "hide"), n += '<div class="topCvCtn ' + i + '">' + '<div class="topCv" style="width:' + (Top.unitWH - 3) + "px; height:" + (Top.unitWH - 3) + 'px">' + '<div class="blkMask"></div>' + '<img src="' + IMG_URL + '/defaults/cover/cv.jpg" width="' + (Top.unitWH - 3) + 'px" height="' + (Top.unitWH - 3) + 'px">' + "</div>" + '<div class="qkPlay" style="width:' + (Top.unitWH - 3) + 'px;">' + '<p class="ats" style="width:' + (Top.unitWH - 71) + 'px"><span class="dark">Artist</span>&nbsp;' + t[r].atst + "</p>" + '<h5 class="trckTit" style="width:' + (Top.unitWH - 55) + "px; font-size:" + Top.fontsize + 'px;">' + t[r].n + "</h5>" + '<a data-mid="' + t[r].mid + '" data-tid="' + t[r].tid + '" data-d="' + t[r].d + '" class="qkPlayBtn" href="#"></a>' + "</div>" + "</div>"
                        }
                        $("#topApp .slide").append(n);
                        var s = $.makeArray($("#topApp .slide").children()),
                            o = 0;
                        for (var r = 0; r < Top.rowCount; ++r) for (var o = 0; o < Top.colCount; ++o) {
                                var u = new Image;
                                u.obj = $(s[r + o * Top.rowCount]).children(".topCv").children("img"), u.onload = function () {
                                    this.obj.prop("src", this.src)
                                }, u.src = $.id2url(t[r + o * Top.rowCount].fid, "AM", "album")
                        }
                        Core.ipad || $("#topApp>.slide").mousemove(function (e) {
                            e = e || window.event;
                            var t, n;
                            if (e.pageX || e.pageY) t = e.pageX, n = e.pageY;
                            t = e.clientX + document.body.scrollLeft - document.body.clientLeft, n = e.clientY + document.body.scrollTop - document.body.clientTop;
                            if (t < Core.bodyWidth / 5) {
                                var r = Top.TIME_ARR[parseInt(t / (Core.bodyWidth / 5 / 5))];
                                if (Top.direction != "right" || Top.ANI_TIME != r) console.log("left"), Top.ANI_TIME = r, Top.slideRight();
                                return
                            }
                            if (t > Core.bodyWidth / 5 * 4) {
                                var r = Top.TIME_ARR[4 - parseInt((t - Core.bodyWidth / 5 * 4) / (Core.bodyWidth / 5 / 5))];
                                if (Top.direction != "left" || Top.ANI_TIME != r) console.log("right"), Top.ANI_TIME = r, Top.slideLeft();
                                return
                            }
                            t > Core.bodyWidth / 5 && t < Core.bodyWidth / 5 * 4 && (console.log("stop"), Top.slideStop())
                        }), $(".topCvCtn").mouseenter(function () {
                            if (Top.obj != null && $(this).children(".qkPlay").children(".qkPlayBtn").data("mid") == Top.obj.data("mid")) return;
                            $(this).children(".topCv").children(".blkMask").css({
                                opacity: "0"
                            }), $(this).children(".qkPlay").show()
                        }).mouseleave(function () {
                            if (Top.obj != null && $(this).children(".qkPlay").children(".qkPlayBtn").data("mid") == Top.obj.data("mid")) return;
                            $(this).children(".topCv").children(".blkMask").css({
                                opacity: "0.5"
                            }), $(this).children(".qkPlay").hide()
                        }), $(".qkPlayBtn").click(function () {
                            if ($(this).hasClass("selected")) {
                                Top.obj.removeClass("selected"), Player.setVolumeDown("pause", 400), Top.obj = null;
                                return
                            }
                            Top.firstPlay && (Top.firstPlay = !1, $.ajax({
                                url: Core.API_VER + "/ticker/post_cmbt",
                                data: {
                                    uid: Signup.userDetail.id,
                                    content: "音乐瀑布"
                                }
                            })), Top.isInsertPlay = !0, Player.isPlay && Player.playCtl(), Top.obj != null && (Top.obj.removeClass("selected"), Top.obj.parent(".qkPlay").hide().parent(".topCvCtn").children(".topCv").children(".blkMask").css({
                                opacity: "0.5"
                            })), $(this).addClass("selected"), $(this).parent(".qkPlay").show().parent(".topCvCtn").children(".topCv").children(".blkMask").css({
                                opacity: "0"
                            }), Top.obj = $(this);
                            var e = $(this).data("mid"),
                                t = $(this).data("tid");
                            Player.insertDuration = $(this).data("d");
                            var n = function () {
                                Player.isVolumeDown ? setTimeout(n, 200) : (Player.setPlay(0, e, t), Player.setVolumeUp(), setTimeout(function () {
                                    Player.isExec = !1
                                }, 500))
                            };
                            n()
                        })
                    }
                })
            };
            Core.ie68 ? ($("#topApp").removeClass("dspr"), $("#topApp").show(), e()) : $("#topApp").show().animate({
                opacity: "1"
            }, 300, e)
        },
        slideLeft: function () {
            Top.slideStop(), Top.direction = "left", $(document).queue("topQueue", function () {
                var e = $("#topApp .slide").width() - Core.bodyWidth + parseInt($("#topApp .slide").css("left"));
                $("#topApp .slide").animate({
                    left: "-=" + e + "px"
                }, e * Top.ANI_TIME, "linear", function () {
                    $(document).dequeue("topQueue")
                })
            }), $(document).dequeue("topQueue")
        },
        slideRight: function () {
            Top.slideStop(), Top.direction = "right", $(document).queue("topQueue", function () {
                var e = -parseInt($("#topApp .slide").css("left"));
                $("#topApp .slide").animate({
                    left: "0px"
                }, e * Top.ANI_TIME, "linear", function () {
                    $(document).dequeue("topQueue")
                })
            }), $(document).dequeue("topQueue")
        },
        slideStop: function () {
            $("#topApp .slide").stop(), $(document).clearQueue("topQueue"), Top.direction = ""
        },
        playOver: function () {
            Player.isExec = !0;
            var e = parseInt(Math.random() * Top.count),
                t = $(".topCvCtn:eq(" + e + ")");
            t.children(".qkPlay").children(".qkPlayBtn").click();
            while (e >= Top.rowCount) e -= Top.rowCount;
            var n = e * Top.unitWH - Core.bodyWidth / 2;
            n < 0 && (n = 0), n > $("#topApp .slide").width() - Core.bodyWidth && (n = $("#topApp .slide").width() - Core.bodyWidth), Top.slideStop(), $(document).queue("topQueue", function () {
                var e = -parseInt($("#topApp .slide").css("left"));
                n > e ? e = n - e : e -= n, Core.ipad ? $("#topApp .slide").css({
                    left: "-" + n + "px"
                }) : $("#topApp .slide").animate({
                    left: "-" + n + "px"
                }, e * Top.TIME_ARR[0], "linear")
            }), $(document).dequeue("topQueue")
        },
        resize: function (e) {
            var t = Core.bodyHeight - 75;
            e == "true" && $("#topApp").css({
                height: t + "px"
            }), t -= 3;
            var n = parseInt(t / 200);
            n > 5 && (n = 5), n < 3 && (n = 3), Top.unitWH = parseInt(t / n), Top.fontsize = parseInt((Top.unitWH - 200) / parseInt(100 / 6)) + 18;
            var r = parseInt(100 / n);
            Top.rowCount = r, Top.colCount = n, Top.count = r * n, $("#topApp .slide").css({
                width: r * Top.unitWH + 3 + "px"
            }), e == undefined && $("#topApp .slide").children(".topCvCtn").each(function (e) {
                e < Top.count ? ($(this).children(".topCv").css({
                    width: Top.unitWH - 3 + "px",
                    height: Top.unitWH - 3 + "px"
                }), $(this).children(".topCv").children("img").prop("width", Top.unitWH - 3 + "px"), $(this).children(".topCv").children("img").prop("height", Top.unitWH - 3 + "px"), $(this).children(".qkPlay").css("width", Top.unitWH - 3 + "px"), $(this).children(".qkPlay").children(".ats").css({
                    width: Top.unitWH - 71 + "px"
                }), $(this).children(".qkPlay").children(".trckTit").css({
                    width: Top.unitWH - 55 + "px",
                    "font-size": Top.fontsize + "px"
                }), $(this).show()) : $(this).hide()
            })
        }
    }, Cht = {
        TEXTAREA_HEIGHT: 65,
        MAX_CHT_HEIGHT: 0,
        fuid: "",
        fid: "",
        isScroll: !1,
        scrollApi: null,
        offlineMes: new Object,
        offlineCount: 0,
        sysOfflineCount: 0,
        isTyping: !1,
        isAddTyping: !1,
        PS: 5,
        isFocus: !1,
        cacheCtnH: 0,
        cacheCttCtnH: 0,
        cacheFrdListH: 0,
        no: 0,
        h: 0,
        init: function () {
            $(document).on("click", ".chtFlyEvent", function () {
                Search.fly($(this), "CmbtFlyBadge", $(this).text().replace(/#([^"]*)#/g, "$1"), !1, !0)
            }), $(document).on("click", ".chtResend", function () {
                var e = $(this).data("fuid"),
                    t = $(this).data("ctt");
                $(this).hide();
                var n = $(this),
                    r = function () {
                        n.data("fuid", ""), n.data("ctt", ""), now.postChat(Signup.userDetail.id, e, t), n.remove()
                    };
                Core.nowIsReady ? r() : (socketConnect(), setTimeout(function () {
                    Core.nowIsReady ? r() : n.show()
                }, 5e3))
            }), $(document).on("click", ".gnsSysEvent", function () {
                Frd.olFrdObj = {
                    avatar: "",
                    frdshp: "",
                    from: "",
                    id: "",
                    nick: "",
                    uid: 0,
                    ol: ""
                }, $("#abtCtn").isDisplay() && About.ouid == Signup.userDetail.id ? $("#abtMenu>.frds").click() : $("#about").click()
            }), $(document).on("click", ".offChtNickEvent", function () {
                var e = $(this).data("frdshp"),
                    t = $(this).data("ol");
                if (e == "false") {
                    Gns.nowGns("Ta还没有关注你，无法开启聊天。");
                    return
                }
                t == undefined && (t = "true"), Frd.olFrdObj = {
                    avatar: $(this).data("avatar"),
                    frdshp: !0,
                    from: "",
                    id: "",
                    nick: $(this).data("nick"),
                    uid: $(this).data("uid"),
                    ol: t
                }, $("#abtCtn").isDisplay() && About.ouid == Signup.userDetail.id ? $("#abtMenu>.frds").click() : $("#about").click()
            }), $(document).on("click", ".offChtListEvent", function () {
                Frd.olFrdObj = {
                    avatar: "",
                    frdshp: "",
                    from: "",
                    id: "",
                    nick: "",
                    uid: "",
                    ol: ""
                }, $("#abtCtn").isDisplay() && About.ouid == Signup.userDetail.id ? $("#abtMenu>.frds").click() : $("#about").click()
            })
        },
        dtlEventClick: function () {
            $cht = $(this).parents(".frdEvent").children(".tpsMenuCtn").children(".rndBtn.chtEvent");
            if ($(this).data("frdshp") == 0 || $cht.hasClass("loading")) return;
            var e = Cht.fuid;
            if (Cht.fuid !== "") {
                if (e == $(this).data("uid")) {
                    Cht.closeCht();
                    return
                }
                Cht.closeCht(!0)
            }
            Cht.no != 0 && parseInt(Cht.no) > parseInt($cht.data("no")) && (Cht.h = 0);
            var t = $(this).parents(".frdEvent").position().top - About.OFFSET_TOP - Cht.h;
            About.scrollApi.scrollToY(t, !0), Cht.no = 0, Cht.h = 0, $(this).parents(".frdEvent").children(".tpsMenuCtn").children(".num.cht").remove(), $(this).parents(".rctCtn").after('<div id="chtCtn" style="overlay:hidden;"></div>'), Cht.fuid = $(this).data("uid"), nick = $(this).data("nick"), Cht.offlineMes[Cht.fuid] != undefined && (Cht.offlineCount -= Cht.offlineMes[Cht.fuid].count, delete Cht.offlineMes[Cht.fuid]), $cht.addClass("loading"), $.ajax({
                url: Core.API_VER + "/chat/fetch_chatctt",
                data: {
                    uid: Signup.userDetail.id,
                    fuid: Cht.fuid,
                    st: "0",
                    ps: Cht.PS
                },
                success: function (e) {
                    Cht.dtlHtml(e), $cht.removeClass("loading")
                }
            })
        },
        closeCht: function (e) {
            e == 1 && (Cht.no = $("#chtCtn").prev().children(".chtEvent").data("no"), Cht.h = $("#chtCtn").height()), $("#chtCtn").prop("id", "oldChtCtn"), $("#chtInput").prop("id", "oldChtInput"), $("#chtMsg").prop("id", "oldChtMsg"), $("#emjBtn").prop("id", "oldEmjBtn"), $("#oldChtCtn").animate({
                height: "0px"
            }, 300, function () {
                $(this).remove(), About.scrollApi.reinitialise()
            }), Cht.fuid = "", Cht.fid = ""
        },
        sysH: function () {
            Cht.MAX_CHT_HEIGHT = Core.MAX_OVERLAY_HEIGHT - Frd.ITEM_H - Frd.TITLE_H - 20 - Frd.STTS_H;
            var e = parseInt(Cht.MAX_CHT_HEIGHT / 117);
            return e
        },
        dtlHtml: function (e) {
            Cht.fid = e.result.fid;
            var t = e.result.items;
            t.length == Cht.PS && $("#chtCtn").append(Frd.sttsHtml("查看聊天记录", "chtMoreEvent"));
            var n = "",
                r, i, s, o = new Array;
            o[0] = t.length - 1;
            for (var u = t.length - 2; u >= 0; --u) r = parseInt(t[u].ts.split(" ")[0].split("-")[2]), i = parseInt(t[u].ts.split(" ")[1].split(":")[0]), s = parseInt(t[u].ts.split(" ")[1].split(":")[1]), r > parseInt(t[o[o.length - 1]].ts.split(" ")[0].split("-")[2]) ? o[o.length] = u : i > parseInt(t[o[o.length - 1]].ts.split(" ")[1].split(":")[0]) ? o[o.length] = u : s - parseInt(t[u].ts.split(" ")[1].split(":")[1]) > 30 && (o[o.length] = u);
            for (var u = t.length - 1, a = 0; u >= 0; --u) Cht.fuid == 0 ? n += Cht.sysHtml(t[u]) : (u == o[a] && (n += Cht.gnTime(t[u].ts), ++a), t[u].sf ? n += Cht.rightDtl(t[u].ctt) : n += Cht.leftDtl(t[u].ctt, u));
            $("#cypherH").html(n);
            var f = $("#cypherH").height();
            $("#cypherH").html("");
            var l = "block;";
            f == 0 && (l = "none;"), n = '<div class="chts" style="height:' + f + "px; display:" + l + '">' + n + "</div>", $("#chtCtn").append(n);
            var c = Retina.suffix;
            $("#chtCtn").append('<div id="chtCttCtn" class="rctCtn chtCtn rght type"><div id="emjCtn" class="emjCtn" style="height:0px; overflow:hidden;"><a href="#" class="emj mns" data-code="tst" style="background-image:url(' + IMG_URL + "/emoji/1" + c + '.png)"></a>' + '<a href="#" class="emj" data-code="wzb" style="background-image:url(' + IMG_URL + "/emoji/2" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="dx" style="background-image:url(' + IMG_URL + "/emoji/3" + c + '.png)"></a>' + '<a href="#" class="emj" data-code="k" style="background-image:url(' + IMG_URL + "/emoji/4" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="bj" style="background-image:url(' + IMG_URL + "/emoji/5" + c + '.png)"></a>' + '<a href="#" class="emj" data-code="by" style="background-image:url(' + IMG_URL + "/emoji/6" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="lh" style="background-image:url(' + IMG_URL + "/emoji/7" + c + '.png)"></a>' + '<a href="#" class="emj nbd" data-code="ks" style="background-image:url(' + IMG_URL + "/emoji/8" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="wq" style="background-image:url(' + IMG_URL + "/emoji/9" + c + '.png)"></a>' + '<a href="#" class="emj" data-code="jy" style="background-image:url(' + IMG_URL + "/emoji/10" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="n" style="background-image:url(' + IMG_URL + "/emoji/11" + c + '.png)"></a>' + '<a href="#" class="emj" data-code="bb" style="background-image:url(' + IMG_URL + "/emoji/12" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="pz" style="background-image:url(' + IMG_URL + "/emoji/13" + c + '.png)"></a>' + '<a href="#" class="emj" data-code="l" style="background-image:url(' + IMG_URL + "/emoji/14" + c + '.png)"></a>' + '<a href="#" class="emj mns" data-code="dk" style="background-image:url(' + IMG_URL + "/emoji/15" + c + '.png)"></a>' + '<a href="#" class="emj nbd" data-code="yw" style="background-image:url(' + IMG_URL + "/emoji/16" + c + '.png)"></a>' + "</div>" + '<div id="chtMsg" class="chtMsg">' + '<p class="chat">' + '<textarea id="chtInput" placeholder="你想和TA说点什么？" class="chtInput" ></textarea>' + '<a id="emjBtn" class="emjBtn" style="display:none" href="#"></a>' + "</p>" + '<div class="chtTail"></div>' + "</div>" + "</div>");
            var h = $("#chtCtn").height();
            $("#chtCtn").css("height", "0px"), $("#chtCtn").animate({
                height: h + "px"
            }, 300, function () {
                $(this).css("height", "auto"), About.scrollApi.reinitialise()
            }), Core.ie68 && $("#emjBtn").css("opacity", "1"), Frd.replaceAvatar(), $("#chtInput").focus(function () {
                if (Cht.emj || $(this).height() == 32) return;
                Cht.isFocus = !0, $("#chtCttCtn").animate({
                    height: "+=16"
                }, 300), $(this).animate({
                    height: "32px"
                }, 300, function () {
                    $("#emjBtn").show()
                })
            }).blur(function () {
                if (Cht.emj | $(this).height() == 16) return;
                var e = 16,
                    t = function () {
                        $("#emjBtn").hide(), $("#emjCtn").height() == 88 ? ($("#emjCtn").animate({
                            height: "0px"
                        }, 300), $("#chtCttCtn").animate({
                            height: "-=104"
                        }, 300)) : $("#chtCttCtn").animate({
                            height: "-=16"
                        }, 300), $("#chtInput").animate({
                            height: "16px"
                        }, 300, function () {
                            Cht.isFocus = !1
                        }), Cht.isTyping = !1, now.typing(Signup.userDetail.id, Cht.fuid, Cht.isTyping)
                    };
                $("#emjCtn").height() == 88 ? (e += 88, setTimeout(t, 50)) : t()
            }).keydown(function (t) {
                if (t.keyCode == 13) {
                    var n = Core.inputConver($.trim($(this).val()));
                    return n.length > 240 ? (Gns.nowGns("发送消息内容超长，请分条发送。"), !1) : ($(this).val(""), n == "" ? !1 : (Cht.isTyping = !1, Core.nowIsReady && now.postChat(Signup.userDetail.id, Cht.fuid, n), Cht.sendMsg(e, n), !1))
                }
                Cht.isTyping || (Cht.isTyping = !0, now.typing(Signup.userDetail.id, Cht.fuid, Cht.isTyping))
            }), $("#chtMsg").click(function () {
                Cht.isFocus || $("#chtInput").focus()
            }), $("#emjBtn").mouseover(function () {
                Cht.emj = !0
            }).mouseout(function () {
                Cht.emj = !1
            }).click(function () {
                $("#emjCtn").height() == 88 ? $("#emjCtn, #chtCttCtn").animate({
                    height: "-=88"
                }, 300, function () {
                    About.scrollApi.reinitialise()
                }) : $("#emjCtn, #chtCttCtn").animate({
                    height: "+=88"
                }, 300, function () {
                    About.scrollApi.reinitialise()
                }), $("#chtInput").focus()
            }), $(".emj").mouseover(function () {
                Cht.emj = !0
            }).mouseout(function () {
                Cht.emj = !1
            }).click(function () {
                $("#emjBtn").click();
                var e = "[" + $(this).data("code") + "]";
                $("#chtInput").val($("#chtInput").val() + e)
            })
        },
        more: function () {
            var e = $("#chtCtn>.chts>.chtCtn").length;
            $.ajax({
                url: Core.API_VER + "/chat/fetch_chatctt",
                data: {
                    uid: Signup.userDetail.id,
                    fuid: Cht.fuid,
                    st: e,
                    ps: Cht.PS
                },
                success: function (e) {
                    var t = e.result.items,
                        n = "";
                    (t.length == 0 || t.length != Cht.PS) && $("#chtCtn>.stts").animate({
                        height: "0px"
                    }, 300, function () {
                        $(this).remove()
                    });
                    if (t.length == 0) return;
                    var r, i, s, o = new Array;
                    o[0] = t.length - 1;
                    for (var u = t.length - 2; u >= 0; --u) r = parseInt(t[u].ts.split(" ")[0].split("-")[2]), i = parseInt(t[u].ts.split(" ")[1].split(":")[0]), s = parseInt(t[u].ts.split(" ")[1].split(":")[1]), r > parseInt(t[o[o.length - 1]].ts.split(" ")[0].split("-")[2]) ? o[o.length] = u : i > parseInt(t[o[o.length - 1]].ts.split(" ")[1].split(":")[0]) ? o[o.length] = u : s - parseInt(t[u].ts.split(" ")[1].split(":")[1]) > 30 && (o[o.length] = u);
                    for (var u = t.length - 1, a = 0; u >= 0; --u) Cht.fuid == 0 ? n += Cht.sysHtml(t[u]) : (u == o[a] && (n += Cht.gnTime(t[u].ts), ++a), t[u].sf ? n += Cht.rightDtl(t[u].ctt) : n += Cht.leftDtl(t[u].ctt));
                    $("#cypherH").html(n);
                    var f = $("#cypherH").height() + $("#chtScroll").height();
                    $("#cypherH").html(""), $("#chtCtn").children(".chts").prepend(n), $("#chtCtn").children(".chts").animate({
                        height: "+=" + f
                    }, 300, function () {
                        About.scrollApi.reinitialise()
                    })
                }
            })
        },
        sendMsg: function (e, t) {
            var n = "";
            n += Cht.rightDtl(t, !0), Cht.appendCht(n, "send"), $("#chtResend").data("ctt", Cht.resendCtt), $("#chtResend").attr("id", "")
        },
        receiveMessage: function (e) {
            e.fuid = Number(e.fuid);
            if (Cht.fuid == e.fuid) {
                var t = "";
                t += Cht.leftDtl(e.ctt), Cht.appendCht(t, "receive")
            } else {
                Player.setVolumeDown("msgTone", 100), Cht.offlineMes[e.fuid] == undefined && (Cht.offlineMes[e.fuid] = new Object, Cht.offlineMes[e.fuid].count = 0), Cht.offlineMes[e.fuid].nick = e.nick, Cht.offlineMes[e.fuid].count += 1, Cht.offlineCount += 1, e.ctt.length > 10 && (e.ctt = e.ctt.substring(0, 10) + "...");
                var n = e.nick + ' 对你说："' + e.ctt + '"，<a href="#" class="trg offChtNickEvent" data-uid="' + e.fuid + '" data-nick="' + e.nick + '" data-avatar="' + e.fid + '">去看看？</a>';
                Gns.nowGns(n), $(".tpsMenuCtn>.chtEvent").each(function () {
                    if ($(this).data("uid") == e.fuid) return $(this).parent().children("em").remove(), $(this).parent().children(".flwLstnEvent").isDisplay() ? $(this).after('<em class="num serif cht" style="right:103px">' + Cht.offlineMes[e.fuid].count + "</em>") : $(this).after('<em class="num serif cht">' + Cht.offlineMes[e.fuid].count + "</em>"), !1
                });
                var r = Cht.offlineCount;
                Signup.userDetail.sts.frdCntNtf == "true" && (r += Signup.onlineCount), About.ouid == Signup.userDetail.id && About.mid != "rct" && $("#frdsChtCount").text(r).show()
            }
        },
        typingMessage: function (e, t) {
            var n = '<div id="typing" class="rctCtn chtCtn lft typng" style="float:left; height:auto"><div class="chtMsg"><p class="chat"></p><div class="chtTail"></div></div></div>';
            if (t == "true" && $("#typing").length == 0) $("#typing").length == 0 && !Cht.isAddTyping && (Cht.isAddTyping = !0, setTimeout(function () {
                    Cht.isAddTyping = !1
                }, 500), Cht.appendCht(n, "typing"));
            else {
                var r = $("#typing").height();
                $("#typing").remove(), $("#chtCtn>.chts").animate({
                    height: "-=" + r
                }, 300)
            }
        },
        appendCht: function (e, t) {
            var n = 0;
            t == "receive" && $("#typing").length != 0 && (n = $("#typing").height(), $("#typing").remove()), $("#cypherH").html(e);
            var r = $("#cypherH").height() - n;
            $("#cypherH").html(""), $("#chtCtn>.chts").append(e).show();
            var i = $("#chtCttCtn").offset().top + r + 79 - Core.bodyHeight;
            i > 0 && About.scrollApi.scrollToY(About.scrollY + i, !0), $("#chtCtn>.chts").animate({
                height: "+=" + r
            }, 300, "linear", function () {
                About.scrollApi.reinitialise()
            })
        },
        setScroll: function () {
            Cht.isScroll = !0, $("#chtScroll").jScrollPane(), $("#chtScroll").mouseenter(function () {
                $(".jspVerticalBar").animate({
                    opacity: "1"
                }, {
                    speed: 300,
                    queue: !1
                })
            }).mouseleave(function () {
                $(".jspVerticalBar").animate({
                    opacity: "0"
                }, {
                    speed: 300,
                    queue: !1
                })
            }), Cht.scrollApi = $("#chtScroll").data("jsp"), Cht.scrollApi.scrollToY(999999999999)
        },
        sysHtml: function (e) {
            var t = "",
                n = IMG_URL + "/defaults/avatar/" + Core.A50 + ".jpg",
                r = "",
                i = "",
                s = "",
                o = "",
                u = "",
                a = "",
                f = "";
            if (e.t == "inhs") i = '<a href="#" class="nick">' + e.frds[0] + "</a> 入驻了Jing.fm", s = e.frds[0], e.frdshps[0] && (a = '<a href="#" class="rndBtn cht offChtNickEvent" data-uid="' + e.frd_ids[0] + '" data-nick="' + e.frds[0] + '" data-avatar="' + e.avatars[0] + '" data-tps="聊天"></a>'), u = '<a class="rndBtn blkFrd sysBlkEvent" data-uid="' + e.frd_ids[0] + '" href="#" data-tps="扔掉"></a>', r = e.avatars[0];
            else if (e.t == "flwd") {
                e.flw_id == Signup.userDetail.id ? (s = e.flwer, f = e.flwer_id, i = '<a href="#" class="nick">' + e.flwer + '</a> 关注了 <a href="#" class="nick">你</a>') : (s = e.flw, f = e.flw_id, i = '<a href="#" class="nick">' + e.flwer + '</a> 关注了 <a href="#" class="nick">' + e.flw + "</a>"), r = e.flw_avatar;
                var l = "flwFrd flwFrdEvent",
                    c = "关注";
                e.me_flw ? e.frdshp ? (a = '<a href="#" class="rndBtn cht offChtNickEvent" data-uid="' + f + '" data-nick="' + s + '" data-avatar="' + e.flw_avatar + '" data-tps="聊天"></a>', u = '<a class="rndBtn blkFrd sysBlkEvent" data-uid="' + f + '" href="#" data-tps="扔掉"></a>') : o = '<a href="#" class="rndBtn cnct on" data-uid="' + f + '" data-nick="' + s + '" data-tps="已关注"></a>' : o = '<a href="#" class="rndBtn flwFrd flwFrdEvent" data-uid="' + f + '" data-nick="' + s + '" data-tps="关注"></a>'
            } else if (e.t == "rmnd") s = e.frd, f = e.frd_id, r = e.avatar, i = '<a href="#" class="nick">' + s + "</a> 想让你关注Ta", e.frdshp ? (a = '<a href="#" class="rndBtn cht offChtNickEvent" data-uid="' + f + '" data-nick="' + s + '" data-avatar="' + r + '" data-tps="聊天"></a>', u = '<a class="rndBtn blkFrd sysBlkEvent" data-uid="' + f + '" href="#" data-tps="扔掉"></a>') : o = '<a href="#" class="rndBtn flwFrd flwFrdEvent" data-uid="' + f + '" data-nick="' + s + '" data-tps="关注"></a>';
            else if (e.t == "acty") return t += '<div class="rctCtn sysMsgCtn" style="height:auto; border-top:medium none"><div class="sysMsgHd"><p class="chat">活动信息<a href="#" class="tmstmp">' + e.ts + "</a></p>" + "</div>" + '<div class="sysMsgBd">' + '<p class="chat">' + e.content + "</p>" + "</div>" + "</div>", t;
            return t += '<div class="rctCtn sysMsgCtn" style="height:auto; border-top:medium none"><div class="sysMsgHd"><p class="chat">' + i + '</p><a href="#" class="tmstmp">' + e.ts + "</a>" + "</div>" + '<div class="sysMsgBd frdEvent" href="#">' + '<div class="avtCtn">' + '<a class="chtMask prflEvent" href="#" data-uid="' + f + '" data-nick="' + s + '"></a>' + '<img src="' + n + '" class="chtAvt" data-fid="' + r + '" width="50px" height="50px">' + "</div>" + '<a class="desc inhs" href="#">' + s + "</a>" + '<div class="tpsMenuCtn">' + o + a + u + "</div>" + "</div>" + "</div>", t
        },
        leftDtl: function (ctt, i) {
            i == undefined && (i = ""), ctt = ctt.replace(/#([^"]*)#/g, ' <a href="#" class="chtCmbt chtFlyEvent">#$1#</a> '), ctt = ctt.replace(/＃([^"]*)＃/g, ' <a href="#" class="chtCmbt chtFlyEvent">#$1#</a> ');
            var suffix = Retina.suffix;
            for (var k in Emj) {
                var reg = eval("/\\[" + k + "\\]/g");
                ctt = ctt.replace(reg, '<img width="28px" height="28px" src="' + IMG_URL + "/emoji/" + Emj[k] + suffix + '.png"/>')
            }
            var html = "",
                sysClass = "";
            return Cht.fuid == 0 && (sysClass = "sysMsg"), html += '<div class="rctCtn chtCtn lft ' + sysClass + '" style="z-index:1; float:left;  height:auto">' + '<div class="chtMsg">' + '<p class="chat">' + ctt + "</p>" + '<div class="chtTail"></div>' + "</div>" + "</div>", html
        },
        rightDtl: function (ctt, isSending) {
            ctt = ctt.replace(/#([^"]*)#/g, ' <a href="#" class="chtCmbt chtFlyEvent"><span>#</span>$1<span>#</span></a> '), ctt = ctt.replace(/＃([^"]*)＃/g, ' <a href="#" class="chtCmbt chtFlyEvent"><span>#</span>$1<span>#</span></a> ');
            var suffix = Retina.suffix;
            for (var k in Emj) {
                var reg = eval("/\\[" + k + "\\]/g");
                ctt = ctt.replace(reg, '<img width="28px" height="28px" src="' + IMG_URL + "/emoji/" + Emj[k] + suffix + '.png"/>')
            }
            var html = "",
                chtResend = "";
            return isSending != undefined && !Core.nowIsReady ? (Cht.resendCtt = ctt, chtResend = '<a id="chtResend" class="chtRsnd chtResend" href="#" data-fuid="' + Cht.fuid + '"></a>') : Cht.resendCtt = "", html += '<div class="rctCtn chtCtn rght" style="float:left;  height:auto"><div class="chtMsg">' + chtResend + '<p class="chat">' + ctt + "</p>" + '<div class="chtTail"></div>' + "</div>" + "</div>", html
        },
        gnTime: function (e) {
            return '<div class="chtSpltCtn"><div class="chtSplt" href="#"></div><em class="serif spltBtn">' + e + "</em>" + "</div>"
        }
    }, Flw = {
        nick: "",
        toid: "",
        music: "",
        isFlw: !1,
        isServer: !1,
        cacheMusic: null,
        cacheSec: 0,
        users: null,
        serverTmo: 0,
        ACTION: 0,
        TID: 1,
        MID: 2,
        FID: 3,
        NAME: 4,
        DUR: 5,
        SEC: 6,
        VOLUME: 7,
        init: function () {
            $(document).on("click", "#flwOK", function () {
                Flw.flwOK($(this).data("fuid"))
            }), $(document).on("click", "#flwNO", function () {
                $("#gnsCtn").data("tag", ""), clearTimeout(Flw.reqTmo), now.followListenResponseAuthorize(!1, $(this).data("fuid"), Signup.userDetail.id, null), Gns.isOver = !1, Gns.closeGns()
            }), $(document).on("click", "#flwOther", function () {
                Flw.isFlw = !0, Flw.toid = $(this).data("fid"), Flw.nick = $(this).data("fnick"), Flw.listenRequest()
            }), $(document).on("click", "#showFlwUsers", Flw.showFlwUser), $(document).on("click", "#stopFlw", function () {
                Flw.leave(), Gns.nowGns("你已经踢开了正在跟听你的人哦，跟听结束。"), Flw.empty()
            }), $(document).on("click", "#clsFlwLstn", function () {
                Flw.leave(), Core.fullScrenMenuHide(), Flw.empty(), Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.cacheMusic = null;
                var e = function () {
                    Player.isVolumeDown ? setTimeout(e, 200) : Player.play(Player.cacheSec)
                };
                e(), Gns.openGnsArr = new Array, Gns.nowGns("已经退出跟听状态")
            })
        },
        flwOK: function (e) {
            $("#gnsCtn").data("tag", ""), clearTimeout(Flw.reqTmo), Flw.isServer = !0, Flw.isFlw = !0, now.followListenResponseAuthorize(!0, e, Signup.userDetail.id, Flw.gnMusicStr(Player.currentTime, Player.isPlay))
        },
        showFlwUser: function () {
            var e = "";
            for (var t in Flw.users) e += Flw.users[t] + "、";
            e = e.substring(0, e.length - 1) + ' 跟听了你。<a href="#" id="stopFlw" class="trg">退出跟听</a>', Gns.nowGns(e)
        },
        gnMusicStr: function (e, t) {
            var n = function (n) {
                var r = "";
                return t == undefined || t == 1 ? r = "play," : r = "pausing,", r + n.tid + "," + n.mid + "," + n.fid + "," + n.name + "," + n.duration + "," + e + "," + Player.volume + "," + n.an + "," + n.atn
            }, r = Player.music[Player.pos];
            return r != undefined && r.tid == Player.tid ? n(r) : ($.ajax({
                url: Core.API_VER + "/media/song/meta",
                data: {
                    id: Player.tid
                },
                success: function (e) {
                    r = new Object, r.tid = e.result.tid, r.mid = e.result.mid, r.fid = e.result.fid, r.name = e.result.n, r.duration = e.result.d, now.sendFollowListenMessage(Signup.userDetail.id, n(r))
                }
            }), null)
        },
        listenRequest: function () {
            now.followListenRequest(Signup.userDetail.id, Flw.toid)
        },
        send: function (e, t) {
            var n = "";
            e == "play" ? n = Flw.gnMusicStr(t) : n = e, n != null && now.sendFollowListenMessage(Signup.userDetail.id, n)
        },
        leave: function () {
            now.notifyFollowListenLeave(Signup.userDetail.id)
        },
        play: function (e, t) {
            Player.closeGns();
            var n = e.split(",");
            Player.music = new Array, Player.pos = 0, Player.music[Player.pos] = new Object, Player.music[Player.pos].tid = n[Flw.TID], Player.music[Player.pos].fid = n[Flw.FID], Player.music[Player.pos].mid = n[Flw.MID], Player.music[Player.pos].name = n[Flw.NAME], Player.music[Player.pos].duration = parseInt(n[Flw.DUR]), Player.setVolume(n[Flw.VOLUME]), Player.play(parseInt(n[Flw.SEC])), Player.startRotate(), t && (setTimeout(function () {
                Player.pause()
            }, 1e3), setTimeout(function () {
                Gns.nowGns("你跟听的人，暂停播放了哦")
            }, 3e3))
        },
        empty: function () {
            Flw.isServer = !1, Flw.isFlw = !1, Flw.toid = "", Flw.nick = "", Flw.users = null, clearTimeout(Flw.serverTmo)
        }
    }, Frd = {
        OLFRD: "olFrd",
        FRDS: "frds",
        EXT: "ext",
        h: 0,
        cacheIconH: 0,
        ITEM_H: 73,
        STTS_H: 46,
        TITLE_H: 81,
        WIDTH: 330,
        current: "olFrd",
        mid: "",
        st: 0,
        ps: 0,
        tab: "",
        olFrdObj: "",
        moods: new Array,
        init: function () {
            $("#frdCtNav .tabBtn").click(function () {
                $("#" + Frd.current).animate("width", ""), Frd.jump($(this).prop("id"), !1)
            }), $(document).on("click", ".badge", function () {
                var e = parseInt($(this).prop("id")),
                    t = parseInt($("#badges").children().first().data("moodid")),
                    n = parseInt($("#badges").children(".crt").prop("id")),
                    r = parseInt($("#badges").children().last().data("moodid")),
                    i = 0;
                if (e < n) {
                    var i = n - e,
                        s = "";
                    for (var o = 0; o < i; ++o) {
                        var u = t - o - 1;
                        u < 0 && (u = Frd.moods.length + u), s = '<a href="#" data-moodid="' + u + '" class="badge">' + Frd.moods[u].n + "</a>" + s
                    }
                    $("#badges").prepend(s).css("left", "-=" + i * 78)
                } else {
                    if (!(e > n)) return;
                    var i = e - n,
                        s = "";
                    for (var o = 0; o < i; ++o) {
                        var u = r + o + 1;
                        u >= Frd.moods.length && (u -= Frd.moods.length), s += '<a href="#" data-moodid="' + u + '" class="badge">' + Frd.moods[u].n + "</a>"
                    }
                    $("#badges").append(s)
                }
                $("#badges").animate({
                    left: "+=" + (n - e) * 78
                }, 300, function () {
                    $("#" + n).removeClass("crt"), $("#" + e).addClass("crt");
                    if (e < n) $("#badges").children().each(function (e) {
                            e > 6 ? $(this).remove() : $(this).prop("id", e)
                        });
                    else {
                        var t = e - n;
                        $("#badges").children().each(function (e) {
                            e < t ? $(this).remove() : $(this).prop("id", e - t)
                        }), $("#badges").css("left", "+=" + t * 78)
                    }
                })
            }), $(document).on("mouseenter", ".tpsMenuCtn>.rndBtn, .plyrBtns", function () {
                var e = $(this).data("tps");
                if (Signup.userDetail.sts.tipNtf != "true" || e == "") return;
                var t = $(this).prop("id");
                e == undefined && ($(this).hasClass("ext off") ? e = "展开" : e = "收起"), $("#tps>.tpsCtn>p").text(e).show(), $("#tps").css({
                    left: "0px"
                });
                var n = $(this).offset().left - $("#tps").width() / 2 + 15;
                $(this).hasClass("plyrBtns") && (n -= 3);
                var r = $(this).offset().top - 50,
                    i = n + $("#tps").width() - (Core.bodyWidth + 420);
                i > 0 && (i += 5, n -= i, $("#tps>.tpsAr").css("margin-left", "+=" + i)), t == "ntrlLngBtn" && ($("#tps").addClass("rvs"), n += 19, r += 95, $("#tps>.tpsAr").css("margin-left", "-21px")), $("#tps").css({
                    left: n,
                    top: r + "px"
                }).show()
            }), $(document).on("mouseleave", ".tpsMenuCtn>.rndBtn, .plyrBtns", function () {
                $("#tps").hide().removeClass("rvs"), $("#tps>.tpsAr").css("margin-left", "-7px")
            }), $(document).on("click", ".tpsMenuCtn>a", function () {
                $("#tps").hide()
            }), $(document).on("click", ".iconEvent>.frdIcon, .iconEvent>.tpsMenuCtn>.ext, .iconEvent>.desc", function () {
                var e = $(this).data("mid");
                if (resUrl[e] == undefined) return;
                $(this).parents(".iconEvent").find(".newApp").hide(), $ext = $(this).parents(".iconEvent").children(".tpsMenuCtn").children(".ext");
                var t = Frd.mid;
                Frd.mid != "" && Frd.closeData(Frd.mid);
                if (t == e) return;
                Frd.mid = e, $ext.removeClass("off").addClass("on"), Frd.st = 0, Frd.ps = Cht.PS;
                if ($ext.hasClass("loading")) return;
                $ext.addClass("loading");
                if (Frd.mid == "mtFrds" || Frd.mid == "invt") Frd.ps = "5";
                $.ajax({
                    url: resUrl[Frd.mid].replace(/{id}/g, Signup.userDetail.id),
                    data: {
                        st: "0",
                        ps: Frd.ps
                    },
                    success: function (e) {
                        $ext.removeClass("loading");
                        if (!e.success) return;
                        var t = e.result.items,
                            n = "";
                        if (Frd.mid == "invt") {
                            Frd.h = Frd.getHeight(!0, t.length);
                            var r = "";
                            Signup.userDetail.c != 0 && (r = '<em id="invtCount" class="num serif" style="right:70px">' + Signup.userDetail.c + "</em>"), n += '<div class="rctCtn invtAppCtn">' + r + '<a id="invtBtn" href="#" class="invtBtn"><span class="invtBtn txt"></span></a>' + "</div>";
                            for (var i = 0; i < t.length; ++i) n += Frd.invtAppGnHtml(t[i]);
                            n += Frd.sttsHtml('共 <span class="serif">' + e.result.total + "</span> 个" + FrdDes[Frd.mid], "frdMoreEvent")
                        } else {
                            Frd.h = Frd.getHeight(!0, t.length), n = Frd.frdHtml(t);
                            if (t.length == 0) n = About.noCttHtml();
                            else {
                                var s;
                                e.result.total > Frd.ps ? s = "frdMoreEvent" : s = "", n += Frd.sttsHtml('共 <span class="serif">' + e.result.total + "</span> 位" + FrdDes[Frd.mid], s)
                            }
                        }
                        Frd.st = t.length, Frd.openData(Frd.mid, n)
                    }
                })
            }), $(document).on("click", ".chtMoreEvent", Cht.more), $(document).on("click", ".frdMoreEvent", Frd.frdMore), $(document).on("click", ".flwLstnEvent", function () {
                if ($(this).data("uid") == Signup.userDetail.id) {
                    Search.fly($(this), "CmbtFlyBadge", "@" + Signup.userDetail.nick, !1, !0);
                    return
                }
                if (Flw.isFlw) {
                    Flw.toid == "" ? Gns.nowGns('你正在被人跟听中，暂时不能跟听别人。<a href="#" id="showFlwUsers" class="trg">查看跟听我的人</a>') : Gns.nowGns("你正在跟听中，暂时不能做其他操作哦。");
                    return
                }
                Player.isPsnRdInsertPlay ? Player.psnRdClick() : $("#flyAblum").length == 1 && $("#flyAblum").click(), Flw.isFlw = !0, Flw.toid = $(this).data("uid"), Flw.nick = $(this).data("nick"), Flw.listenRequest()
            }), $(document).on("click", ".blkEvent, .hdEvent", function () {
                Frd.deleteItem($(this)), $.ajax({
                    url: Core.API_VER + "/account/block_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: $(this).data("uid")
                    }
                })
            }), $(document).on("click", ".sysBlkEvent", function () {
                $(this).data("tps") == "扔掉" ? ($(this).data("tps", "恢复"), $(this).prev().hide(), $.ajax({
                    url: Core.API_VER + "/account/block_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: $(this).data("uid")
                    }
                })) : ($(this).data("tps", "扔掉"), $(this).prev().show(), $.ajax({
                    url: Core.API_VER + "/account/block_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: $(this).data("uid")
                    }
                }))
            }), $(document).on("click", ".chtEvent", Cht.dtlEventClick), $(document).on("click", ".prflEvent", Frd.prflClick), $(document).on("click", ".cnctEvent", function () {
                var e = $(this).data("mid");
                if ($(this).hasClass("on")) {
                    if (!confirm("确定解除绑定吗？")) return;
                    $(this).removeClass("on").addClass("off"), $.ajax({
                        url: Core.API_VER + "/oauth/remove_bind",
                        data: {
                            uid: Signup.userDetail.id,
                            identify: resSns[e]
                        },
                        success: function (e) {
                            if (!e.success) {
                                Gns.nowGns(e.msg);
                                return
                            }
                        }
                    })
                } else resEmail[e] != undefined ? window.open(Core.API_VER + "/oauth/mailauthorize?uid=" + Signup.userDetail.id + "&type=" + e, "_blank", "directories=0, location=0, menubar=0, resizable=0, status=0, toolbar=0") : Apps.openSns(e);
                return !1
            }), $(document).on("click", ".invtEvent", function () {
                Gns.nowGns("正在发送邀请..."), $.ajax({
                    url: Core.API_VER + "/oauth/invite_friend",
                    data: {
                        uid: Signup.userDetail.id,
                        suid: $(this).data("uid"),
                        nick: $(this).data("nick"),
                        identify: ConverSns[Frd.mid + "1"]
                    },
                    success: function (e) {
                        if (!e.success) {
                            e.code == "802" ? Gns.nowGns("你的" + btnDes[ConverSns[Frd.mid + "1"]] + '绑定已过期，<a data-identify="' + Frd.mid + '" href="#" class="trg gnsSnsBind">请重新绑定</a>') : e.code == "804" ? Gns.nowGns("访问" + btnDes[ConverSns[Frd.mid + "1"]] + "的时候超时，请重试") : Gns.nowGns(e.msg);
                            return
                        }
                        setTimeout(function () {
                            Gns.nowGns("已经邀请成功")
                        }, 2e3)
                    }
                })
            }), $(document).on("click", ".hdFrdEvent", function () {
                var e = $(this);
                Frd.deleteItem(e)
            }), $(document).on("click", ".flwFrdEvent", function () {
                var e = $(this);
                $.ajax({
                    url: Core.API_VER + "/account/follow_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: $(this).data("uid")
                    },
                    success: function (t) {
                        if (!t.success) {
                            Gns.nowGns(t.codemsg);
                            return
                        }
                        e.parents(".cvCtn").length == 1 ? e.hide() : (e.removeClass("flwFrd").addClass("cnct on"), e.next(".notKnwnEvent").animate({
                            width: "0px"
                        }, 300)), Gns.nowGns("你已成功关注了 " + e.data("nick"))
                    }
                })
            }), $(document).on("click", ".notKnwnEvent", function () {
                var e = $(this);
                $.ajax({
                    url: Core.API_VER + "/account/post_notknown_friends",
                    data: {
                        uid: Signup.userDetail.id,
                        fid: $(this).data("uid")
                    },
                    success: function (t) {
                        if (!t.success) {
                            Gns.nowGns(t.codemsg);
                            return
                        }
                        Frd.deleteItem(e)
                    }
                })
            }), $(document).on("keydown", "#mtfrdsNick", function (e) {
                if (e.keyCode == 13) return $("#schNick").click(), !1
            }), $(document).on("click", "#schNick", function () {
                var e = $("#mtfrdsNick").val();
                if (e == "") return;
                var t = !1;
                Frd.mid != "" && (Frd.mid == "sch" ? t = !0 : Frd.closeData()), Frd.mid = "sch", $.ajax({
                    url: Core.API_VER + "/search/fetch_nick",
                    data: {
                        u: Signup.userDetail.id,
                        q: e,
                        st: "0",
                        ps: "10"
                    },
                    success: function (e) {
                        var n = e.result.items;
                        if (!e.success || n.length == 0) {
                            Gns.nowGns("暂时没有找到哦，是不是打错昵称了？再试试");
                            return
                        }
                        Frd.h = n.length * Frd.ITEM_H, Frd.openData("sch", Frd.frdHtml(n), t)
                    }
                })
            }), $(document).on("click", ".rmndEvent", function () {
                $.ajax({
                    url: Core.API_VER + "/account/remind_frd",
                    data: {
                        uid: Signup.userDetail.id,
                        frdid: $(this).data("uid")
                    }
                }), Gns.nowGns("提醒已发送")
            }), $(document).on("click", ".updtEvent", Apps.refreshFriends), $(document).on("click", ".copyEvent", function () {
                var e = $(this).prev().val();
                Core.ie ? window.clipboardData.setData("Text", e) : $(this).prev().clippy({
                    text: e
                })
            }), $(document).on("click", "#invtBtn", function () {
                if (Frd.isInvtAnimate) return;
                var e = $(this);
                $.ajax({
                    url: Core.API_VER + "/app/generate_invitations",
                    data: {
                        uid: Signup.userDetail.id
                    },
                    success: function (t) {
                        if (!t.success) {
                            Gns.nowGns(t.codemsg);
                            return
                        }
                        $("#frdList>.stts>.desc>.serif").text(parseInt($("#frdList>.stts>.desc>.serif").text()) + 1), Frd.isInvtAnimate = !0, e.after('<img id="invtPlusTip" src="' + IMG_URL + "/plusOne" + Retina.suffix + '.png" style="left:' + (e.width() / 2 - 7) + "px; top:" + (e.height() / 2 - 15) + 'px; position:absolute; z-index:9999"/>'), Core.ie68 ? $("#invtPlusTip").animate({
                            top: "-=50"
                        }, 1e3, function () {
                            $(this).remove(), Frd.isInvtAnimate = !1
                        }) : $("#invtPlusTip").animate({
                            top: "-=50",
                            opacity: "0"
                        }, 1e3, function () {
                            $(this).remove(), Frd.isInvtAnimate = !1
                        }), t.result.c == 0 ? $("#invtCount").hide() : $("#invtCount").text(t.result.c), Signup.userDetail.c = t.result.c;
                        if ($("#frdList").children(".fm").length < 5) {
                            var n = t.result.items;
                            $("#frdList>.stts").before(Frd.invtAppGnHtml(n[0])), Core.ie || ($(".copyEvent").last().clippy(), $(".copyEvent").last().children().css({
                                position: "absolute",
                                left: "20px",
                                top: "6px"
                            }))
                        }
                    }
                })
            })
        },
        invtAppGnHtml: function (e) {
            var t = "",
                n = e;
            Core.ie && (n = "");
            var r = '<a href="#" class="clpyBtn copyEvent">' + n + "</a>",
                i = "readonly";
            return Core.ipad && (r = "", i = ""), swfobject.ua.pv[0] || (r = ""), t += '<div class="rctCtn fm"><div class="fld link"><input name="link" value="' + e + '" autocorrect="off" class="input invtLink" ' + i + ">" + r + "</div>" + "</div>", t
        },
        deleteItem: function (e) {
            e.parents(".rctCtn").animate({
                height: "0px"
            }, 300, function () {
                if ($(this).parent().children(".stts").length == 1) {
                    var e = $(this).parent().children(".stts").children(".desc").children(".serif"),
                        t = parseInt(e.text()) - 1;
                    e.text(t)
                }
                $(this).remove(), About.scrollApi.reinitialise()
            })
        },
        jump: function (TAB) {
            TAB == "" && (TAB = Frd.OLFRD), Frd.tab = "", Frd.mid = "", Cht.fuid = "", Cht.fid = "", $("#appsCttCtn").hide().css("overflow", "hidden"), Frd.current != "" && $("#" + Frd.current).removeClass("crt"), $("#" + TAB).addClass("crt"), Frd.current = TAB;
            if (TAB == Frd.FRDS) $("#frds").find(".newApp").hide();
            else {
                var mNew = Signup.userDetail.newview.m,
                    bNew = Signup.userDetail.newview.b;
                if (bNew.length > 0) $("#frds").find(".newApp").show();
                else for (var i = 0; i < mNew.length; ++i) for (var j = 0; j < SnsArr.length; ++j) if (mNew[i].indexOf("-" + SnsArr[j]) >= 0) {
                                $("#frds").find(".newApp").show();
                                break
                            }
            }
            eval("Frd." + TAB + "Click")()
        },
        isMenuShow: function (e) {
            var t = resData.frdCt;
            for (var n = 0; n < t.length; ++n) if (t[n].mid == e) return t[n].display == "true" ? !0 : !1;
            return !1
        },
        iconHtml: function (e, t) {
            var n = "",
                r = "",
                i = "",
                s = t.mid;
            e == "ext" && s != "mtFrds" && s != "rbsFrd" && s != "invt" ? ConverSns[s] != undefined ? Frd.isMenuShow(ConverSns[s]) ? (ConverSns[s] != undefined && (s = ConverSns[s], n = '<a data-mid="' + s + '" href="#" class="rndBtn updt updtEvent" data-tps="更新"></a>', t.name = FrdDes[s]), r = '<a data-mid="' + s + '" href="#" class="rndBtn ext off"></a>') : i = '<a data-mid="' + s + '" href="#" class="rndBtn cnct cnctEvent off" data-tps="绑定"></a>' : i = '<a data-mid="' + s + '" href="#" class="rndBtn cnct cnctEvent off" data-tps="连接"></a>' : r = '<a data-mid="' + s + '" href="#" class="rndBtn ext off"></a>';
            var o = Signup.userDetail.newview.m,
                u = !0,
                a = "";
            for (var f = 0; f < o.length; ++f) if (o[f].indexOf("-" + t.mid) >= 0) {
                    u = !1, Core.ulNewIcon("m", t.mid);
                    break
                }
            var l = '<div data-mid="' + s + '" class="rctCtn iconEvent">' + '<a data-mid="' + s + '" class="frdIcon ' + s + '" href="#"></a>' + '<a data-mid="' + s + '" class="desc" href="#">' + t.name + "</a>" + '<div class="tpsMenuCtn">' + n + r + i + "</div>" + "</div>";
            return l
        },
        frdHtml: function (e) {
            var t = "",
                n = "";
            for (var r = 0; r < e.length; ++r) {
                var i = "",
                    s = "",
                    o = "",
                    u = "",
                    a = "",
                    f = "",
                    l = "",
                    c = "",
                    h = "",
                    p = "",
                    d = "",
                    v = r,
                    m = e[r].uid,
                    g = e[r].nick,
                    y = e[r].frdshp,
                    b = "";
                e[r].avatar == null && (e[r].avatar = ""), e[r].ol != undefined && Frd.mid != "mtFrds" && (++v, e[r].ol ? i = '<a href="#" class="rndBtn ' + Device[e[r].device][0] + '" data-tps="' + Device[e[r].device][1] + '"></a>' : i = '<a href="#" class="rndBtn ofl" data-tps="离线"></a>');
                var w = "";
                ConverSns[Frd.mid] == undefined && (w = "prflEvent"), Frd.mid == "rbsFrd" && (a = '<a class="rndBtn blkFrd hdEvent" data-uid="' + m + '" href="#" data-tps="恢复"></a>');
                if (ConverSns[Frd.mid] != undefined) {
                    var E = m;
                    e[r].inhs ? E = e[r].auid : c = '<a href="#" class="rndBtn invt invtEvent" data-uid="' + E + '" data-nick="' + g + '" data-tps="邀请"></a>', h = '<a href="#" class="rndBtn hdFrd hdFrdEvent" data-uid="' + E + '" data-nick="' + g + '" data-tps="隐藏"></a>'
                }
                if (Frd.mid == "mtFrds" || Frd.mid == "sch") {
                    b = "flwFrd flwFrdEvent";
                    var S = "关注";
                    Frd.mid == "sch" && e[r].flwd && (b = "cnct on", S = "已关注"), f = '<a href="#" class="rndBtn ' + b + '" data-uid="' + m + '" data-nick="' + g + '" data-tps="' + S + '"></a>', Frd.mid == "mtFrds" && (l = '<a href="#" class="rndBtn notKnwn notKnwnEvent" data-uid="' + m + '" data-tps="不认识"></a>'), Frd.mid == "sch" && e[r].flwd && e[r].ol && !e[r].frdshp && (p = '<a href="#" class="rndBtn pkr rmndEvent" data-uid="' + m + '" data-nick="' + g + '" data-tps="提醒Ta"></a>')
                }
                Frd.mid == "inHs" && (e[r].ol && (s = '<a href="#" class="rndBtn chtflwLstn flwLstnEvent" data-uid="' + m + '" data-nick="' + g + '" data-tps="跟听"></a>'), e[r].ol && !e[r].frdshp && (p = '<a href="#" class="rndBtn pkr rmndEvent" data-uid="' + m + '" data-nick="' + g + '" data-tps="提醒Ta"></a>'), u = '<a class="rndBtn blkFrd blkEvent" data-uid="' + m + '" href="#" data-tps="扔掉"></a>');
                if (Frd.mid == "inHs" || ConverSns[Frd.mid] != undefined && e[r].inhs) {
                    var x = "",
                        S = "聊天",
                        T = "",
                        N = "cht";
                    m == 0 && (x = "sys", S = "查看", N = "ntfc"), y || (T = 'style="display:none"'), o = '<a href="#" class="rndBtn ' + N + ' chtEvent" ' + T + ' data-uid="' + m + '" data-nick="' + g + '" data-no="' + v + '" data-tps="' + S + '" data-frdshp="' + y + '"></a>', Cht.offlineMes[m] != undefined && (s == "" ? o += '<em class="num serif cht ' + x + '">' + Cht.offlineMes[m].count + "</em>" : o += '<em class="num serif cht ' + x + '" style="right:103px">' + Cht.offlineMes[m].count + "</em>"), b = "chtEvent"
                }
                var C = IMG_URL + "/defaults/avatar/" + Core.A50 + ".jpg";
                Frd.mid == "inHs" && Core.isNewBdg(m, !0) && (d = '<em class="newApp overlay" style="left:50px; top: 5px;"></em>'), b != "chtEvent" && (b = "");
                var k = "";
                e[r].uid == "0" ? k = '<a class="' + b + '" href="#" data-uid="' + m + '" data-nick="' + g + '" data-no="' + v + '" data-frdshp="' + y + '"><img src="' + C + '" class="chtAvt" data-fid="sys">' : k = '<a class="chtMask ' + w + '" href="#" data-uid="' + m + '" data-nick="' + g + '" data-no="' + v + '" data-frdshp="' + y + '"></a><img src="' + C + '" class="chtAvt" data-fid="' + e[r].avatar + '">';
                var L = p + o + s + a + u + f + l + c + h + i;
                e[r].uid == Signup.userDetail.id && (L = "");
                var A = "",
                    O = "";
                e[r].snsuser != null && (A = e[r].snsuser.identify, O = e[r].snsuser.nick), t += '<div class="rctCtn frdEvent" data-nick="' + O + '" data-identify="' + A + '">' + '<div class="avtCtn">' + k + "</div>" + '<a class="desc ' + b + '" href="#" data-uid="' + m + '" data-nick="' + g + '" data-no="' + v + '" data-frdshp="' + y + '">' + g + "</a>" + '<div class="tpsMenuCtn ' + Frd.mid + '">' + L + "</div>" + d + "</div>"
            }
            return t
        },
        sttsHtml: function (e, t) {
            var n = "";
            Frd.mid != "mtFrds" && t != "" && (n = '<a href="#" class="more ' + t + '"></a>');
            var r = '<div class="rctCtn stts"><p class="desc islt" >' + e + "</p>" + '<div class="rBox">' + n + "</div>" + "</div>";
            return r
        },
        openData: function (e, t, n) {
            var r = null;
            $("#abtCttCtn>.frdCt>.rctCtn").each(function (t) {
                if ($(this).data("mid") == e) return r = $(this), !1
            }), n != undefined && n == 1 ? Frd.mid == "invt" ? ($("#frdList>.fm").remove(), $("#frdList>.invtAppCtn").after(t), $("#frdList").animate({
                height: Frd.h + "px"
            }, 300, function () {
                $(this).css("height", "auto"), Frd.replaceAvatar(), About.scrollApi.reinitialise()
            })) : ($("#frdList>.frdEvent").remove(), $("#frdList").prepend(t), $("#frdList").animate({
                height: Frd.h + "px"
            }, 300, function () {
                $(this).css("height", "auto"), Frd.replaceAvatar(), About.scrollApi.reinitialise()
            })) : (r.after('<div id="frdList" style="height:0px">' + t + "</div>"), $("#frdList").addClass(e).animate({
                height: Frd.h + "px"
            }, 300, function () {
                $(this).css("height", "auto"), Frd.replaceAvatar(), About.scrollApi.reinitialise()
            })), Frd.mid == "invt" && !Core.ie && ($(".copyEvent").clippy(), $(".copyEvent").children().css({
                position: "absolute",
                left: "20px",
                top: "6px"
            }))
        },
        closeData: function (e) {
            Frd.mid = "", $("#frdList").prop("id", "oldFrdList"), $("#oldFrdList").prev().children(".tpsMenuCtn").children(".ext").removeClass("on").addClass("off"), $("#oldFrdList").animate({
                height: "0px"
            }, 300, function () {
                $(this).remove(), About.scrollApi.reinitialise()
            })
        },
        frdMore: function () {
            $.ajax({
                url: resUrl[Frd.mid].replace(/{id}/g, Signup.userDetail.id),
                data: {
                    st: Frd.st,
                    ps: Frd.ps
                },
                success: function (e) {
                    var t = e.result.items;
                    Frd.h = Frd.getHeight(!0, t.length);
                    var n = "";
                    if (Frd.mid == "invt") {
                        Frd.h += 125;
                        for (var r = 0; r < t.length; ++r) n += Frd.invtAppGnHtml(t[r])
                    } else n = Frd.frdHtml(t);
                    (t.length == 0 || Frd.st + t.length >= e.result.total) && $(".frdMoreEvent").remove(), Frd.st += t.length, Frd.openData(Frd.mid, n, !0)
                }
            })
        },
        iconMore: function () {
            var e = resData.ext;
            Frd.st += $(".iconEvent").length;
            var t = 0;
            $(".iconEvent").each(function (n) {
                var r, i = n + Frd.st;
                i >= e.length ? ($(this).remove(), ++t) : (ConverSns[e[i].mid] != undefined ? Frd.isMenuShow(ConverSns[e[i].mid]) ? r = '<a data-mid="' + e[i].mid + '" href="#" class="rndBtn cnct cnctEvent on" data-tps="已绑定"></a>' : r = '<a data-mid="' + e[i].mid + '" href="#" class="rndBtn cnct cnctEvent off" data-tps="绑定"></a>' : r = '<a data-mid="' + e[i].mid + '" href="#" class="rndBtn cnct cnctEvent off" data-tps="连接"></a>', $(this).data("mid", e[i].mid), $(this).children("a").data("mid", e[i].mid), $(this).children(".tpsMenuCtn").html(r), $(this).children(".frdIcon").prop("class", "frdIcon " + e[i].mid), $(this).children(".desc").text(e[i].name))
            }), t > 0 && ($("#appsCtn").animate({
                height: "-=" + (t * Frd.ITEM_H + Frd.STTS_H)
            }, 300), $("#appsCttCtn>.stts").remove())
        },
        replaceAvatar: function () {
            $(".frdEvent").find("img").each(function () {
                var e = $(this).data("fid");
                if (e == "f") return;
                e == "" ? $(this).prop({
                    width: "50px",
                    height: "50px"
                }).css({
                    width: "50px",
                    height: "50px"
                }) : ($(this).css("background-color", "#e1e1e1"), Core.imgLoad($(this), "", e, 50, "frd")), $(this).data("fid", "f")
            })
        },
        getHeight: function (e, t) {
            var n = t * Frd.ITEM_H;
            return e && (n += Frd.STTS_H), n
        },
        getMaxCount: function (e) {
            var t = Core.bodyHeight - 170;
            e && (t -= Frd.STTS_H);
            var n = parseInt(t / Frd.ITEM_H);
            return n
        },
        prflClick: function () {
            var e = {
                ouid: $(this).data("uid"),
                nick: $(this).data("nick")
            };
            About.currentMain = "abt", About.switching(e)
        }
    }, About = {
        isAnimate: !1,
        no: 0,
        tabNo: 0,
        isOpen: !1,
        MAIN_WIDTH: 1100,
        isSchFldAnimate: !0,
        st: 0,
        total: 0,
        PS: 20,
        mid: "",
        isLoad: !1,
        scrollApi: null,
        abtCtn: null,
        ouid: "",
        isInsertPlay: !1,
        ptTms: 0,
        isTopInsertPlay: !1,
        isSingleInsertPlay: !1,
        isAtInsertPlay: !1,
        atInsertPlayNick: "",
        playNextId: 0,
        model: !0,
        modelChanging: !1,
        currentMain: "",
        topId: "",
        topObj: new Object,
        topChildObj: new Object,
        FID: 0,
        TITLE: 1,
        NAME: 2,
        DISPLAY: 3,
        OFFSET_TOP: 120,
        scrollY: 0,
        isStart: !1,
        favObj: new Object,
        cacheAbout: new Array,
        init: function () {
            swfobject.ua.pv[0] || $("#abtCover").click(function () {
                Gns.nowGns("请安装Flash后再上传封面。")
            }), $("#about").click(function () {
                if ($("#topApp").isDisplay() || Signup.userDetail.newbie != 0) return;
                _gaq.push(["_trackEvent", "About", "AbtExt", "AboutMenu"]), $("#aboutCount").hide();
                var e = {
                    ouid: Signup.userDetail.id,
                    nick: Signup.userDetail.nick
                };
                About.currentMain = "abt", About.switching(e)
            }), $("#abtCover").prop("src", IMG_URL + "/defaults/profile/cover" + Retina.suffix + ".jpg"), $("#abtAvt").prop("src", IMG_URL + "/defaults/avatar/80" + Retina.suffix + ".jpg.png");
            var leftpx = function (e) {
                var t = e * 73 + 14;
                return e > 1 && (t += 100), t
            };
            $("#abtBckBtn").click(function () {
                var e = {};
                return About.cacheAbout.length == 0 ? ($("#about").click(), !1) : About.cacheAbout[About.cacheAbout.length - 1].uid == About.ouid ? (About.cacheAbout = About.cacheAbout.slice(0, About.cacheAbout.length - 1), $("#abtBckBtn").click(), !1) : (e = {
                    ouid: About.cacheAbout[About.cacheAbout.length - 1].uid,
                    nick: About.cacheAbout[About.cacheAbout.length - 1].nick
                }, About.cacheAbout = About.cacheAbout.slice(0, About.cacheAbout.length - 1), About.currentMain = "abt", About.switching(e), !1)
            }), $(".abtBtn").mouseenter(function () {
                var e = leftpx(parseInt($(this).data("no")));
                $("#willCrt").animate({
                    left: e + "px"
                }, {
                    queue: !1
                }, 300)
            }).click(function () {
                var no = parseInt($(this).data("no")),
                    left = leftpx(no);
                $("#nowCrt").animate({
                    left: left + "px"
                }, {
                    queue: !1
                }, 300), About.no = no, About.mid = $(this).data("mid"), About.ctn.hide().html(""), About.mid == "stgs" && About.ouid != Signup.userDetail.id ? About.ctn.prop("class", "abtCttCtn xFile") : About.ctn.prop("class", "abtCttCtn " + About.mid), $("#abtCttCtn").css("border-left", "none"), About.st = 0, About.isLoad = !0, eval("About." + About.mid)()
            }), $(".abtMenu").mouseleave(function () {
                var e = leftpx(parseInt(About.no));
                $("#willCrt").animate({
                    left: e + "px"
                }, {
                    queue: !1
                }, 300)
            }), $(document).on("mouseenter", ".abtTab", function () {
                var e = 0,
                    t = parseInt($(this).data("no"));
                About.mid != "love" && About.mid != "moods" || Signup.userDetail.id == About.ouid ? (e = 125, t == 2 ? $("#tabWillCrt").css("width", "130px") : $("#tabWillCrt").css("width", "125px")) : ($("#tabNowCrt").css("width", "190px"), $("#tabWillCrt").css("width", "190px"), e = 190);
                var n = t * e;
                $("#tabWillCrt").animate({
                    left: n + "px"
                }, {
                    queue: !1
                }, 300)
            }), $(document).on("click", ".abtTab", function () {
                var no = parseInt($(this).data("no")),
                    w = 0;
                About.mid == "love" && Signup.userDetail.id != About.ouid ? ($("#tabNowCrt").css("width", "190px"), w = 190) : (w = 125, no == 2 ? $("#tabNowCrt").css("width", "130px") : $("#tabNowCrt").css("width", "125px"));
                var left = no * w;
                $("#tabNowCrt").animate({
                    left: left + "px"
                }, {
                    queue: !1
                }, 300), About.tabNo = no, About.mid = $(this).data("mid"), About.ctn.children(".abtTabCtn").next().html(""), About.ctn.css("border-bottom", "none"), About.st = 0, About.isLoad = !0, Frd.mid = "", $("#abtCttCtn").css("border-left", "none"), $("#abtCttCtn>.frdCt").prop("class", "frdCt lstCtn " + About.mid), eval("About." + About.mid)()
            }), $(document).on("mouseleave", ".abtTabCtn", function () {
                var e = 0,
                    t = parseInt($(this).data("no"));
                About.mid != "love" && About.mid != "moods" || Signup.userDetail.id == About.ouid ? e = 125 : e = 190;
                var n = parseInt(About.tabNo) * e;
                $("#tabWillCrt").animate({
                    left: n + "px"
                }, {
                    queue: !1
                }, 300)
            }), $(document).on("click", "#closeMoodPlay", About.closeMoodPlay), $(document).on("click", "#closeSinglePlay", About.closeSinglePlay), $(document).on("click", "#closeAtPlay", About.closeAtPlay), $(document).on("click", "#closeAbtTopPlay", About.aboutTop), $("#abtCtn").children().bind("jsp-scroll-y", About.scrollYEvent).jScrollPane(), $("#abtCtn").mouseenter(function () {
                $(".jspVerticalBar").animate({
                    opacity: "1"
                }, {
                    speed: 300,
                    queue: !1
                })
            }).mouseleave(function () {
                $(".jspVerticalBar").animate({
                    opacity: "0"
                }, {
                    speed: 300,
                    queue: !1
                })
            }), About.scrollApi = $("#abtCtn").children().data("jsp"), About.ctn = $("#abtCttCtn"), $(document).on("click", ".loveBtn", About.loveBtnClick), $(document).on("click", ".hateBtn", Apps.hateBtnClick), $(document).on("click", ".moodEvent", About.moodClick), $(document).on("click", ".delFltr", function () {
                $.ajax({
                    url: Core.API_VER + "/account/filter/remove_filter_tags",
                    data: {
                        uid: Signup.userDetail.id,
                        tid: $(this).parent().prop("id")
                    }
                });
                if ($("#fltrAppCtn").children().length == 1) $("#fltrAppCtn").css({
                        height: "+=41",
                        "padding-top": "0px",
                        "padding-bottom": "0px"
                    }), $("#fltrAppCtn").parent().parent().animate({
                        height: "-=" + $("#fltrAppCtn").height()
                    }, 300), $("#fltrAppCtn").html("").animate({
                        height: "0px"
                    }, 300, function () {
                        $("#fltrAppCtn").hide()
                    });
                else {
                    var e = About.fltrHeight($("#fltrAppCtn").html(), $(this).parent().prop("id"));
                    e != $("#fltrAppCtn").height() + 41 ? (e = $("#fltrAppCtn").height() + 41 - e, $("#fltrAppCtn").parent().parent().animate({
                        height: "-=" + e
                    }, 300), $("#fltrAppCtn").animate({
                        height: "-=" + e
                    }, 300), $(this).parent().remove()) : $(this).parent().remove()
                }
            }), $(document).on("click", ".tkrsFavEvent", function () {
                var e = $(this).data("ouid"),
                    t = $(this).data("tit"),
                    n = $(this).data("t"),
                    r = $(this).data("fid"),
                    i = $(this).parent(),
                    s = $(this);
                $(this).hasClass("selected") ? $.ajax({
                    url: Core.API_VER + "/ticker/remove_favs",
                    data: {
                        uid: Signup.userDetail.id,
                        ouid: e,
                        tit: t,
                        t: n,
                        fid: r
                    },
                    success: function (n) {
                        n.success && (s.removeClass("selected"), About.mid == "tkrsFav" && i.animate({
                            height: "0px"
                        }, 300, function () {
                            $(this).remove(), $(".tkrsFavEvent").length == 0 && About.ctnHtml(About.noCttHtml("收藏动态中喜欢的组合", "about"), "append"), About.scrollApi.reinitialise()
                        }), delete About.favObj[e + t])
                    }
                }) : $.ajax({
                    url: Core.API_VER + "/ticker/post_favs",
                    data: {
                        uid: Signup.userDetail.id,
                        ouid: e,
                        tit: t,
                        t: n,
                        fid: r
                    },
                    success: function (n) {
                        if (n.success || n.code == "319") s.css("color", "red"), s.addClass("selected"), About.favObj[e + t] = !0
                    }
                })
            }), $(document).on("click", ".tkrsLoveEvent", function () {
                var e = $(this).data("tid");
                $(this).hasClass("selected") ? ($(".rctAbtCtn>a[data-tidId='l" + e + "']").removeClass("selected"), $(".rctAbtCtn>a[data-tidId='l" + e + "']").mouseleave(), Player.music[Player.pos].tid == e && $("#playerLove").removeClass("selected")) : ($(".rctAbtCtn>a[data-tidId='l" + e + "']").addClass("selected"), $(".rctAbtCtn>a[data-tidId='l" + e + "']").mouseenter(), Player.music[Player.pos].tid == e && $("#playerLove").addClass("selected")), $.ajax({
                    url: Core.API_VER + "/music/post_love_song",
                    data: {
                        uid: Signup.userDetail.id,
                        ouid: $(this).data("uid"),
                        tid: e
                    }
                })
            }), $(document).on("click", ".singlePlay", function () {
                About.isSingleInsertPlay = !0, Player.cacheMusic == null && (Player.cacheMusic = Player.music, Player.cachePos = Player.pos, Player.cacheSt = Search.st, Player.cacheSec = Player.currentTime, Player.cacheTotal = Search.total);
                var e = $(this).data("fid"),
                    t = $(this).data("mid"),
                    n = $(this).data("tid"),
                    r = $(this).data("name"),
                    i = $(this).data("d"),
                    s = $(this).data("an"),
                    o = $(this).data("atn"),
                    u = $(this).data("nick");
                Player.music = new Array, Search.setMusic({
                    fid: e,
                    mid: t,
                    tid: n,
                    n: r,
                    d: i,
                    an: s,
                    atn: o
                }, 0), Core.fullScrenMenuShow('<span id="prevHtml">正在收听 ' + u + '的动态 <span class="spl"></span><a href="#" id="closeSinglePlay" class="clsFlwLstn overlay">退出</a></span>', "singlePlay"), Player.pos = -1, About.next()
            }), $(document).on("mouseenter", ".cmbt, .uprd", function () {
                if ($(this).children(".favIcon").hasClass("selected")) return;
                Core.ie68 ? $(this).children(".favIcon").show() : $(this).children(".favIcon").css("opacity", "1")
            }), $(document).on("mouseleave", ".cmbt, .uprd", function () {
                if ($(this).children(".favIcon").hasClass("selected")) return;
                Core.ie68 ? $(this).children(".favIcon").hide() : $(this).children(".favIcon").css("opacity", "0")
            }), $(document).on("mouseenter", ".love", function () {
                if ($(this).children(".favIcon").hasClass("selected")) return;
                Core.ie68 ? $(this).children(".favIcon").show() : $(this).children(".favIcon").css("opacity", "1")
            }), $(document).on("mouseleave", ".love", function () {
                if ($(this).children(".favIcon").hasClass("selected")) return;
                Core.ie68 ? $(this).children(".favIcon").hide() : $(this).children(".favIcon").css("opacity", "0")
            }), About.uploadify(), Core.ipad && fingerGestureListener($("#abtCtn")[0], function (e) {
                e.status == "end" && $("#about").click()
            }), $("#uploadCover").css("opacity", "0").hide(), $("#cvCtn").mouseenter(function () {
                Signup.userDetail.id == About.ouid && ($("#uploadCover").show(), $("#uploadCover").css("opacity", "0"), $("#uploadCover").animate({
                    opacity: "1"
                }, {
                    queue: !1
                }, 300))
            }).mouseleave(function () {
                $("#uploadCover").animate({
                    opacity: "0"
                }, {
                    queue: !1
                }, 300)
            }), $(document).on("click", "#pmlnkOK", function () {
                $("#gnsCtn").data("tag", ""), Gns.isOver = !1, Gns.closeGns();
                var e = $(this).data("text");
                return $.ajax({
                    url: Core.API_VER + "/account/update_profile",
                    data: {
                        permalink: e,
                        uid: Signup.userDetail.id
                    },
                    success: function (t) {
                        t.success ? $("#pmlnk").length == 1 && ($("#pmlnk").html("jing.fm/" + e), $("#pmlnk").unbind("click"), About.pmlnkBtnHtml(e)) : Gns.nowGns(t.codemsg)
                    }
                }), !1
            }), $(document).on("click", "#pmlnkNO", function () {
                return $("#gnsCtn").data("tag", ""), Gns.isOver = !1, Gns.closeGns(), $("#pmlnk").length == 1 && ($("#pmlnk").html("jing.fm/" + $(this).data("text")), About.pmlnkBtnHtml($(this).data("text"))), !1
            }), $(document).on("click", "#loveRd, #atNickPlay", function () {
                if ($(this).hasClass("pause")) $("#closeAtPlay").click();
                else {
                    About.isAtInsertPlay = !0, About.atInsertPlayNick = $(this).data("nick");
                    var e = "";
                    $(this).prop("id") == "loveRd" ? (e = "红心电台", Core.isFullScren = !1, About.atInsertPlayNick = Signup.userDetail.nick) : e = About.atInsertPlayNick + " 喜欢的歌曲", Core.fullScrenMenuShow('<span id="prevHtml">正在收听 ' + e + ' <span class="spl"></span><a href="#" id="closeAtPlay" class="clsFlwLstn overlay">退出</a></span>', "atPlay"), $.ajax({
                        url: Core.API_VER + "/search/jing/fetch_pls",
                        data: {
                            q: "@" + About.atInsertPlayNick,
                            ps: 100,
                            st: "0",
                            u: Signup.userDetail.id
                        },
                        success: function (e) {
                            Player.music = new Array;
                            for (var t = 0; t < e.result.items.length; ++t) Search.setMusic(e.result.items[t], t);
                            Player.pos = -1, $("#playCtl").hasClass("pause") && $("#playCtl").click(), About.next()
                        }
                    })
                }
                return !1
            })
        },
        insertStop: function (e) {
            Player.singleObj = undefined, About.isSingleInsertPlay = !1;
            if (e == "continue") {
                Player.setPlay(Player.cacheSec, Player.music[Player.pos].mid, Player.music[Player.pos].tid);
                var t = function () {
                    Player.isVolumeDown ? setTimeout(t, 200) : Player.playCtl()
                };
                t()
            } else e == "next" && Player.next()
        },
        scrollYEvent: function (event, scrollPositionY, isAtTop, isAtBottom) {
            About.currentMain == "abt" && (About.isStart || $("#uploadCover").hide(), $("#tps").hide(), About.scrollY = scrollPositionY, About.model && scrollPositionY >= 360 ? (About.model = !1, $("#cvCtn").css({
                position: "fixed",
                top: "-360px",
                right: "0px"
            }), $("#abtMenu").css({
                position: "fixed",
                top: "60px",
                right: "0px"
            }), $("#mask").show()) : !About.model && scrollPositionY < 360 && ($("#mask").hide(), $("#cvCtn").css({
                position: "relative",
                top: "0px"
            }), $("#abtMenu").css({
                position: "relative",
                top: "0px"
            }), About.model = !0), isAtBottom && !About.isLoad && About.st < About.total && About.mid != "stgs" && About.mid != "ext" && (About.isLoad = !0, eval("About." + About.mid)()))
        },
        uploadify: function () {
            $("#uploadCover").uploadify({
                formData: {
                    uid: Signup.userDetail.id,
                    atoken: Core.getCookie("jing.auth").split(",")[0],
                    rtoken: Core.getCookie("jing.auth").split(",")[1]
                },
                uploader: "/api/v1/personal/coverupload",
                swf: "http://jing.fm/assets/vendor/uploadify.swf",
                auto: !0,
                fileObjName: "file",
                width: 73,
                multi: !1,
                height: 65,
                buttonClass: "cvUpldBtn",
                buttonText: "",
                onSWFReady: function () {},
                onDialogOpen: function () {},
                onDialogClose: function () {},
                onUploadStart: function (e) {
                    About.isStart = !0, $("#abtCover").prop("src", IMG_URL + "/defaults/profile/cover" + Retina.suffix + ".jpg");
                    var t = {
                        lines: 13,
                        length: 7,
                        width: 4,
                        radius: 10,
                        corners: 1,
                        rotate: 0,
                        color: "#fff",
                        speed: .8,
                        trail: 10,
                        shadow: !0,
                        hwaccel: !1,
                        className: "abtSpin",
                        zIndex: 2e9,
                        top: "180px",
                        left: "auto"
                    };
                    About.spin = $("#cvCtn").spin(t)
                },
                onUploadSuccess: function (file, response, data) {
                    response = eval("(" + response + ")");
                    if (response.success) {
                        var cover = new Image;
                        cover.onload = function () {
                            $("#abtCover").prop("src", this.src), About.spin.data().spinner.stop()
                        }, cover.src = $.id2url(response.result, "CP", "cover")
                    }
                    About.isStart = !1
                }
            })
        },
        switching: function (e) {
            if (About.isAnimate) return;
            Cht.fuid = "", $("#abtCtn").show(), About.currentMain == "abt" ? About.abtSwitch(e) : About.topSwitch(e)
        },
        abtSwitch: function (e) {
            $("#abtMainCtn").show(), $("#topMainCtn").hide(), About.topId = "", $("#aboutNewIcon").isDisplay() && ($("#aboutNewIcon").hide(), Core.ulNewIcon("m", "about"));
            if (About.isOpen && e.ouid == About.ouid) About.cacheAbout = new Array, About.stopPt(), About.isOpen = !1, About.isAnimate = !0, About.ouid = "", About.scrollApi.scrollToY(0), About.currentMain = "", $("#gnsCtn").isDisplay() && $("#gnsCtt").animate({
                    left: "+=210"
                }, 300), $("#allBd").css("width", Core.bodyWidth + 420 + 420 + "px"), Core.bodyWidth += 420, Player.resize(), Search.resize(!0), Core.bodyWidth -= 420, $("#mainBd").animate({
                    width: Core.bodyWidth + 420 + "px"
                }, 300, function () {
                    $("#allBd, #mainBd").css("width", "100%"), $("#abtCtn").hide(), Core.resize(), About.isAnimate = !1, About.ctn.hide().html(""), $("#abtCover").prop("src", IMG_URL + "/defaults/profile/cover" + Retina.suffix + ".jpg"), $("#abtAvt").prop("src", IMG_URL + "/defaults/avatar/80" + Retina.suffix + ".jpg.png"), About.model || (About.scrollApi.reinitialise(), $("#mask").hide(), $("#cvCtn").css({
                        position: "relative",
                        top: "0px"
                    }), $("#abtMenu").css({
                        position: "relative",
                        top: "0px"
                    }), About.model = !0, About.scrollApi.scrollToY(0))
                });
            else {
                About.cacheAbout[About.cacheAbout.length] = {
                    uid: e.ouid,
                    nick: e.nick
                }, $("#frdsTkrsCount, #frdsChtCount").hide(), About.stopPt(), About.mid = "tkrs", About.ouid = e.ouid, About.st = 0, About.no = 0, $("#abtDes").html('<span id="aboutNick">' + e.nick + '</span>&nbsp<br><span class="lstnPrd">Ta已经听了<span>'), $("#atNickPlay").data("nick", e.nick);
                var t = $("#cvCtn>.tpsMenuCtn");
                t.children(".offChtNickEvent, .flwLstnEvent, .flwFrdEvent, .psnRdEvent").hide(), About.isOpen ? (About.ctn.hide().html(""), $("#abtCover").prop("src", IMG_URL + "/defaults/profile/cover" + Retina.suffix + ".jpg"), $("#abtAvt").prop("src", IMG_URL + "/defaults/avatar/80" + Retina.suffix + ".jpg.png"), About.model || (About.scrollApi.reinitialise(), $("#mask").hide(), $("#cvCtn").css({
                    position: "relative",
                    top: "0px"
                }), $("#abtMenu").css({
                    position: "relative",
                    top: "0px"
                }), About.model = !0, About.scrollApi.scrollToY(0))) : (About.isAnimate = !0, About.isOpen = !0, NtrlLngTop.isOpen ? (NtrlLngTop.isOpen = !1, $("#allBd").css("width", Core.bodyWidth + 420 + 420 + "px"), $("#abtCtn").show(), $("#mainBd").animate({
                    width: "-=20"
                }, 300), $("#ntrlLngTopCtn").animate({
                    width: "0px"
                }, 300, function () {
                    About.isAnimate = !1, $("#allBd").css("width", "100%"), $(this).hide()
                })) : ($("#allBd").css("width", Core.bodyWidth + 420 + "px"), $("#mainBd").css("width", Core.bodyWidth + "px"), $("#abtCtn").show(), $("#mainBd").animate({
                    width: Core.bodyWidth - 420 + "px"
                }, 300, function () {
                    About.isAnimate = !1, $("#allBd").css("width", "100%")
                }), $("#gnsCtn").isDisplay() && $("#gnsCtt").animate({
                    left: "-=210"
                }, 300), Core.resize())), About.ctn.prop("class", "abtCttCtn tkrs");
                if (Signup.userDetail.id == e.ouid) {
                    Signup.userDetail.newview.t.length != 0 && $("#stgsNewIcon").show(), About.cacheAbout = new Array, $("#abtBckBtn").hide(), $("#atNickPlay").hide(), $("#aboutStgs").text("设置");
                    var n = Cht.offlineCount;
                    Signup.userDetail.sts.frdCntNtf == "true" && (n += Signup.onlineCount), n != 0 && $("#frdsChtCount").text(n).show()
                } else $("#abtBckBtn").show(), $("#atNickPlay").show(), $("#aboutStgs").text("档案"), $("#uploadCover").hide();
                $.ajax({
                    url: Core.API_VER + "/personal/bases",
                    data: {
                        uid: Signup.userDetail.id,
                        ouid: e.ouid
                    },
                    success: function (n) {
                        var r = n.result,
                            i = "",
                            s = '<span> <strong id="abtSec" class="serif">' + r.pt % 60 + "</strong> 秒</span>";
                        r.pt / 60 < 1 && (i = "hide"), s = '<span class="' + i + '"> <strong id="abtMin" class="serif">' + parseInt(r.pt / 60) % 60 + "</strong> 分钟</span>" + s, i = "", r.pt / 60 / 60 < 1 && (i = "hide"), s = '<span class="' + i + '"> <strong id="abtHour" class="serif">' + parseInt(r.pt / 60 / 60) + "</strong> 小时</span>" + s, $("#abtDes>.lstnPrd").append(s), r.user.ol && (Signup.userDetail.id == About.ouid ? $("#playCtl").hasClass("pause") && About.startPt() : About.startPt());
                        var o = r.user.avatar;
                        if (o != "" && o != null) {
                            var u = new Image;
                            u.onload = function () {
                                $("#abtAvt").prop("src", this.src)
                            }, o.indexOf("http://") != 0 ? (o = $.id2url(o, "U1", "avatar"), o.indexOf("?") >= 0 ? o = o.substring(0, o.indexOf("?")) + ".png" + o.substring(o.indexOf("?")) : o += ".png", $("#abtAvt").prev().css("background", 'url("' + IMG_URL + '"/cclAvtMask.png") no-repeat scroll 0 0 transparent'), $("#abtAvt").parent().css("border", "none")) : Core.ie68 && ($("#abtAvt").prev().css("background-image", "none"), $("#abtAvt").parent().css("border", "3px solid #fff")), u.src = o
                        }
                        var a = new Image;
                        a.ouid = About.ouid, a.onload = function () {
                            if (About.ouid != this.ouid) return;
                            $("#abtCover").prop("src", this.src)
                        }, r.cover.length >= 17 ? a.src = $.id2url(r.cover, "CP", "cover") : a.src = IMG_URL + "/defaults/profile/" + r.cover.substring(0, r.cover.indexOf(".")) + Retina.suffix + ".jpg";
                        var f = r.user.frdshp,
                            l = r.user.ol,
                            c = r.user.flwd;
                        Signup.userDetail.id == About.ouid && (f = l = !1, c = !0), t.children("a").data({
                            nick: e.nick,
                            uid: e.ouid,
                            frdshp: f,
                            ol: l,
                            avatar: r.user.avatar
                        }), f && l && t.children(".flwLstnEvent").show(), f && t.children(".offChtNickEvent").show(), c || t.children(".flwFrdEvent").show(), Signup.userDetail.id == About.ouid ? (t.children(".flwLstnEvent").hide(), t.children(".psnRdEvent").hide()) : t.children(".flwLstnEvent").data("tps", "跟听"), $("#nowCrt, #willCrt").css("left", "14px");
                        if (Frd.olFrdObj != "") {
                            setTimeout(function () {
                                $("#abtMenu>.frds").click(), $("#abtMenu").mouseleave()
                            }, 300);
                            return
                        }
                        About.ctn.addClass("abtCttCtn tkrs");
                        var h = function () {
                            About.isAnimate ? setTimeout(h, 50) : Signup.userDetail.id == About.ouid ? About.tkrs(undefined) : About.tkrs(r.ticker)
                        };
                        setTimeout(h, 50)
                    }
                })
            }
        },
        topSwitch: function (e) {
            $("#abtMainCtn").hide(), $("#topMainCtn").show(), About.ouid = "";
            if (About.isOpen && About.topId == e.ouid) About.isOpen = !1, About.isAnimate = !0, About.scrollApi.scrollToY(0), About.topId = "", About.currentMain = "", $("#gnsCtn").isDisplay() && $("#gnsCtt").animate({
                    left: "+=210"
                }, 300), $("#allBd").css("width", Core.bodyWidth + 420 + 420 + "px"), Core.bodyWidth += 420, Player.resize(), Search.resize(!0), Core.bodyWidth -= 420, $("#mainBd").animate({
                    width: Core.bodyWidth + 420 + "px"
                }, 300, function () {
                    $("#allBd, #mainBd").css("width", "100%"), $("#abtCtn").hide(), Core.resize(), About.isAnimate = !1, $("#topMainCtn").html("")
                });
            else {
                $("#frdsTkrsCount, #frdsChtCount").hide(), About.topId = e.ouid, About.isOpen ? $("#topMainCtn").html("") : (About.isAnimate = !0, About.isOpen = !0, NtrlLngTop.isOpen ? (NtrlLngTop.isOpen = !1, $("#allBd").css("width", Core.bodyWidth + 420 + 420 + "px"), $("#abtCtn").show(), $("#mainBd").animate({
                    width: "-=20"
                }, 300), $("#ntrlLngTopCtn").animate({
                    width: "0px"
                }, 300, function () {
                    About.isAnimate = !1, $("#allBd").css("width", "100%"), $(this).hide()
                })) : ($("#allBd").css("width", Core.bodyWidth + 420 + "px"), $("#mainBd").css("width", Core.bodyWidth + "px"), $("#abtCtn").show(), $("#mainBd").animate({
                    width: Core.bodyWidth - 420 + "px"
                }, 300, function () {
                    About.isAnimate = !1, $("#allBd").css("width", "100%")
                }), $("#gnsCtn").isDisplay() && $("#gnsCtt").animate({
                    left: "-=210"
                }, 300), Core.resize())), $("#topMainCtn").html('<div id="chrtLstCv" class="chrtLstCv"></div>');
                var t = new Image;
                t.onload = function () {
                    $("#chrtLstCv").css("background-image", "url('" + this.src + "')")
                }, t.src = $.id2url(About.topObj[About.topId][About.FID], "BC", "chart_cover");
                var n = "",
                    r = new Array;
                for (var i in About.topChildObj[About.topId]) n += i + ",", r[r.length] = i;
                $.ajax({
                    url: Core.API_VER + "/chart/fetch",
                    data: {
                        uid: Signup.userDetail.id,
                        nodeids: n.substring(0, n.length - 1)
                    },
                    success: function (e) {
                        if (!e.success) return;
                        for (var t = 0; t < r.length; ++t) {
                            var n = e.result[r[t]],
                                i = n.items,
                                s = '<div class="chrtLst"><div class="chrtYr">' + About.topObj[About.topId][About.TITLE] + '<span class="">' + n.name + "</span></div>";
                            for (var o = 0; o < i.length; ++o) {
                                var u = "",
                                    a = "";
                                (o + 1) % 4 == 0 && (u = 'style="border-right:none;"'), i[o].newest > 0 && Apps.offsetDate(i[o].newest, "top") && (a = '<em class="newApp overlay" style="top:5px; left:56px;"></em>'), s += "<a " + u + ' class="chrtIsu chrtPlay" href="#" data-next="' + i[o].next + '">' + i[o].name + a + "</a>"
                            }
                            var f = 4 - i.length % 4;
                            f == 4 && (f = 0);
                            for (var o = 0; o < f; ++o) {
                                var u = "";
                                o == f - 1 && (u = 'style="border-right:none;"'), s += "<a " + u + ' class="chrtIsu" href="#"></a>'
                            }
                            s += "</div>", $("#topMainCtn").append(s)
                        }
                        About.scrollApi.reinitialise(), $(".chrtPlay").click(function (e) {
                            About.chrtPlayObj != undefined && (About.chrtPlayObj.children(".chrtIsuHvr").remove(), $(this).removeClass("selected")), About.chrtPlayObj = $(this), $(this).append('<em class="chrtIsuHvr"></em>'), $(this).addClass("selected"), $("#instSchCtn").isDisplay() && $(document).click();
                            var t = $(this).data("next"),
                                n = $(this).text(),
                                r = $(this).parent().children().first().children("span").text();
                            return $.ajax({
                                url: Core.API_VER + "/chart/fetch",
                                data: {
                                    uid: Signup.userDetail.id,
                                    nodeids: t
                                },
                                success: function (e) {
                                    About.aboutTop(e, t, n, r)
                                }
                            }), !1
                        }).mouseenter(function () {
                            if ($(this).hasClass("selected")) return;
                            $(this).append('<em class="chrtIsuHvr"></em>')
                        }).mouseleave(function () {
                            if ($(this).hasClass("selected")) return;
                            $(this).children(".chrtIsuHvr").remove()
                        })
                    }
                })
            }
        },
        aboutTop: function (e, t, n, r) {
            if (Flw.isFlw && Flw.toid != "") {
                Gns.nowGns("你正在跟听中，暂时不能做其他操作哦。");
                return
            }
            if (!About.isTopInsertPlay || About.playNextId != t && t != undefined) {
                About.playNextId = t, About.isTopInsertPlay = !0, Core.fullScrenMenuShow('<span id="prevHtml">正在收听 ' + About.topObj[About.topId][About.TITLE] + r + "年" + n + '<span class="spl"></span><a href="#" id="closeAbtTopPlay" class="clsFlwLstn overlay">退出</a></span>', "abtTop", About.topId), Player.music = new Array;
                var i = e.result[t].items;
                for (var s = 0; s < i.length; s++) {
                    var o = parseInt(Math.random() * i.length),
                        u = i[s];
                    i[s] = i[o], i[o] = u
                }
                for (var s = 0; s < i.length; ++s) Search.setMusic(i[s], s);
                Player.pos = -1, $("#playCtl").hasClass("pause") && $("#playCtl").click(), About.next()
            } else Core.fullScrenMenuHide(), Gns.openGnsArr = new Array, Gns.closeGns(), About.isTopInsertPlay = !1, Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.play(Player.cacheSec), Player.cacheMusic = null, Search.setSchVal(Search.keywords)
        },
        closeAbout: function () {
            About.currentMain == "abt" ? About.switching({
                ouid: About.ouid
            }) : About.currentMain == "top" && About.switching({
                ouid: About.topId
            })
        },
        startPt: function () {
            About.ptTms == 0 && About.isOpen && (About.ptTms = setInterval(function () {
                var e = parseInt($("#abtSec").text()) + 1,
                    t = parseInt($("#abtMin").text()),
                    n = parseInt($("#abtHour").text()),
                    r = !1,
                    i = !1;
                e == 60 && (r = !0, e = 0), r && (++t, t == 60 && (i = !0, t = 0)), i && ++n, $("#abtSec").text(e), r && $("#abtMin").parent().show(), $("#abtMin").text(t), i && $("#abtHour").parent().show(), $("#abtHour").text(n)
            }, 1e3))
        },
        stopPt: function () {
            clearTimeout(About.ptTms), About.ptTms = 0
        },
        tkrs: function (e) {
            if (Signup.userDetail.id == About.ouid) {
                _gaq.push(["_trackEvent", "About", "Tkrs", "About/Ticker"]);
                var t = '<div class="abtTabCtn"><a class="abtTab tkrsAll" href="#" data-mid="tkrsAll" data-no="0">大家听的</a><a class="abtTab tkrsMe" href="#" data-mid="tkrsMe" data-no="1">我收听的</a><a class="abtTab tkrsFav" href="#" data-mid="tkrsFav" data-no="2">我收藏的</a><div id="tabNowCrt" class="crt"></div><div id="tabWillCrt" class="crt"></div></div><div class="mscCtn"></div>';
                About.ctnHtml(t, "append", !1), $(".abtTabCtn>.tkrsAll").click()
            } else _gaq.push(["_trackEvent", "About", "Ppltkrs", "People/About/Tickers"]), $("#abtCttCtn").css("border-left", "1px solid #d6d6d6"), $.ajax({
                    url: Core.API_VER + "/personal/tickers",
                    data: {
                        uid: Signup.userDetail.id,
                        ouid: About.ouid,
                        st: About.st,
                        ps: About.PS
                    },
                    success: function (e) {
                        About.tkrsAction(e.result)
                    }
                })
        },
        loveSelected: function (e) {
            $.ajax({
                url: Core.API_VER + "/music/validate_favorites",
                data: {
                    uid: Signup.userDetail.id,
                    tids: e
                },
                success: function (e) {
                    if (!e.success) return;
                    var t = e.result;
                    for (var n = 0; n < t.length; ++n) $(".rctAbtCtn>a[data-tidId='l" + t[n] + "']").addClass("selected"), Core.ie68 || $(".rctAbtCtn>a[data-tidId='l" + t[n] + "']").css({
                            opacity: "1"
                        })
                }
            })
        },
        tkrsAction: function (e) {
            if (About.mid.indexOf("tkrs") == -1) return;
            var t = e.items,
                n = "";
            for (var r = 0; r < t.length; ++r) {
                $(".abtTabCtn").length == 1 ? About.ctn.children(".abtTabCtn").next().append(About.tkrsGnHtml(t[r], r)) : About.ctn.append(About.tkrsGnHtml(t[r], r));
                if (t[r].t == "C" || t[r].t == "D" || t[r].t == "S" || t[r].t == "L") $(".abtTabCtn").length == 1 ? Core.imgLoad(About.ctn.children(".abtTabCtn").next().children().last().find("img"), "", t[r].avt, 53) : Core.imgLoad(About.ctn.children().last().find("img"), "", t[r].avt, 53);
                t[r].t == "L" && (n += t[r].tid + ",")
            }
            n != "" && About.loveSelected(n.substring(0, n.length - 1)), t.length == 0 && (About.mid == "tkrsFav" ? About.ctnHtml(About.noCttHtml("收藏动态中喜欢的组合", "about"), "append") : About.ctnHtml(About.noCttHtml("", "about"), "append")), About.st += t.length, About.total = e.total, About.ctnHtml("", "append"), setTimeout(function () {
                About.isLoad = !1
            }, 500)
        },
        tkrsAll: function () {
            _gaq.push(["_trackEvent", "About", "AllTkrs", "About/Ticker/AllTickers"]), $.ajax({
                url: Core.API_VER + "/ticker/fetch_recents",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    About.tkrsAction(e.result)
                }
            }), Signup.tkrsCount = 0, $("#frdsTkrsCount").hide()
        },
        tkrsMe: function () {
            _gaq.push(["_trackEvent", "About", "MyTkrs", "About/Ticker/MyTickers"]), $.ajax({
                url: Core.API_VER + "/personal/tickers",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    About.tkrsAction(e.result)
                }
            })
        },
        tkrsFav: function () {
            _gaq.push(["_trackEvent", "About", "MyFav", "About/Ticker/MyFavoriteSearches"]), $.ajax({
                url: Core.API_VER + "/ticker/fetch_favs",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    About.tkrsAction(e.result)
                }
            })
        },
        tkrsGnHtml: function (e, t) {
            var n = "";
            if (e.t == "L") {
                var r = "",
                    i = "";
                Core.ie68 ? r = 'style="display:none"' : i = "dspr";
                var s = $.id2url(e.fid, "AT", "album");
                n = '<div class="rctAbtCtn love"><div class="imgs" style="overflow:hidden;position:relative;"><a href="#" class="sbj"></a><img src="' + IMG_URL + "/defaults/avatar/" + Core.A50 + '.jpg" width="53px" height="53px" class="bdg">' + "</div>" + '<div class="ctt">' + '<a data-fid="CmbtFlyBadge" data-span="%' + e.tit + '" class="tckrTit" href="#">' + e.tit + "</a>" + '<div class="actn"><a href="#" class="prflEvent" data-uid="' + e.uid + '" data-nick="' + e.nick + '">' + e.nick + "</a> " + Apps.offsetDate(e.ts) + "喜欢了这首歌</div>" + "</div>" + '<div class="trckCtn">' + '<a class="albm singlePlay" data-nick="' + e.nick + '" data-fid="' + e.fid + '" data-mid="' + e.mid + '" data-tid="' + e.tid + '" data-d="' + e.d + '" data-name="' + e.tit + '" data-atn="' + e.atn + '" data-an="' + e.an + '" style="background:#e1e1e1 url(' + s + ') no-repeat; background-size:50px" href="#">' + '<div class="tckrPlyCtl"></div>' + "</a>" + '<div class="trckTit">' + e.tit + "</div>" + '<a class="tkrsFlyEvent" data-fid="CmbtFlyBadge" data-span="' + e.atn + '" href="#">' + e.atn + "</a>" + "</div>" + '<a data-uid="' + e.uid + '" data-tidId="l' + e.tid + '" ' + r + ' href="#" class="favIcon love tkrsLoveEvent ' + i + '" data-tid="' + e.tid + '"></a>' + "</div>"
            } else if (e.t == "K") {
                var s = $.id2url(e.fid, "AT", "album");
                n = '<div class="rctAbtCtn sml"><div class="imgs"><a data-fid="' + e.fid + '" data-span="' + e.tit + '" class="sbj tkrsFlyEvent" style="background:#e1e1e1 url(' + s + ') no-repeat;background-size:50px;" href="#"></a>' + "</div>" + '<div class="ctt">' + '<a data-fid="' + e.fid + '" data-span="' + e.tit + '" class="tckrTit tkrsFlyEvent" href="#">相似：' + e.tit + "</a>" + '<div class="actn"><a href="#" data-uid="' + e.uid + '" data-nick="' + e.nick + '">' + e.nick + "</a> " + Apps.offsetDate(e.ts) + "听了相似歌曲</div>" + "</div>" + "</div>"
            } else if (e.t == "C") {
                var o = "",
                    i = "",
                    r = "";
                About.mid == "tkrsFav" && (About.favObj[e.uid + e.tit] = !0), Core.ie68 ? r = 'style="display:none"' : i = "dspr", About.favObj[e.uid + e.tit] && (o = "selected", r = "", i = ""), n = '<div class="rctAbtCtn cmbt"><div class="imgs" style="overflow:hidden;position:relative;"><a data-fid="CmbtFlyBadge" data-span="' + e.tit + '" href="#" class="sbj tkrsFlyEvent"></a>' + '<img src="' + IMG_URL + "/defaults/avatar/" + Core.A50 + '.jpg" width="53px" height="53px" class="bdg">' + "</div>" + '<div class="ctt">' + '<a href="#" class="tckrTit tkrsFlyEvent" data-fid="CmbtFlyBadge" data-span="' + e.tit + '">' + e.tit + "</a>" + '<div class="actn"><a href="#" class="prflEvent" data-uid="' + e.uid + '" data-nick="' + e.nick + '">' + e.nick + "</a> " + Apps.offsetDate(e.ts) + "使用了组合</div>" + "</div>" + '<a href="#" ' + r + ' class="favIcon tkrsFavEvent ' + o + " " + i + '" data-ouid="' + e.uid + '" data-tit="' + e.tit + '" data-t="' + e.t + '" data-fid="' + e.fid + '"></a>' + "</div>"
            } else if (e.t == "D") {
                var u = "",
                    a = e.fid,
                    f = "",
                    o = "",
                    i = "",
                    r = "";
                About.mid == "tkrsFav" &&
                    (About.favObj[e.uid + e.tit] = !0), Core.ie68 ? r = 'style="display:none"' : i = "dspr", About.favObj[e.uid + e.tit] && (o = "selected", r = "", i = ""), e.fid == "" ? (a = "CmbtFlyBadge", u = '<img class="bdg" src="' + IMG_URL + "/defaults/avatar/" + Core.A50 + '.jpg" width="53px" height="53px" />') : e.fid.indexOf(".") >= 0 ? (u = '<div class="bdg" style="background-image: url(' + $.id2url(e.fid, "ST", "artist") + ');"></div>', f = '<a href="#" ' + r + ' class="favIcon tkrsFavEvent ' + o + " " + i + '" data-ouid="' + e.uid + '" data-tit="' + e.tit + '" data-t="' + e.t + '" data-fid="' + e.fid + '"></a>') : u = '<div class="bdg" style="background-image: url(' + Core.badgesUrl(e.fid, 50) + ');"></div>', n = '<div class="rctAbtCtn uprd"><div class="imgs" style="overflow: hidden; position: relative;"><a data-fid="' + a + '" data-span="' + e.tit + '" href="#" class="sbj tkrsFlyEvent"></a>' + u + "</div>" + '<div class="ctt">' + '<a data-fid="' + a + '" data-span="' + e.tit + '" href="#" class="tckrTit tkrsFlyEvent">听了：' + e.tit + "</a>" + '<div class="actn"><a href="#" class="prflEvent" data-uid="' + e.uid + '" data-nick="' + e.nick + '">' + e.nick + "</a> " + Apps.offsetDate(e.ts) + "听了</div>" + "</div>" + f + "</div>"
            } else if (e.t == "S") {
                var u = "",
                    a = e.fid;
                e.fid == "" ? (a = "CmbtFlyBadge", u = '<img class="bdg" src="' + IMG_URL + "/defaults/avatar/" + Core.A50 + '.jpg" width="53px" height="53px" />') : e.fid.indexOf(".") >= 0 ? u = '<div class="bdg" style="background-image: url(' + $.id2url(e.fid, "ST", "artist") + ');"></div>' : u = '<div class="bdg" style="background-image: url(' + Core.badgesUrl(e.fid, 50) + ');"></div>', n = '<div class="rctAbtCtn uprd"><div class="imgs" style="overflow: hidden; position: relative;"><a data-fid="' + a + '" data-span="' + e.tit + '" href="#" class="sbj tkrsFlyEvent"></a>' + u + "</div>" + '<div class="ctt">' + '<a data-fid="' + a + '" data-span="' + e.tit + '" href="#" class="tckrTit tkrsFlyEvent">说了：' + e.tit + "</a>" + '<div class="actn"><a href="#" class="prflEvent" data-uid="' + e.uid + '" data-nick="' + e.nick + '">' + e.nick + "</a> " + Apps.offsetDate(e.ts) + "说了</div>" + "</div>" + "</div>"
            }
            return n
        },
        msc: function () {
            if (Signup.userDetail.id == About.ouid) {
                _gaq.push(["_trackEvent", "About", "Msc", "MusicMenu"]);
                var e = '<div class="abtTabCtn"><a class="abtTab love" href="#" data-mid="love" data-no="0">喜欢的</a><a class="abtTab hate" href="#" data-mid="hate" data-no="1">讨厌的</a><a class="abtTab moods" href="#" data-mid="moods" data-no="2">情绪电台</a><div id="tabNowCrt" class="crt"></div><div id="tabWillCrt" class="crt"></div></div><div class="mscCtn"></div>'
            } else {
                _gaq.push(["_trackEvent", "About", "Pplmsc", "People/About/Music"]);
                var e = '<div class="abtTabCtn prfl"><a class="abtTab love" href="#" data-mid="love" data-no="0">喜欢的</a><a class="abtTab moods" href="#" data-mid="moods" data-no="1">情绪电台</a><div id="tabNowCrt" class="crt" style="width:190px"></div><div id="tabWillCrt" class="crt" style="width:190px"></div></div><div class="mscCtn"></div>'
            }
            About.ctnHtml(e, "append", !1), $(".abtTabCtn>.love").click()
        },
        love: function () {
            Signup.userDetail.id == About.ouid ? _gaq.push(["_trackEvent", "About", "Love", "About/Music/LovedSongs"]) : _gaq.push(["_trackEvent", "About", "Ppllove", "People/About/Music/LovedSongs"]), $.ajax({
                url: Core.API_VER + "/music/fetch_favorites",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    if (About.mid != "love") return;
                    var t = e.result.items,
                        n = "",
                        r = "";
                    Signup.userDetail.id == About.ouid && (r = '<a class="loveBtn" href="#"></a>');
                    for (var i = 0; i < t.length; ++i) n += '<div id="' + t[i].id + '" class="rctAbtCtn love">' + '<div  style="background-image:url(' + $.id2url(t[i].fid, "AT", "album") + ')" class="cv"></div>' + '<p class="tit">' + t[i].tit + "</p>" + "<span>" + t[i].al + "&nbsp;&nbsp;" + About.artistStr(t[i]) + "&nbsp;&nbsp;" + Apps.offsetDate(t[i].ts) + "</span>" + r + "</div>";
                    t.length == 0 && (n = About.noCttHtml("", "about")), About.st += t.length, About.total = e.result.total, About.ctnHtml(n, "append"), setTimeout(function () {
                        About.isLoad = !1
                    }, 500)
                }
            })
        },
        hate: function () {
            _gaq.push(["_trackEvent", "About", "Hate", "About/Music/HatedSongs"]), $.ajax({
                url: Core.API_VER + "/music/fetch_hates",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    if (About.mid != "hate") return;
                    var t = e.result.items,
                        n = "",
                        r = "";
                    Signup.userDetail.id == About.ouid && (r = '<a class="hateBtn" href="#"></a>');
                    for (var i = 0; i < t.length; ++i) n += '<div id="' + t[i].id + '" class="rctAbtCtn hate">' + '<div style="background-image:url(' + $.id2url(t[i].fid, "AT", "album") + ')" class="cv"></div>' + '<p class="tit">' + t[i].tit + "</p>" + "<span>" + t[i].al + "&nbsp;&nbsp;" + About.artistStr(t[i]) + "&nbsp;&nbsp;" + Apps.offsetDate(t[i].ts) + "</span>" + r + "</div>";
                    t.length == 0 && (n = About.noCttHtml("", "about")), About.st += t.length, About.total = e.result.total, About.ctnHtml(n, "append"), setTimeout(function () {
                        About.isLoad = !1
                    }, 500)
                }
            })
        },
        moods: function () {
            Signup.userDetail.id == About.ouid ? _gaq.push(["_trackEvent", "About", "Moods", "About/Music/Moods"]) : _gaq.push(["_trackEvent", "About", "Pplmoods", "People/About/Music/Moods"]), $.ajax({
                url: Core.API_VER + "/personal/moods",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    if (About.mid != "moods") return;
                    var t = e.result.items,
                        n = "";
                    for (var r = 0; r < t.length; ++r) {
                        var i = "",
                            s = "";
                        About.moodText == t[r].n && (s = "pause", i = "on"), n += '<div class="rctAbtCtn moods"><div class="imgs"><a data-fid="' + t[r].fid + '" data-span="@' + $("#aboutNick").text() + " + " + t[r].n + '" class="sbj tkrsFlyEvent" href="#"></a>' + '<div class="bdg" style="background-image:url(' + Core.badgesUrl(t[r].fid, 50) + ');"></div>' + "</div>" + '<div class="wvTbl">' + '<div class="wv ' + i + '"></div>' + "</div>" + '<a href="#" class="mood moodEvent ' + s + '">' + t[r].n + "</a>" + "</div>"
                    }
                    t.length == 0 && (n = About.noCttHtml("", "about")), About.st += t.length, About.total = e.result.total, About.ctnHtml(n, "append"), $(".moodEvent").each(function () {
                        if ($(this).text() == About.moodText) return About.moodObj = $(this), !1
                    }), setTimeout(function () {
                        About.isLoad = !1
                    }, 500)
                }
            })
        },
        moodClick: function () {
            $(this).hasClass("pause") ? About.closeMoodPlay() : (About.moodObj == undefined ? About.isInsertPlay = !0 : (About.moodObj.removeClass("pause"), About.moodObj.prev().children().removeClass("on")), $(this).addClass("pause"), $(this).prev().children().addClass("on"), About.moodObj = $(this), About.moodText = $(this).text(), Core.fullScrenMenuShow('<span id="prevHtml">正在收听 ' + $("#aboutNick").text() + "的" + $(this).text() + '心情电台<span class="spl"></span><a href="#" id="closeMoodPlay" class="clsFlwLstn overlay">退出</a></span>', "mood"), $.ajax({
                url: Core.API_VER + "/search/jing/fetch_pls",
                data: {
                    q: "@" + $("#aboutNick").text() + " + " + $(this).text(),
                    ps: 100,
                    st: "0",
                    u: Signup.userDetail.id,
                    ss: About.ss
                },
                success: function (e) {
                    Player.music = new Array;
                    for (var t = 0; t < e.result.items.length; ++t) Search.setMusic(e.result.items[t], t);
                    Player.pos = -1, $("#playCtl").hasClass("pause") && $("#playCtl").click(), About.next()
                }
            }))
        },
        closeSinglePlay: function () {
            Core.fullScrenMenuHide(), About.isSingleInsertPlay = !1, Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.cacheMusic = null;
            var e = function () {
                Player.isVolumeDown ? setTimeout(e, 200) : Player.play(Player.cacheSec)
            };
            e()
        },
        closeMoodPlay: function () {
            About.moodObj != undefined && (About.moodObj.removeClass("pause"), About.moodObj.prev().children().removeClass("on")), Core.fullScrenMenuHide(), About.isInsertPlay = !1, About.moodObj = undefined, About.moodText = "", Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.cacheMusic = null;
            var e = function () {
                Player.isVolumeDown ? setTimeout(e, 200) : Player.play(Player.cacheSec)
            };
            e()
        },
        closeAtPlay: function () {
            $("#atNickPlay").removeClass("pause"), Core.fullScrenMenuHide(), About.isAtInsertPlay = !1, About.atInsertPlayNick = "", Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.cacheMusic = null;
            var e = function () {
                Player.isVolumeDown ? setTimeout(e, 200) : Player.play(Player.cacheSec)
            };
            e()
        },
        next: function () {
            $("#playCtl").removeClass("play").addClass("pause"), Player.addLoading(), Player.closeGns(), $("#playerRptOne").hasClass("selected") || ++Player.pos;
            var e = function () {
                if (Player.isVolumeDown) setTimeout(e, 200);
                else {
                    if (Player.pos >= Player.music.length) {
                        if (About.isSingleInsertPlay) {
                            $("#closeSinglePlay").click();
                            return
                        }
                        Player.pos = 0
                    }
                    Player.play()
                }
            };
            e()
        },
        artistStr: function (e) {
            var t = e.at;
            if (e.feat != "") {
                t == "Various Artists" ? t = "" : t += " & ";
                var n = e.feat.split(",");
                for (var r = 0; r < n.length; ++r) t += n[r] + " & ";
                t = t.substring(0, t.length - 3)
            }
            return t
        },
        loveBtnClick: function () {
            $(this).addClass("selected");
            var e = $(this).parent().prop("id");
            $.ajax({
                url: Core.API_VER + "/music/post_love_song",
                data: {
                    uid: Signup.userDetail.id,
                    tid: e
                }
            }), Player.pos >= 0 && e == Player.music[Player.pos].tid && $("#playerLove").removeClass("selected"), $(this).parent().animate({
                height: "0px"
            }, 300, function () {
                $(this).remove(), About.scrollApi.reinitialise()
            }), --About.st, --About.total
        },
        hateBtnClick: function () {
            $(this).addClass("selected");
            var e = $(this).parent().prop("id");
            $.ajax({
                url: Core.API_VER + "/music/post_hate_song",
                data: {
                    uid: Signup.userDetail.id,
                    tid: e
                }
            }), $(this).parent().animate({
                height: "0px"
            }, 300, function () {
                $(this).remove(), About.scrollApi.reinitialise()
            }), --About.st, --About.total
        },
        frds: function () {
            if (Signup.userDetail.id == About.ouid) {
                _gaq.push(["_trackEvent", "About", "Frds", "FriendsMenu"]), $("#frdsChtCount").hide();
                var e = "hide",
                    t = 0;
                Cht.offlineMes[0] != undefined && (e = "", t = Cht.offlineMes[0].count);
                var n = '<div class="abtTabCtn"><a class="abtTab rct" href="#" data-mid="rct" data-no="0">最近操作</a><a class="abtTab ext" href="#" data-mid="ext" data-no="1">扩展好友</a><a class="abtTab ntft" href="#" data-mid="ntft" data-no="2">消息中心</a><em id="sysChtCount" style="left: 350px; top: 12px;" class="num serif tkrs ' + e + ' overlay">' + t + "</em>" + '<div id="tabNowCrt" class="crt"></div>' + '<div id="tabWillCrt" class="crt"></div>' + '</div><div class="frdCt lstCtn"></div>';
                About.ctnHtml(n, "append", !1), Frd.olFrdObj != "" ? (Frd.olFrdObj.uid == 0 ? ($(".abtTabCtn>.ntft").click(), Frd.olFrdObj = "") : Frd.olFrdObj.uid == "ext" ? ($(".abtTabCtn>.ext").click(), Frd.olFrdObj = "") : $(".abtTabCtn>.rct").click(), $(".abtTabCtn").mouseleave()) : $(".abtTabCtn>.rct").click()
            } else _gaq.push(["_trackEvent", "About", "Pplfrds", "People/About/Friends"]), About.st == 0 && About.ctnHtml('<div class="abtTabCtn hide"></div><div class="frdCt lstCtn"></div>', "append", !1), $.ajax({
                    url: Core.API_VER + "/account/fetch_friends",
                    data: {
                        uid: Signup.userDetail.id,
                        ouid: About.ouid,
                        st: About.st,
                        ps: About.PS
                    },
                    success: function (e) {
                        Frd.mid = "sch";
                        var t = e.result.items;
                        About.st += t.length, About.total = e.result.total, t.length == 0 ? About.ctnHtml(About.noCttHtml("", "about"), "append") : About.ctnHtml(Frd.frdHtml(t), "append"), Frd.replaceAvatar(), setTimeout(function () {
                            About.isLoad = !1
                        }, 500)
                    }
                })
        },
        rct: function () {
            _gaq.push(["_trackEvent", "About", "Rct", "About/Friends/Recents"]);
            var e = function (e) {
                About.hostIndex = e.result.index;
                var t = e.result.items;
                Frd.mid = "inHs", About.st += t.length, About.total = e.result.total;
                if (Frd.olFrdObj != "") {
                    var n = !0;
                    for (var r = 0; r < t.length; ++r) if (t[r].uid == Frd.olFrdObj.uid) {
                            n = !1;
                            break
                        }
                    n && (t = (new Array(Frd.olFrdObj)).concat(t))
                }
                t.length == 0 ? About.ctnHtml(About.noCttHtml("", "about"), "append") : About.ctnHtml(Frd.frdHtml(t), "append"), Frd.replaceAvatar();
                if (Frd.olFrdObj != "") {
                    var i = $(".chtEvent[data-uid='" + Frd.olFrdObj.uid + "']");
                    setTimeout(function () {
                        About.scrollApi.scrollToY(i.parents(".frdEvent").position().top - About.OFFSET_TOP), i[0].click()
                    }, 300), Frd.olFrdObj = ""
                }
                setTimeout(function () {
                    About.isLoad = !1
                }, 500)
            };
            Frd.olFrdObj.uid == "" && (Frd.olFrdObj = ""), About.hostIndex == undefined || About.st == 0 ? $.ajax({
                url: Core.API_VER + "/account/fetch_friends_order",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS
                },
                success: e
            }) : $.ajax({
                url: Core.API_VER + "/account/fetch_friends_order",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid,
                    st: About.st,
                    ps: About.PS,
                    index: About.hostIndex
                },
                success: e
            })
        },
        ext: function () {
            _gaq.push(["_trackEvent", "About", "Ext", "About/Friends/ExtendFriends"]);
            var e = resData.ext,
                t = '<div class="rctCtn fm" data-mid="sch"><div class="fld link"><input id="mtfrdsNick" name="link" class="input" value="输入好友的昵称" onblur="if(this.value == \'\')this.value=\'输入好友的昵称\'" onfocus="if(this.value == \'输入好友的昵称\')this.value = \'\'"><a id="schNick" href="#" class="btn"><span class="btnLt"></span>搜索好友<span class="btnRt"></span></a></div></div>';
            for (var n = 0; n < e.length; ++n) t += Frd.iconHtml("ext", e[n]);
            About.ctnHtml(t, "append"), About.isLoad = !1
        },
        ntft: function () {
            _gaq.push(["_trackEvent", "About", "Ntft", "About/Friends/Notifications"]), Cht.offlineMes[0] != undefined && (Cht.offlineCount -= Cht.offlineMes[0].count, delete Cht.offlineMes[0]), $("#sysChtCount").hide(), $.ajax({
                url: Core.API_VER + "/chat/fetch_chatctt",
                data: {
                    uid: Signup.userDetail.id,
                    fuid: "0",
                    st: About.st,
                    ps: About.PS
                },
                success: function (e) {
                    var t = e.result.items;
                    About.st += t.length, About.total = e.result.total;
                    var n = "";
                    for (var r = 0; r < t.length; ++r) n += Cht.sysHtml(t[r]);
                    t.length == 0 && (n = About.noCttHtml("", "about")), $("#abtCttCtn>.frdCt").children().last().children(".sysMsgBd").css({
                        "margin-bottom": "10px",
                        "border-bottom": "1px solid #D8D9D1"
                    }), About.ctnHtml(n, "append"), $("#abtCttCtn>.frdCt").children().last().children(".sysMsgBd").css({
                        "margin-bottom": "0px",
                        "border-bottom": "0px"
                    }), Frd.replaceAvatar(), setTimeout(function () {
                        About.isLoad = !1
                    }, 500)
                }
            })
        },
        stgs: function () {
            $("#stgsNewIcon").hide(), $.ajax({
                url: Core.API_VER + "/personal/settings",
                data: {
                    uid: Signup.userDetail.id,
                    ouid: About.ouid
                },
                success: function (e) {
                    Signup.userDetail.id == About.ouid ? About.setting(e) : About.xFile(e), setTimeout(function () {
                        About.isLoad = !1
                    }, 500)
                }
            })
        },
        xFile: function (e) {
            _gaq.push(["_trackEvent", "About", "Profile", "People/About/Profile"]), $("#abtCttCtn").css("border-left", "1px solid #d6d6d6");
            var t = e.result.userinfo,
                n = t.nickname == null ? "" : t.nickname,
                r = t.birthday == null ? "" : t.birthday,
                i = r.split("-")[0] == undefined ? "1970" : r.split("-")[0],
                s = r.split("-")[1] == undefined ? "01" : r.split("-")[1],
                o = r.split("-")[2] == undefined ? "01" : r.split("-")[2],
                u = t.permalink == null ? "" : t.permalink,
                a = "";
            for (var f = 0; f < e.result.identifys.length; ++f) a += '<i class="abtScl ' + ConverSns[e.result.identifys[f]] + '"></i>';
            u == "" && (u = t.id), u = '<a href="http://jing.fm/' + u + '" target="_blank" class="extLk">jing.fm/' + u + "</a>";
            var l = t.bio == null ? "" : t.bio;
            l == "" && (l = "这个人很懒，什么也没留下！");
            var c = t.sex == null ? "秘密" : t.sex;
            if (r == "") r = "秘密";
            else {
                var h = new Date,
                    r = h.getYear() + 1900 - i;
                s > h.getMonth() + 1 ? --r : o > h.getDate() && --r
            }
            var p = "",
                d = [];
            d[0] = ["昵称", n], d[1] = ["ID", u], d[2] = ["社交", a], d[3] = ["性别", c], d[4] = ["年龄", r], d[5] = ["签名", l];
            for (var f = 0; f < d.length; ++f) {
                if (f == 2 && d[f][1] == "") continue;
                f == 5 ? p += '<div class="rctAbtCtn bioCtn"><h4><span>' + d[f][0] + '</span><p class="bio">' + d[f][1] + "</p></h4>" + "</div>" : p += '<div class="rctAbtCtn"><h4><span>' + d[f][0] + "</span>" + d[f][1] + "</h4>" + "</div>"
            }
            p += '<div class="rctAbtCtn abtIll"></div>', About.ctnHtml(p, "append")
        },
        setting: function (response) {
            _gaq.push(["_trackEvent", "About", "Stgs", "SettingsMenu"]);
            var settingHtml = '<div class="stgCbn"><div class="rctAbtCtn stgHd"><div class="stIcon info"></div><p class="ctn">个人设置</p></div>';
            for (var key in StPrfl) {
                var newIcon = "";
                key == "thm" && Signup.userDetail.newview.t.length != 0 && (newIcon = '<em id="thmNewIcon" class="newApp" style="left: 54px;top:5px"></em>'), settingHtml += '<a id="' + key + '" href="#" class="rctAbtCtn stg extEvent">' + newIcon + '<p class="ctn">' + StPrfl[key][0] + "</p>" + '<div class="extSt"></div>' + "</a>"
            }
            settingHtml += "</div>";
            var setting = response.result.setting;
            for (var key in St) {
                var html = "";
                if (key.indexOf("Des") >= 0) continue;
                for (var childkey in St[key]) {
                    var off = "",
                        mid = childkey;
                    if (setting[childkey] == undefined && childkey != "ss") {
                        if (setting[ConverSns[childkey + "1"]] == undefined) continue;
                        setting[ConverSns[childkey + "1"]] == "false" && (off = "off"), mid = ConverSns[childkey + "1"]
                    }
                    setting[childkey] == "false" && (off = "off"), childkey == "ss" && !Search.ss && (off = "off"), html += '<div class="rctAbtCtn stg"><p class="ctn">' + St[key][childkey] + "</p>" + '<a href="#" data-mid="' + mid + '" class="swtch ' + off + ' swtchEvent"></a>' + "</div>"
                }
                if (html == "") continue;
                var styleMargin = "";
                key == "dsply" && (styleMargin = 'style="margin-bottom:0px; border-bottom:0px"'), html = '<div class="stgCbn" ' + styleMargin + ">" + '<div class="rctAbtCtn stgHd">' + '<div class="stIcon ' + key + '"></div>' + '<p class="ctn">' + St[key + "Des"] + "</p>" + "</div>" + html + "</div>", settingHtml += html
            }
            About.ctnHtml(settingHtml, "append"), $(".extEvent").click(function () {
                if (About.isAnimate) return;
                $("#thmNewIcon").hide(), About.isAnimate = !0, $children = $(this).children(".extSt"), $(this).hasClass("on") ? ($children.removeClass("on"), $(this).removeClass("on"), $(this).next().animate({
                    height: "0px"
                }, 300, function () {
                    $(this).remove(), About.isAnimate = !1, About.scrollApi.reinitialise()
                })) : ($children.addClass("on"), $(this).addClass("on"), About.stId = $(this).prop("id"), eval("About." + About.stId)($(this)))
            }), $(".swtchEvent").click(function () {
                var e, t = $(this).data("mid");
                $(this).hasClass("off") ? ($(this).removeClass("off"), e = "true", ConverSns[t] != undefined && Core.getCookie("snsStKnow") != "true" && Gns.nowGns('开启之后会自动同步你喜欢、搜索和解锁的操作，<a href="#" class="trg knowEvent" data-cookieid="snsStKnow">我知道了</a>。', Gns.level1)) : ($(this).addClass("off"), e = "false");
                if (t == "ss") {
                    $(this).hasClass("off") ? (Search.ss = !1, Search.st = 99999999, Gns.nowGns("你已经关闭了智能引导，现在可以收听艺人全部的歌曲了")) : Search.ss = !0;
                    return
                }
                Signup.userDetail.sts[t] = e, t == "rtCv" ? Signup.userDetail.sts.rtCv == "true" ? Player.startRotate() : (Player.stopRotate(), Player.setRotate()) : t == "hbr" ? (Player.hbr = e, Player.play(Player.currentTime)) : t == "lgA" && (Signup.userDetail.sts.lgA == "true" ? $("html").addClass("ftlg") : $("html").removeClass("ftlg")), $.ajax({
                    url: Core.API_VER + "/setting/post_settings",
                    data: {
                        uid: Signup.userDetail.id,
                        n: t,
                        v: e
                    }
                })
            })
        },
        info: function (e) {
            $.ajax({
                url: Core.API_VER + "/account/fetch_profile",
                data: {
                    uid: Signup.userDetail.id
                },
                success: function (t) {
                    About.isAnimate = !1;
                    var n = t.result,
                        r = n.nickname == null ? "" : n.nickname,
                        i = n.birthday == null ? "" : n.birthday,
                        s = i.split("-")[0] == undefined ? "1970" : i.split("-")[0],
                        o = i.split("-")[1] == undefined ? "01" : i.split("-")[1],
                        u = i.split("-")[2] == undefined ? "01" : i.split("-")[2],
                        a = n.permalink == null ? "" : n.permalink;
                    a == "" && (a = n.id);
                    var f = n.bio == null ? "" : n.bio;
                    f == "" && (f = "这个人很懒，什么也没留下！");
                    var l = n.sex == null ? "" : n.sex,
                        c = "",
                        h = "";
                    l == "男" ? c = "checked='checked'" : h = "checked='checked'";
                    var p = '<input value="' + r + '" name="nick" class="input nick" id="nick">' + '<div id="pmlnk" class="pmlnk">jing.fm/' + a + "</div>" + '<div class="fld">' + '<div class="radio"><input type="radio" value="男" ' + c + ' name="sex">男</div>' + '<div class="radio"><input type="radio" value="女" ' + h + ' name="sex">女</div>' + "</div>" + '<div class="fld">' + '<div class="select">' + '<select name="year" id="year"></select><span>年</span>' + "</div>" + '<div class="select">' + '<select name="mouth" id="mouth"></select><span>月</span>' + "</div>" + '<div class="select">' + '<select name="day" id="day"></select><span>日</span>' + "</div>" + "</div>" + "<input onfocus=\"if(this.value == '这个人很懒，什么也没留下！')this.value = ''\" onblur=\"if(this.value == '')this.value='这个人很懒，什么也没留下！'\" value=\"" + f + '" class="input bio" id="bio">';
                    p = '<div class="rctAbtCtn form ' + About.stId + '"><div class="fldCtn">' + p + "</div></div>", About.afterHtml(e, p), About.date(s, o, u);
                    var d = Core.API_VER + "/account/update_profile?uid=" + Signup.userDetail.id;
                    $("#nick").keydown(function (e) {
                        e.stopPropagation()
                    }).focus(function () {
                        About.nick = Core.inputConver($(this).val())
                    }).blur(function () {
                        var e = Core.inputConver($(this).val());
                        if (About.nick == e) return;
                        var t = $(this).offset().left,
                            n = $(this).offset().top - 32;
                        /^[\u4E00-\u9FA5A-Za-z0-9_]+$/.test(e) ? Core.strLength(e) < 2 || Core.strLength(e) > 14 ? (Signup.tpsShow($(this), "昵称只能2-14个字符"), $(this).data("error", "昵称只能2-14个字符")) : e.length == 0 ? ($("#tps").hide(), Signup.tpsShow($(this), "不能为空"), $(this).data("error", "不能为空")) : ($("#tps").hide(), $(this).data("error", ""), $.ajax({
                            url: d,
                            data: {
                                nick: e
                            },
                            success: function (t) {
                                t.success ? ($("#nick").data("error", ""), Signup.userDetail.nick = e, $("#aboutNick").text(e)) : t.code == "253" ? (Signup.tpsShow($("#nick"), "昵称重复"), $("#nick").data("error", "不能为空")) : alert(t.codemsg)
                            }
                        })) : (Signup.tpsShow($(this), '支持中英文、数字、"_"'), $(this).data("error", '支持中英文、数字、"_"'))
                    }), About.pmlnkBtnHtml(a), a == Signup.userDetail.id && $("#pmlnk").bind("click", function () {
                        if ($("#pmlnkOK").length == 1) return;
                        if ($("#editPmlnk").length == 1 || About.pmlnk == 1) {
                            About.pmlnk = !1;
                            return
                        }
                        $(this).html("jing.fm/"), $(this).append('<input id="editPmlnk" type="text" class="overlay editPmlnk" value="' + a + '" />'), $("#editPmlnk").focus(), $("#editPmlnk").keydown(function (e) {
                            e.keyCode == 13 && $(this).blur()
                        }).click(function () {
                            return !1
                        }).blur(function () {
                            var e = $(this).val();
                            if (e == a) About.pmlnk = !0, $("#pmlnk").html("jing.fm/" + a), About.pmlnkBtnHtml(a);
                            else {
                                if (!/[^(0-9\s\-\_)+][A-Za-z0-9\-\_]+$/i.test(e)) {
                                    Signup.tpsShow($(this), '支持英文数字、"_-"');
                                    return
                                }
                                if (e.length < 4 || e.length > 16) {
                                    Signup.tpsShow($(this), "不能少于4个，不能超过16个");
                                    return
                                }
                                $("#tps").hide(), $this = $(this), $.ajax({
                                    url: Core.API_VER + "/account/check_permalink",
                                    data: {
                                        permalink: e,
                                        oldpermalink: a
                                    },
                                    success: function (t) {
                                        t.success ? Gns.nowGns("个人主页永久链接设置后不能修改，确定要修改为：http://jing.fm/" + e + ' 吗？ <a id="pmlnkOK" class="trg" href="#" data-text="' + e + '">确定</a> | <a id="pmlnkNO" class="trg" href="#" data-text="' + a + '">取消</a>', Gns.level1) : t.code == 263 ? Signup.tpsShow($this, "永久链接已被使用") : t.code == 264 ? Signup.tpsShow($this, "不能少于4个，不能超过16个") : Gns.nowGns(t.codemsg)
                                    }
                                })
                            }
                        })
                    }), $("#bio").blur(function () {
                        $.ajax({
                            url: d,
                            data: {
                                bio: Core.inputConver($(this).val())
                            }
                        })
                    }).keydown(function (e) {
                        e.stopPropagation()
                    }), $("input[name=sex]").click(function () {
                        $.ajax({
                            url: d,
                            data: {
                                sex: $(this).val()
                            }
                        })
                    }), $("#year, #mouth, #day").change(function () {
                        var e = $("#year").val() + "-" + $("#mouth").val() + "-" + $("#day").val();
                        $.ajax({
                            url: d,
                            data: {
                                bday: e
                            }
                        })
                    })
                }
            })
        },
        pmlnkBtnHtml: function (e) {
            var t = "http://jing.fm/" + e;
            Core.ie && (t = ""), $("#pmlnk").append('<a id="pmlnkShare" href="#" class="share">' + t + '</a><a id="pmlnkVisit" data-pmlnk="jing.fm/' + e + '" href="#" class="visit">查看</a>'), Core.ie || $("#pmlnkShare").clippy(), $("#pmlnkShare").append("分享"), $("#pmlnkVisit").click(function () {
                return window.open("http://" + $(this).data("pmlnk")), !1
            }), $("#pmlnkShare").click(function () {
                var e = $("#pmlnkVisit").data("pmlnk");
                return Core.ie ? window.clipboardData.setData("Text", e) : $(this).prev().clippy({
                    text: e
                }), !1
            })
        },
        date: function (e, t, n) {
            e == "" && (e = 1985);
            var r = new Date,
                i = r.getYear() + 1900;
            Core.ie68 && (i = r.getYear());
            var s = "",
                o = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
            i % 4 == 0 && (o[1] = 29);
            for (var u = 80; u >= 10; --u) {
                var a = "";
                i - u == Number(e) && (a = "selected"), s += "<option value='" + (i - u).toString() + "' " + a + ">" + (i - u).toString() + "</option>"
            }
            $("#year").html(s), s = "";
            for (var u = 1; u < 13; ++u) {
                var a = "",
                    f = "";
                u < 10 ? f = "0" + u : f = u + "", f == t && (a = "selected"), s += "<option value='" + f + "' " + a + ">" + f + "</option>"
            }
            $("#mouth").html(s), s = "", nowMouth = Number(t.replace("0", ""));
            for (var u = 1; u < o[nowMouth - 1] + 1; ++u) {
                var a = "",
                    l = "";
                u < 10 ? l = "0" + u : l = u + "", l == n && (a = "selected"), s += "<option value='" + l + "' " + a + ">" + l + "</option>"
            }
            $("#day").html(s)
        },
        avatar: function (e) {
            $.ajax({
                url: Core.API_VER + "/account/fetch_avatar",
                data: {
                    uid: Signup.userDetail.id
                },
                success: function (t) {
                    About.isAnimate = !1;
                    var n = t.result.items,
                        r = [];
                    for (var i = 0, s = 1; i < n.length; ++i) n[i].type == "Local" ? r[0] = n[i] : (r[s] = n[i], ++s);
                    n = r;
                    var o = "";
                    Core.ipad ? o = '<div id="uploadify" class="uploadify" style="height: 52px; width: 252px;"><div class="uploadify-button"><span class="uploadify-button-text">iPad 不支持上传头像</span></div></div>' : swfobject.ua.pv[0] ? o = '<input id="uploadify" name="file" type="file">' : o = '<div id="uploadify" class="uploadify" style="height: 52px; width: 252px;"><div class="uploadify-button"><span class="uploadify-button-text">没装flash，不能上传头像</span></div></div>';
                    var u = "";
                    for (var i = 0; i < n.length; ++i) {
                        var a = "",
                            f = n[i].url,
                            l = "";
                        n[i].type == "Qq" ? a = "腾讯微博" : n[i].type == "Sina" ? a = "新浪微博" : n[i].type == "Renren" ? a = "人人网" : n[i].type == "Douban" ? a = "豆瓣网" : n[i].type == "Gr" ? a = "Gravatar.com" : a = "本地上传", n[i].using && (l = "selected"), u += '<div class="fldCtn ' + n[i].type.toLowerCase() + '">' + '<div class="imgs">' + '<a href="#" class="sbj"></a>' + '<div class="bdg" style="overflow:hidden;">' + '<img id="avt-' + i + '" src="' + IMG_URL + "/defaults/avatar/" + Core.A64 + '.jpg" width="50px" height="50px" style="position:absolute;" />' + "</div>" + "</div>" + '<div class="imgCtl">' + a + '<a data-url="' + n[i].url + '" data-type="' + n[i].type + '" class="chc avtSwitch ' + l + '" href="#"></a></div>' + "</div>"
                    }
                    u += '<div class="fldCtn upload">' + o + "</div>", u = '<div class="rctAbtCtn form ' + About.stId + '">' + u + "</div>", About.afterHtml(e, u, n.length * 74 + 114), Core.uploadify();
                    for (var i = 0; i < n.length; ++i) Core.imgLoad($("#avt-" + i), "", n[i].url, 50);
                    $(".avtSwitch").click(function () {
                        if (!$(this).hasClass(".selected")) {
                            $(".avtSwitch").removeClass("selected"), $(this).addClass("selected");
                            var e = $(this),
                                t = e.data("type");
                            $.ajax({
                                url: Core.API_VER + "/account/change_avatar",
                                data: {
                                    uid: Signup.userDetail.id,
                                    type: t
                                },
                                success: function (t) {
                                    if (!t.success) return;
                                    Signup.userDetail.fid = e.data("url");
                                    var n = new Image;
                                    n.onload = function () {
                                        $("#abtAvt").prop("src", this.src)
                                    };
                                    var r = Signup.userDetail.fid;
                                    r.indexOf("http://") != 0 && (r = $.id2url(r, "U1", "avatar") + ".png"), n.src = r, Signup.userDetail.fidtiny = t.result
                                }
                            })
                        }
                    })
                }
            })
        },
        etpwd: function (e) {
            About.isAnimate = !1;
            var t = '<input id="oldPwd" class="input oldPwd" type="password" placeholder="输入你的旧密码"><input id="newPwd" class="input newPwd" type="password" placeholder="输入你的新密码"><a class="btn stSbmt" href="#" id="pwdSubmit"><span class="btnLt"></span>提交<span class="btnRt"></span></a>';
            t = '<div class="rctAbtCtn form ' + About.stId + '"><div class="fldCtn">' + t + "</div></div>", About.afterHtml(e, t), $("#newPwd, #oldPwd").blur(function () {
                var e = $(this).val();
                e == "" ? $(this).data("error", "密码不能为空") : e.length < 6 ? ($(this).data("error", "密码必须6位以上"), Signup.tpsShow($(this), "密码必须6位以上")) : ($("#tps").hide(), $(this).data("error", ""))
            }), $("#pwdSubmit").click(function () {
                if (!Core.isEmpty($("#newPwd").data("error"))) Signup.tpsShow($("#newPwd"), $("#newPwd").data("error"));
                else if (!Core.isEmpty($("#oldPwd").data("error"))) Signup.tpsShow($("#oldPwd"), $("#oldPwd").data("error"));
                else {
                    var e = Core.API_VER + "/account/change_pwd";
                    $.ajax({
                        url: e,
                        data: {
                            uid: Signup.userDetail.id,
                            oldpwd: $("#oldPwd").val(),
                            newpwd: $("#newPwd").val()
                        },
                        success: function (e) {
                            e.success ? (Gns.nowGns("你的密码已经修改成功"), $("#oldPwd, #newPwd").val(""), $("#oldPwd, #newPwd").prev().show()) : Gns.nowGns(e.codemsg)
                        }
                    })
                }
            }), $("#newPwd").keyup(function (e) {
                e.keyCode == 13 && ($("#newPwd").blur(), $("#pwdSubmit").click())
            }).keydown(function (e) {
                e.stopPropagation()
            })
        },
        thm: function (e) {
            $.ajax({
                url: Core.API_VER + "/setting/fetch_settings",
                data: {
                    uid: Signup.userDetail.id,
                    t: "thm"
                },
                success: function (t) {
                    About.isAnimate = !1;
                    var n = t.result.items[0].v,
                        r = "";
                    for (var i = 0; i < Thm.length; ++i) {
                        var s = "";
                        n == Thm[i][0] && (s = "selected");
                        var o = "";
                        Core.isNewIcon("t", Thm[i][0]) && (o = '<em class="newApp" style="left:auto; top:3px; right:11px;"></em>', Core.ulNewIcon("t", Thm[i][0])), r += '<div class="rctAbtCtn thm">' + o + '<div class="thmIcon ' + Thm[i][0] + '"></div>' + '<p class="ctn">' + Thm[i][1] + "</p>" + '<a id="' + Thm[i][0] + '" href="#" class="chc ' + s + ' thmSwitch"></a>' + "</div>"
                    }
                    r = '<div class="rctAbtCtn form ' + About.stId + '"><div class="fldCtn">' + r + "</div></div>", About.afterHtml(e, r), $(".thmSwitch").click(function () {
                        if (!$(this).hasClass("selected")) {
                            var e = $(this).prop("id");
                            $(".thmSwitch").removeClass("selected"), $(this).addClass("selected"), Gns.nowGns('你正在更换 <a class="trg" href="#">' + $(this).prev().text() + "</a> 主题。"), $.ajax({
                                url: Core.API_VER + "/setting/post_settings",
                                data: {
                                    uid: Signup.userDetail.id,
                                    n: "thm",
                                    v: e
                                }
                            });
                            var t = function () {
                                for (var t = 0; t < Thm.length; ++t) $("html").removeClass(Thm[t][0] + "Thm");
                                $("html").addClass(e + "Thm");
                                var n = "-16px";
                                $("#mainMenuCtn").animate({
                                    bottom: n
                                }, 300), Signup.userDetail.sts.thm = e
                            };
                            t()
                        }
                    })
                }
            })
        },
        fltr: function (e) {
            $.ajax({
                url: Core.API_VER + "/account/filter/fetch_filter_tags",
                data: {
                    uid: Signup.userDetail.id
                },
                success: function (t) {
                    About.isAnimate = !1;
                    var n = "",
                        r = t.result.items;
                    for (var i = 0; i < r.length; ++i) n += About.fltrGnHtml(r[i]);
                    var s = About.fltrHeight(n);
                    s == 41 && (s = 0);
                    var o = "";
                    r.length == 0 && (o = "hide"), n = '<div id="fltrAppCtn" class="fltrAppCtn ' + o + '">' + n + "</div>", n += '<div class="fld"><span class="fltrHint"></span><input id="fltrAppSch" class="input fltrIpt" value="添加你想过滤的内容" onblur="if(this.value == \'\')this.value=\'添加你想过滤的内容\'" onfocus="if(this.value == \'添加你想过滤的内容\')this.value = \'\'"><a id="fltrAppBtn" href="#" class="btn"><span class="btnLt"></span>提交<span class="btnRt"></span></a></div>', n = '<div class="rctAbtCtn form ' + About.stId + '"><div class="fldCtn">' + n + "</div></div>", s += 78, About.afterHtml(e, n, s), $("#fltrAppSch").keyup(function (e) {
                        if (e.keyCode == 13) {
                            $("#fltrAppBtn").click();
                            return
                        }
                        if (e.keyCode == 39) {
                            $(this).val($(this).val() + $(".fltrHint").text()), $(".fltrHint").text("");
                            return
                        }
                        var t = $(this).val();
                        if (t == "") {
                            $(".fltrHint").text("");
                            return
                        }
                        $.ajax({
                            url: Core.API_VER + "/account/filter/auto_filter_tags",
                            data: {
                                uid: Signup.userDetail.id,
                                q: t
                            },
                            success: function (e) {
                                if (e.result == null || e.result == "") {
                                    $(".fltrHint").text("");
                                    return
                                }
                                e.result = e.result.replace(t, ""), $(".fltrHint").css({
                                    left: Search.getStrWidth(t) + 13 + "px"
                                }), $(".fltrHint").text(e.result)
                            }
                        })
                    }), $("#fltrAppBtn").click(function () {
                        var e = $(this).prev().val();
                        if (e == "" || e == "添加你想过滤的内容") return;
                        $.ajax({
                            url: Core.API_VER + "/account/filter/post_filter_tags",
                            data: {
                                uid: Signup.userDetail.id,
                                tag: e
                            },
                            success: function (e) {
                                if (!e.success) {
                                    Gns.nowGns(e.codemsg);
                                    return
                                }
                                var t = About.fltrHeight($("#fltrAppCtn").html() + About.fltrGnHtml(e.result));
                                if (t != $("#fltrAppCtn").height() + 41) {
                                    t -= $("#fltrAppCtn").height(), $("#fltrAppCtn").height() != 0 && (t -= 41), $("#fltrAppCtn").parent().parent().animate({
                                        height: "+=" + t
                                    }, 300);
                                    var n = !1;
                                    $("#fltrAppCtn").height() == 0 && (n = !0, $("#fltrAppCtn").css({
                                        "padding-top": "0px",
                                        "padding-bottom": "0px"
                                    })), $("#fltrAppCtn").show().animate({
                                        height: "+=" + t
                                    }, 300, function () {
                                        n && $("#fltrAppCtn").css({
                                            "padding-top": "25px",
                                            "padding-bottom": "15px",
                                            height: t - 41 + "px"
                                        }), $("#fltrAppCtn").append(About.fltrGnHtml(e.result))
                                    })
                                } else $("#fltrAppCtn").append(About.fltrGnHtml(e.result))
                            }
                        }), $(this).prev().val("")
                    })
                }
            })
        },
        fltrGnHtml: function (e) {
            return '<div class="fltrWdCtn" id="' + e.id + '">' + '<div class="fltrWdLt"></div>' + '<p class="fltrWdCtt">' + e.n + "</p>" + '<div class="fltrWdRb"></div>' + '<a href="#" class="delFltr"></a>' + "</div>"
        },
        fltrHeight: function (e, t) {
            var n = '<div id="fltrAppCtnTemp" class="rctCtn fltrAppCtn" style="width:328px">' + e + "</div>";
            About.ctn.append(n), t != undefined && $("#fltrAppCtnTemp").children().each(function () {
                if ($(this).prop("id") == t) return $(this).remove(), !1
            });
            var r = $("#fltrAppCtnTemp").height();
            return $("#fltrAppCtnTemp").remove(), r + 41
        },
        afterHtml: function (e, t, n) {
            n != undefined && (StPrfl[About.stId][1] = n), About.isAnimate = !0, e.after(t), e.next().animate({
                height: StPrfl[About.stId][1] + "px"
            }, 300, function () {
                About.isAnimate = !1, About.scrollApi.reinitialise()
            })
        },
        ctnHtml: function (e, t, n) {
            About.ctn.css("border-bottom", "1px solid #DDDDDD"), About.ctn.children(".abtTabCtn").length == 1 ? t == undefined ? About.ctn.children(".abtTabCtn").next().html(e) : About.ctn.children(".abtTabCtn").next().append(e) : t == undefined ? About.ctn.html(e) : About.ctn.append(e), About.ctn.show(), n == undefined && About.scrollApi.reinitialise(), $(".jspPane").height() - 360 > Core.bodyHeight && !About.model && About.st <= 20 && About.scrollApi.scrollToY(360), $("#span").html("")
        },
        noCttHtml: function (e, t) {
            if (e == "" || e == undefined) e = "这里现在是空的";
            var n = 197;
            t == "about" && (n = $("#abtCtn>.abtCtn").height() - 443);
            var r = '<div class="noCtt" style="height:' + n + 'px">' + '<div class="boxCtn">' + '<div class="icon box"></div>' + '<p class="ctn">' + e + "</p>" + "</div>" + "</div>";
            return r
        },
        resize: function () {
            $("#abtCtn").children().css("height", Core.bodyHeight);
            if (About.isOpen || NtrlLngTop.isOpen) Core.bodyWidth -= 420;
            Core.bodyWidth > 785 ? About.isSchFldAnimate = !0 : About.isSchFldAnimate = !1, Core.bodyWidth >= 715 ? ($("#mainMenuCtn").css({
                width: "331px"
            }), $("#schFld").css({
                width: "268px"
            }), Search.setSchHint()) : ($("#mainMenuCtn").css({
                width: 331 - (715 - Core.bodyWidth) + "px"
            }), $("#schFld").css({
                width: 268 - (715 - Core.bodyWidth) + "px"
            }), $(".schHint").hide()), About.scrollApi != null && About.scrollApi.reinitialise();
            if (About.isAnimate) return;
            About.isOpen && $("#mainBd").css("width", Core.bodyWidth + "px"), NtrlLngTop.isOpen && $("#mainBd").css("width", Core.bodyWidth + 20 + "px")
        }
    }, NtrlLngTop = {
        isOpen: !1,
        isInsertPlay: !1,
        ps: 0,
        init: function () {
            $(document).on("click", "#closeNtrlPlay", NtrlLngTop.closeNtrlPlay)
        },
        switching: function (e) {
            if (About.isAnimate) return;
            Cht.fuid = "", $("#ntrlLngTopCtn").show(), NtrlLngTop.isOpen ? (NtrlLngTop.isOpen = !1, About.isAnimate = !0, $("#gnsCtn").isDisplay() && $("#gnsCtt").animate({
                left: "+=210"
            }, 300), $("#allBd").css("width", Core.bodyWidth + 420 + 420 + "px"), Core.bodyWidth += 420, Player.resize(), Search.resize(!0), Core.bodyWidth -= 420, $("#ntrlLngTopCtn").animate({
                width: "0px"
            }, 300, function () {
                $(this).hide()
            }), $("#mainBd").animate({
                width: Core.bodyWidth + 420 + "px"
            }, 300, function () {
                $("#allBd, #mainBd").css("width", "100%"), Core.resize(), About.isAnimate = !1
            })) : (About.isAnimate = !0, NtrlLngTop.isOpen = !0, About.isOpen ? ($("#allBd").css("width", Core.bodyWidth + 420 + 420 + "px"), About.isOpen = !1, About.stopPt(), About.ouid = "", About.mid = "", About.scrollApi.scrollToY(0), About.currentMain = "", $("#mainBd").animate({
                width: "+=20"
            }, 300), $("#ntrlLngTopCtn").show().animate({
                width: "400px"
            }, 300, function () {
                About.isAnimate = !1, $("#allBd").css("width", "100%"), $("#abtCtn").hide(), About.ctn.hide().html(""), $("#abtCover").prop("src", IMG_URL + "/defaults/profile/cover" + Retina.suffix + ".jpg"), $("#abtAvt").prop("src", IMG_URL + "/defaults/avatar/80" + Retina.suffix + ".jpg.png"), About.model || (About.scrollApi.reinitialise(), $("#mask").hide(), $("#cvCtn").css({
                    position: "relative",
                    top: "0px"
                }), $("#abtMenu").css({
                    position: "relative",
                    top: "0px"
                }), About.model = !0, About.scrollApi.scrollToY(0))
            })) : ($("#allBd").css("width", Core.bodyWidth + 420 + "px"), $("#mainBd").css("width", Core.bodyWidth + "px"), $("#ntrlLngTopCtn").show().animate({
                width: "400px"
            }, 300, function () {
                About.isAnimate = !1, $("#allBd").css("width", "100%")
            }), $("#mainBd").animate({
                width: Core.bodyWidth - 400 + "px"
            }, 300)), $("#gnsCtn").isDisplay() && $("#gnsCtt").animate({
                left: "-=210"
            }, 300), Core.resize(), NtrlLngTop.showNtrl())
        },
        showNtrl: function () {
            return $("#ntrlLngTopCtn>.ntrlLngTop").html('<h3 class="ntrlLngHd"><i class="ic"></i>优秀的搜索条件<a href="#" id="ntrlChngBtn" class="chngBtn">换一批</a></h3>'), $.ajax({
                url: Core.API_VER + "/app/fetch_natural",
                data: {
                    ps: 100
                },
                success: function (e) {
                    if (!e.success) return;
                    var t = "",
                        n = e.result.items;
                    for (var r = 0; r < n.length; ++r) {
                        var i = "";
                        r < NtrlLngTop.ps && (i = "dspr"), t += '<div class="ntrlLng ' + i + '">' + '<a href="#" class="ntlPlyCtl tkrsFlyEvent" data-fid="CmbtFlyBadge" data-span="' + n[r].sw + '"></a>' + '<blockquote class="qtSch">' + n[r].sw + "</blockquote>" + "</div>"
                    }
                    $("#ntrlLngTopCtn>.ntrlLngTop").append(t);
                    var s = $.makeArray($("#ntrlLngTopCtn>.ntrlLngTop>.ntrlLng")),
                        o = 0,
                        u = new Array;
                    for (var r = 0; r < NtrlLngTop.ps; ++r) u[r] = r;
                    for (var r = 0; r < u.length; r++) {
                        var a = parseInt(Math.random() * u.length),
                            f = u[r];
                        u[r] = u[a], u[a] = f
                    }
                    for (var r = 0; r < NtrlLngTop.ps; ++r) setTimeout(function () {
                            $(s[u[o]]).animate({
                                opacity: "1"
                            }, 300), ++o
                        }, r * 150)
                }
            }), $("#ntrlChngBtn").click(NtrlLngTop.showNtrl), !1
        },
        closeSelf: function () {
            NtrlLngTop.isOpen && NtrlLngTop.switching()
        },
        ntrlClick: function () {
            $(this).hasClass("pause") ? ($(this).removeClass("pause"), NtrlLngTop.closeNtrlPlay()) : (NtrlLngTop.ntrlObj == undefined ? NtrlLngTop.isInsertPlay = !0 : NtrlLngTop.ntrlObj.removeClass("pause"), $(this).addClass("pause"), NtrlLngTop.ntrlObj = $(this), NtrlLngTop.ntrlText = $(this).next().text(), Core.fullScrenMenuShow('<span id="prevHtml">正在收听 ' + NtrlLngTop.ntrlText + ' <span class="spl"></span><a href="#" id="closeNtrlPlay" class="clsFlwLstn overlay">退出</a></span>', "ntrl"), $.ajax({
                url: Core.API_VER + "/search/jing/fetch_pls",
                data: {
                    q: NtrlLngTop.ntrlText,
                    ps: 100,
                    st: "0",
                    u: Signup.userDetail.id
                },
                success: function (e) {
                    Player.music = new Array;
                    for (var t = 0; t < e.result.items.length; ++t) Search.setMusic(e.result.items[t], t);
                    Player.pos = -1, $("#playCtl").hasClass("pause") && $("#playCtl").click(), NtrlLngTop.next()
                }
            }))
        },
        closeNtrlPlay: function () {
            NtrlLngTop.ntrlObj != undefined && NtrlLngTop.ntrlObj.removeClass("pause"), Core.fullScrenMenuHide(), NtrlLngTop.isInsertPlay = !1, NtrlLngTop.ntrlObj = undefined, NtrlLngTop.ntrlText = "", Search.st = Player.cacheSt, Player.music = Player.cacheMusic, Player.pos = Player.cachePos, Search.total = Player.cacheTotal, Player.cacheMusic = null;
            var e = function () {
                Player.isVolumeDown ? setTimeout(e, 200) : Player.play(Player.cacheSec)
            };
            e()
        },
        next: function () {
            $("#playCtl").removeClass("play").addClass("pause"), Player.addLoading(), Player.closeGns(), $("#playerRptOne").hasClass("selected") || ++Player.pos;
            var e = function () {
                Player.isVolumeDown ? setTimeout(e, 200) : (Player.pos >= Player.music.length && (Player.pos = 0), Player.play())
            };
            e()
        },
        resize: function () {
            NtrlLngTop.ps = parseInt((Core.bodyHeight - 95) / 65);
            var e = NtrlLngTop.ps * 65 + 57;
            $("#ntrlLngTopCtn>.ntrlLngTop").css({
                height: e + 3 + "px",
                "margin-top": "-" + e / 2 + "px"
            })
        }
    };
